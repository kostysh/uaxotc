# OTC 
Smart contract and Dapp to trade any ERC20 tokens for UAX (Ukraine Hryvna stablecoin. This coin will be online at the end of the 2019 - start of 2020)  

Smart contracts are deployed to the Ropsten TestNet.  
Demo of the DApp is avalable here: [https://kostysh.bitbucket.io/](https://kostysh.bitbucket.io/)

## Author
- Kostiantyn Smyrnov <kostysh@gmail.com>

## Prerequisites
- node.js (version 10 up)

## Setup 

```sh
npm i
```

## Testing

```sh
npm test
```
