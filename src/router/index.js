import { route as trades } from '../views/Trades';
import { route as lot } from '../views/Lot';
import { route as create } from '../views/CreateLot';
import { route as mylots } from '../views/MyLots';
import { route as qa } from '../views/Qa';

export default [
    ...trades,
    ...lot,
    ...create,
    ...mylots,
    ...qa
];

