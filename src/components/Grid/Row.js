import styled from 'styled-components';

const Row = styled.div`
    & {
        display:flex;
        flex-direction: row;
        flex-wrap: wrap;
    }

    &::after {
        content: '';
        display: table;
        clear: both;
    }

    ${({ justify }) => {
        let style;
        switch (justify) {
            case 'start':
                style = 'justify-content: flex-start;';
                break;
            case 'end':
                style = 'justify-content: flex-end;';
                break;
            case 'center':
                style = 'justify-content: center;';
                break;
            case 'between':
                style = 'justify-content: space-between;';
                break;
            case 'around':
                style = 'justify-content: space-around;';
                break;
            default:
                style = '';
        }
        return (style);
    }}

    ${({ align }) => {
        let style;
        switch (align) {
            case 'start':
                style = 'align-items: flex-start;';
                break;
            case 'end':
                style = 'align-items: flex-end;';
                break;
            case 'center':
                style = 'align-items: center;';
                break;
            case 'stretch':
                style = 'align-items: stretch;';
                break;
            case 'baseline':
                style = 'align-items: baseline;';
                break;
            default:
                style = '';
        }
        return (style);
    }}
`;

export default Row;

