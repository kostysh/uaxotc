import React from 'react';
import styled from 'styled-components';

export const GridContext = React.createContext({
    debug: false
});

const Grid = styled.div`
    &::after {
        content: '';
        display: table;
        clear: both;
    }
`;

export default ({children, context, ...props}) => (
    <GridContext.Provider value={{...context, ...props}}>
        <Grid {...props}>
            {children} 
        </Grid>
    </GridContext.Provider>    
);

