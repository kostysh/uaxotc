import React, { Component } from 'react';
import styled from 'styled-components';

import  { GridContext } from './Grid';

const StyledColumn = styled.div `
    & {
        display: flex;
        flex-direction: column;
        flex-basis: 0;
    }

    ${({ justify }) => {
        let style;
        switch (justify) {
            case 'start':
                style = 'justify-content: flex-start;';
                break;
            case 'end':
                style = 'justify-content: flex-end;';
                break;
            case 'center':
                style = 'justify-content: center;';
                break;
            case 'between':
                style = 'justify-content: space-between;';
                break;
            case 'around':
                style = 'justify-content: space-around;';
                break;
            default:
                style = '';
        }
        return (style);
    }}

    ${({ align }) => {
        let style;
        switch (align) {
            case 'start':
                style = 'align-content: flex-start;';
                break;
            case 'end':
                style = 'align-content: flex-end;';
                break;
            case 'center':
                style = 'align-content: center;';
                break;
            case 'stretch':
                style = 'align-content: stretch;';
                break;
            case 'baseline':
                style = 'align-content: baseline;';
                break;
            default:
                style = '';
        }
        return (style);
    }}

    ${({ flex }) => (flex || flex === 0 ? `flex-grow: ${flex} !important;` : 'flex-grow: 1;')}

    ${({ textAlign }) => (textAlign ? `text-align: ${textAlign};` : '')}

    ${({ basis }) => (basis || basis === 0  ? `flex-basis: ${basis} !important;` : '')}

    ${({ shrink }) => (shrink || shrink === 0 ? `flex-shrink: ${shrink} !important;` : '')}

    ${({ debug }) => (debug ? 'border: 1px solid red;' : '')}
`;

class Column extends Component {
    render() {
        return (
            <StyledColumn {...{...this.context, ...this.props}}>
                {this.props.children} 
            </StyledColumn>
        );
    };
}

Column.contextType = GridContext;

export default Column;

