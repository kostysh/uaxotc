import React from 'react';
import styled from 'styled-components';
import { Text } from '@aragon/ui';
import { useTranslation } from 'react-i18next';

// System imports
import { formatTokenBalance } from '../../utils/tokens.helpers';

// Custom components
import Box from '../Box';
import EtherLink from '../EtherLink';

const TokenDetailsCardOuter = styled(Box)`
& {
    padding: 30px;
    background-color: white;    

    p {
        margin-bottom: 10px;
        overflow: hidden;
        text-overflow: ellipsis;

        &:last-child {
            margin-bottom: 0;
        }
    }

    @media (max-width: 540px) {
        border-bottom: 1px solid rgba(0,0,0,0.07);
    }
}
`;

export default ({ network, tokenDetails, tokenBalance }) => {
    const { t } = useTranslation();
    return (
        <TokenDetailsCardOuter>
            <Text.Paragraph>
                <Text size="large">
                    <strong>{t('create.wizard.cards.tokendetails.labels.contract')}:</strong>&nbsp;
                </Text>
                <EtherLink 
                    network={network}
                    address={tokenDetails.address}
                    type="token"
                    short
                />
            </Text.Paragraph>
            <Text.Paragraph>
                <Text size="large">
                    <strong>{t('create.wizard.cards.tokendetails.labels.name')}:</strong>&nbsp;{tokenDetails.name} ({tokenDetails.symbol})
                </Text>
            </Text.Paragraph>
            <Text.Paragraph>
                <Text size="large">
                    <strong>{t('create.wizard.cards.tokendetails.labels.balance')}:</strong> {formatTokenBalance(tokenBalance, tokenDetails.decimals)}
                </Text>
            </Text.Paragraph>
        </TokenDetailsCardOuter>        
    );
};