import React from 'react';
import styled from 'styled-components';
import { AppBar } from '@aragon/ui';

export const Page = styled.div`
& {
    padding: 30px;
    min-width: 320px;
    max-width: 964px;
    display: flex;
    flex-direction: column;  
    align-items: stretch;

    @media (max-width: 540px) {
        padding-top: 0;
        padding-left: 0;
        padding-right: 0;
    }
}
`;

const PageBarOuter = styled.div`
& {
    border-top: 1px solid rgba(0,0,0,0.07);
    border-bottom: 1px solid rgba(0,0,0,0.07);
    box-sizing: border-box;
}

> div {
    &:after {
        display: none;
    }    
}

h1 {
    span {
        font-size: 18px;
    }
}
`;

export const PageBar = ({ children, ...props }) => (
    <PageBarOuter>
        <AppBar {...props}>
            {children}
        </AppBar>
    </PageBarOuter>    
);



export const PageContent = styled.div`
& {
    display: flex;
    flex-direction: row;
    justify-content: flex-start;
    align-items: stretch;
    flex-grow: 1;

    @media (max-width: 540px) {
        flex-direction: column-reverse;
        flex-wrap: wrap;
    }
}
`;

export const LeftPanel = styled.div`
& {
    display: flex;
    flex-grow: 1;
    flex-shrink: 0;
    flex-direction: column;
    width: 60%;
    background: white;
    overflow: visible;

    @media (max-width: 570px) {
        width: auto;
    }

    @media (max-width: 540px) {
        width: 100%;
    }
}
`;

export const LeftPanelFooter = styled.div`
& {
    display: flex;
    flex-grow: 1;
    background-color: rgba(0,203,230,0.15);
    color: #34415e;

    @media (max-width: 540px) {
        justify-content: center;
    }
}
`;

export const RightPanel = styled.div`
& {
    display: flex;
    flex-direction: column;
    flex-grow: 1;
    flex-shrink: 1;
    width: 40%;
    margin-left: 30px;

    @media (max-width: 540px) {
        margin-top: 20px;
        margin-left: 0;
        width: 100%;        
    }
}
`;