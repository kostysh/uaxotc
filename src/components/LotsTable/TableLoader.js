import React from 'react';
import styled from 'styled-components';

// Custom components
import Loader from '../Loader';

const TableLoaderOuter = styled.div`
& {
display: flex;
flex-direction: column;
align-items: center;
justify-content: center;
padding: 30px;
}
`;

export default props => (
    <TableLoaderOuter>
        <Loader 
            color="#757575" 
            {...props}
        />
    </TableLoaderOuter>
);