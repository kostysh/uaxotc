import en from './lotsTable.en.json';
import ru from './lotsTable.ru.json';
import uk from './lotsTable.uk.json';

export default {
    en,
    ru,
    uk
};