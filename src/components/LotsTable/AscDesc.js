import React, { useState } from 'react';
import styled from 'styled-components';

const AscDescOuter = styled.div`
& {
    display: flex;
    flex-direction: row;
    align-items: center;
    cursor: pointer;
}

.icon {
    disply: block;
    width: 10px;
    height: 10px;
    margin-left: 10px;
    background-image: none;
    background-repeat: no-repeat;
    background-position: 50% 50%;

    &.asc {
        background-image: url(data:image/svg+xml,%3Csvg%20width%3D%229%22%20height%3D%225%22%20viewBox%3D%220%200%209%205%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3E%3Cpath%20d%3D%22M0%200h8.36L4.18%204.18z%22%20fill%3D%22%23B3B3B3%22%20fill-rule%3D%22evenodd%22%2F%3E%3C%2Fsvg%3E);
        transform: rotate(180deg);
    }

    &.desc {
        background-image: url(data:image/svg+xml,%3Csvg%20width%3D%229%22%20height%3D%225%22%20viewBox%3D%220%200%209%205%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3E%3Cpath%20d%3D%22M0%200h8.36L4.18%204.18z%22%20fill%3D%22%23B3B3B3%22%20fill-rule%3D%22evenodd%22%2F%3E%3C%2Fsvg%3E);        
    }
}
`;

export default ({
    children, 
    value,
    onChange = () => {}
}) => {
    const parseValue = value => {
        switch (value) {
            case 'asc': 
                return 1;
            case 'desc':
                return 2;            
            default:
                return 0;
        }
    };
    const [mode, setMode] = useState(parseValue(value));
    const toggleMode = () => {
        switch (mode) {
            case 0: 
                setMode(1);
                onChange('asc');
                break;
            case 1:
                setMode(2);
                onChange('desc');
                break;
            case 2:
                setMode(0);
                onChange(false);
                break;
            default:
        }
    };
    const modeClass = () => {
        switch (mode) {
            case 0: 
                return 'icon';
            case 1:
                return 'icon asc';
            case 2:
                return 'icon desc';
            default:
        }
    };
    return (
        <AscDescOuter onClick={toggleMode}>
            <div>{children}</div>
            <div className={modeClass()} />
        </AscDescOuter>
    );
};