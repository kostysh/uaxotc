import React, { Component } from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import { withTranslation } from 'react-i18next';
import { Button } from '@aragon/ui';

// System imports
import * as selectors from '../../store/selectors';
import * as actions from '../../store/actions';

const PaginatorOuter = styled.div`
& {
    display: flex;
    flex-direction: row;
    flex-wrap: nowrap;
    align-items: stretch;
    justify-content: flex-start;
    margin: 10px 20px 20px 20px;
    
    button {
        font-size: 13px;
        padding: 1px 10px;
    }

    @media (max-width: 540px) {
        justify-content: center;
    }
}
`;

const ButtonsBlock = styled(PaginatorOuter)`
& {
    margin: 0 10px 0 10px;
    
    @media (max-width: 540px) {
        justify-content: space-between;
    }
}
`;

const MobPaginatorOuter = styled(PaginatorOuter)`
& {
    display: none;
    margin-top: 0;

    @media (max-width: 540px) {
        display: flex;        
    }
}

> div {
    display: flex;
    flex-direction: row;
    flex-wrap: nowrap;
    flex-grow: 1;

    &:nth-of-type(1) {
        justify-content: flex-start;
    }

    &:nth-of-type(2) {
        justify-content: flex-end;
    }
}
`;

const NavigationBlock = styled.div`
& {
    display: flex;
    flex-direction: row;
    flex-wrap: nowrap;
    align-items: stretch;
    justify-content: center;

    @media (max-width: 540px) {
        display: none;
    }
}
`;

const PageButtonOuter = styled.div`
& {
    margin-right: 5px;

    &:last-child {
        margin-right: 0;
    }
}
`;

class LotsPaginator extends Component {

    doPaginate = (page, pagination, maxPages = 6) => {
        const { owned } = this.props;
        const paginateFunc = owned ? this.props.ownedPaginate : this.props.paginate;
        const { total, limit, start } = pagination;
        const totalPages = Math.ceil(total / limit) - 1;
        const rightEdge = start + maxPages - 1;
        const leftEdge = start;
        let newStart;

        if (page < start) {
            const left = page - maxPages + 1;
            newStart = left >= 0 ? left : 0;
        }

        if (page >= totalPages) {
            page = totalPages;
            newStart = totalPages - maxPages + 1;
        }

        if (page <= 0) {
            page = 0;
            newStart = 0;
        }

        if (page >= rightEdge && page < totalPages) {
            newStart = rightEdge;
        }

        if (page === leftEdge) {
            const left = leftEdge - maxPages + 1;
            newStart = left >= 0 ? left : 0;
        }

        paginateFunc({
            page,
            ...newStart !== undefined ? { start: newStart } : {}
        });
    };

    doPaginateToStart = (pagination, maxPages) => this.doPaginate(0, pagination, maxPages);

    doPaginateToEnd = (pagination, maxPages) => this.doPaginate(Math.ceil(pagination.total / pagination.limit) - 1, pagination, maxPages);

    doPaginateJumpNext = (pagination, maxPages) => this.doPaginate(pagination.page + 1, pagination, maxPages);

    doPaginateJumpPrev = (pagination, maxPages) => this.doPaginate(pagination.page - 1, pagination, maxPages);

    render() {
        const { owned, pagination, ownedPagination, maxPages = 6 } = this.props;
        const targetPagination = owned ? ownedPagination : pagination;
        const { total, page, limit, start } = targetPagination;
        const totalPages = Math.ceil(total / limit);
        const pages = Array(totalPages).fill(0).map((_, index) => ({
            label: index + 1,
            page: index
        }));

        return (
            <div>
                <PaginatorOuter>
                    <NavigationBlock>
                        <PageButtonOuter>
                            <Button 
                                mode="outline"
                                size="mini" 
                                onClick={() => this.doPaginateToStart(targetPagination, maxPages)}
                            >{ page > 0 ? '1 ' : '' }&#8676;</Button>
                        </PageButtonOuter>
                        <PageButtonOuter>
                            <Button 
                                mode="outline"
                                size="mini" 
                                onClick={() => this.doPaginateJumpPrev(targetPagination, maxPages)}
                            >&larr;</Button>
                        </PageButtonOuter>                        
                    </NavigationBlock> 
                    <ButtonsBlock>
                        {pages.slice(start, start + maxPages).map((pg, index) => (
                            <PageButtonOuter key={index}>
                                <Button 
                                    mode={pg.page === page ? 'secondary' : 'outline'}
                                    disabled={pg.page === page}
                                    size="mini" 
                                    onClick={() => this.doPaginate(pg.page, targetPagination, maxPages)}
                                >{pg.label}</Button>
                            </PageButtonOuter>                    
                        ))}
                    </ButtonsBlock>
                    <NavigationBlock>
                        <PageButtonOuter>
                            <Button 
                                mode="outline"
                                size="mini" 
                                onClick={() => this.doPaginateJumpNext(targetPagination, maxPages)}
                            >&rarr;</Button>
                        </PageButtonOuter>
                        <PageButtonOuter>
                            <Button 
                                mode="outline"
                                size="mini" 
                                onClick={() => this.doPaginateToEnd(targetPagination, maxPages)}
                            >&#8677; {totalPages}</Button>
                        </PageButtonOuter>
                    </NavigationBlock>                    
                </PaginatorOuter>
                <MobPaginatorOuter>
                    <div>
                        <PageButtonOuter>
                            <Button 
                                mode="outline"
                                size="mini" 
                                onClick={() => this.doPaginateToStart(targetPagination, maxPages)}
                            >{ page > 0 ? '1 ' : '' }&#8676;</Button>
                        </PageButtonOuter>
                        <PageButtonOuter>
                            <Button 
                                mode="outline"
                                size="mini" 
                                onClick={() => this.doPaginateJumpPrev(targetPagination, maxPages)}
                            >&larr;</Button>
                        </PageButtonOuter>
                    </div>
                    <div>
                        <PageButtonOuter>
                            <Button 
                                mode="outline"
                                size="mini" 
                                onClick={() => this.doPaginateJumpNext(targetPagination, maxPages)}
                            >&rarr;</Button>
                        </PageButtonOuter>
                        <PageButtonOuter>
                            <Button 
                                mode="outline"
                                size="mini" 
                                onClick={() => this.doPaginateToEnd(targetPagination, maxPages)}
                            >&#8677; {totalPages}</Button>
                        </PageButtonOuter>
                    </div>
                </MobPaginatorOuter>
            </div>
        );
    }
}

function mapStateToProps(state) {

    return {
        pagination: selectors.tradesLotsPagination(state),
        ownedPagination: selectors.mylotsPagination(state),
        blockNumber: selectors.tradesBlockNumber(state),
    };
}

const mapDispatchToProps = dispatch => {

    return {
        paginate: pagination => dispatch(actions.tradesLotsPaginate(pagination)),
        ownedPaginate: pagination => dispatch(actions.mylotsPaginate(pagination))
    };
};

const connectedLotsPaginator= withTranslation()(connect(mapStateToProps, mapDispatchToProps)(LotsPaginator));

export default connectedLotsPaginator;