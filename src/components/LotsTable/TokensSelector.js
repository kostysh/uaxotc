import React, { Component } from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import { DropDown } from '@aragon/ui';
import { withTranslation } from 'react-i18next';

// System imports
import * as selectors from '../../store/selectors';
import * as actions from '../../store/actions';

const TokensOuter = styled.div`
    & {
        min-width: 106px;

        @media (max-width: 540px) {
            min-width: 60px;

            div {
                div {
                    &:first-child {
                        padding-left: 5px;
                        padding-right: 16px;
                        background-position: calc(100% - 4px) 50%;
                    }
                }

                div[role=listbox] {
                    padding-right: 0;
                }
            }
        }        
    }
`;

class TokensSelector extends Component {
    
    handleChange(index) {
        const { tokens, selectToken, unselectToken } = this.props;

        if (index > 0) {
            selectToken(tokens[index - 1]);
        } else {
            unselectToken();
        }        
    }

    render() {
        const { t, tokens, lotsParams: { token: selectedToken = 0 } } = this.props;
        const records = [
            selectedToken 
                ? t('lotstable.tokensselector.labels.long') 
                : t('lotstable.tokensselector.labels.short'), 
            ...tokens
        ];

        return (
            <TokensOuter>
                <DropDown 
                    wide
                    items={records}
                    active={records.indexOf(selectedToken)}
                    onChange={index => this.handleChange(index)}
                />
            </TokensOuter>            
        );
    }
}

function mapStateToProps(state) {

    return {
        tokens: selectors.tradesLotsTokens(state),
        lotsParams: selectors.tradesLotsParams(state)
    };
}

const mapDispatchToProps = dispatch => {

    return {
        selectToken: token => dispatch(actions.tradesLotsParamsAdd({
            token
        })),
        unselectToken: () => dispatch(actions.tradesLotsParamsRemove(['token']))
    };
};

const connectedTokensSelector = connect(mapStateToProps, mapDispatchToProps)(TokensSelector);

export default withTranslation()(connectedTokensSelector);