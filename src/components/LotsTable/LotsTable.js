import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import { Text, Button, Checkbox, IconArrowRight, IconRemove, IconFundraising } from '@aragon/ui';
import { useTranslation } from 'react-i18next';

// Custom components
import TokensSelector from './TokensSelector';
import Table from '../Table';
import LotsPaginator from './LotsPaginator';
import AscDesc from './AscDesc';
import { Notification } from '../Notifications';
import { EtherAnchor } from '../EtherLink';

// System imports
import config from '../../config';
import { formatTokenBalance, perOne } from '../../utils/tokens.helpers';

const BuyButton = styled(Button)`
& {
    display: flex;
    flex-direction: row;
    line-height: 1.3;

    &:hover {
        outline: 1px solid rgba(0,0,0,0.1);
    }

    @media (max-width: 540px) {
        padding: 5px 10px;

        .label {
            display: none;
        }
    }     
}

.label {
        margin-right: 10px;
    }
`;

const NoLotsOuter = styled.div`
& {
    display: flex;
    flex-direction: column;
    align-items: stretch;
    justify-content: center;
    padding: 30px;
}
`;

const NoLotsMessage = styled.div`
& {
    margin-bottom: 20px;
}
`;

const StatusOuter = styled.div`
& {
    display: flex;
    flex-direction: column;
    align-items: flex-start;
    justify-content: center;
    padding: 5px 15px;
}
`;

const StatusSelector = ({
    value,
    onSelect = () => {}
}) => {
    const { t } = useTranslation();
    const [closed, setClosed] = useState(value !== undefined ? value : true);
    const toggleClosed = () => {
        setClosed(!closed);
        return !closed;
    };

    return (
        <StatusOuter>
            <div>{t('lotstable.head.closed')}</div>
            <Checkbox 
                checked={closed}
                onChange={_ => {
                    onSelect(toggleClosed());
                }}
            />
        </StatusOuter> 
    );
};

const NoLots = ({
    owned = false,
    closed = false,
    error, 
    onReload = () => {}, 
    openModal = () => {},
    onStatusFilter = () => {}
}) => {
    const { t } = useTranslation();

    return (
        <NoLotsOuter>
            <NoLotsMessage>
                <Text>
                    {t('lotstable.nolots.message')}&nbsp;
                </Text>
                <Button mode="outline" size="mini" onClick={onReload}>{t('lotstable.nolots.buttons.reload')}</Button>
                {(owned && closed) &&
                    <div>
                        <Text>
                            {t('lotstable.nolots.ortry')}&nbsp;
                        </Text>
                        <Button mode="outline" size="mini" emphasis="negative" onClick={() => onStatusFilter(false)}>{t('lotstable.nolots.buttons.removestatus')}</Button>
                    </div>
                }
            </NoLotsMessage>
            {error &&
                <Notification 
                    type="error"
                    title={t('lotstable.nolots.error')}
                    message={error.message} 
                    details={error}
                    openModal={openModal}
                />
            }
        </NoLotsOuter>
    );
};

export default ({
    owned = false, 
    loader, 
    isLoading = true,
    uaxDetails,
    records = [],
    lotsParams = {},
    error,
    onReload = () => {},
    openModal = () => {},
    onSortLot = () => {},
    onSortPrice = () => {},
    onSortOne = () => {},
    onStatusFilter = () => {},
    onClose = () => {},
    onBuy = () => {}
}) => {
    const { t } = useTranslation();
    const [closed, setClosed] = useState(false);
    const onActionButtonClick = lot => {

        if (owned) {
            return onClose(lot.id);
        }

        onBuy(lot.id);
    };

    if (!isLoading && (!records || records.length === 0)) {
        return <NoLots {...{ owned, closed, error, onReload, openModal, onStatusFilter }}/>;
    }

    if (isLoading) {
        return loader;
    }

    return (
        <div>
            <Table>
                <thead>
                    <tr>
                        <th><TokensSelector /></th>
                        <th>
                            <AscDesc
                                value={lotsParams.value}
                                onChange={onSortLot}
                            >
                                {t('lotstable.head.lot')}
                            </AscDesc>                            
                        </th>
                        <th>
                            <AscDesc
                                value={lotsParams.price}
                                onChange={onSortPrice}
                            >
                                {t('lotstable.head.price')}<br />(UAX)
                            </AscDesc>
                        </th>
                        <th className="wrap">
                            <AscDesc
                                value={lotsParams.one}
                                onChange={onSortOne}
                            >
                                {t('lotstable.head.one')}
                            </AscDesc>                            
                        </th>
                        <th>
                            {owned &&
                                <StatusSelector 
                                    value={lotsParams.closed}
                                    onSelect={status => {
                                        setClosed(status);
                                        onStatusFilter(status);
                                    }}
                                />                                   
                            }
                            {!owned &&
                                <div style={{textAlign: 'center'}}><Link title={t('qa.questions.q8.title')} to="/qa/how-to-buy-tokens"><IconFundraising /></Link></div>
                            }
                        </th>                                               
                    </tr>
                </thead>
                <tbody>
                    {records.map((lot, i) => (
                        <tr key={i}>
                            <td>
                                <EtherAnchor
                                    network={config.network}
                                    type="token"
                                    address={lot[1].tokenDetails.address}
                                >
                                    <Text>{lot[1].tokenDetails.symbol}</Text>
                                </EtherAnchor>                                
                            </td>
                            <td>
                                <Text>{formatTokenBalance(lot[1].value, lot[1].tokenDetails.decimals)}</Text>
                            </td>
                            <td>
                                <Text>{formatTokenBalance(lot[1].price, uaxDetails.decimals)}</Text>
                            </td>
                            <td>
                                <Text>
                                    {perOne(lot[1].price, uaxDetails.decimals, lot[1].value, lot[1].tokenDetails.decimals)}
                                </Text>
                            </td>
                            <td>
                                {(owned && lot[1].closed) &&
                                    <StatusOuter>
                                        <Text size="small">{t('lotstable.labels.closed')}</Text>
                                    </StatusOuter> 
                                }
                                {(!owned || !lot[1].closed) &&
                                    <BuyButton 
                                        mode="text" 
                                        size="small"
                                        onClick={() => onActionButtonClick(lot[1])}
                                    >
                                        <Text size="small" className="label">{owned ? t('lotstable.buttons.close') : t('lotstable.buttons.buy')}</Text>
                                        {owned &&
                                            <IconRemove />
                                        }
                                        {!owned &&
                                            <IconArrowRight />   
                                        } 
                                    </BuyButton>     
                                }                                                  
                            </td>
                        </tr>
                    ))}
                </tbody>        
            </Table>
            <LotsPaginator owned={owned} />
        </div>
    );
};