import React from 'react';
import styled, { keyframes } from 'styled-components';
import { Text } from '@aragon/ui';

const PageOuter = styled.div`
& {
    position: relative;
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: center;
    height: 200px;
}
`;

const dotsAnimation = keyframes`
from {
    width: 1px;
    height: 1px;
}
to {
    width: 10px;
    height: 10px;
}
`;

const dotsAnimationSmall = keyframes`
from {
    width: 1px;
    height: 1px;
}
to {
    width: 5px;
    height: 5px;
}
`;

const Dot = styled.div`
& {
    background-color: ${({ color = '#757575' }) => color};
    border-radius: 50%;
    margin: ${({ size }) => (size === 'small' ? 2 : 5) }px;
    animation-name: ${dotsAnimation};
    animation-duration: 0.33s;
    animation-iteration-count: infinite;
    animation-direction: alternate;

    &.small {
        animation-name: ${dotsAnimationSmall};
    }

    &:nth-child(1) {
        animation-delay: 0.11s;
    }
    &:nth-child(2) {
        animation-delay: 0.21s;
    }
    &:nth-child(3) {
        animation-delay: 0.33s;
    }
}
`;

const parseHeightOrSize = ({ size = 'normal', height }) => {

    if (size) {
        return size === 'small' ? '10px' : '20px';
    }

    if (height) {
        return height;
    }
    
    return '';
};

const DotsOuter = styled.div`
& {
    display: flex;
    align-items: center;
    justify-content: center;
    height: ${parseHeightOrSize};
    overflow: hidden;
}
`;

const LabelOuter = styled.div`
& {
    display: flex;
    align-items: center;
    justify-content: center;
    width: ${({ width }) => width};
}
`;

const DotsLoader = props => {
    return (
        <div>
            <DotsOuter {...props} className="ZZZ">
                <Dot className={props.size || false} {...props} />
                <Dot className={props.size || false} {...props} />
                <Dot className={props.size || false} {...props} />                       
            </DotsOuter>
            {props.label &&
                <LabelOuter>
                    <Text size={props.size || 'normal'}>{props.label}</Text>
                </LabelOuter>                
            }
        </div>         
    );
};

export const PageLoader = props => (
    <PageOuter>
        <DotsLoader height="20px" color="#757575" {...props} />
    </PageOuter>
);

export default DotsLoader;

