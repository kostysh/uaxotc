import styled from 'styled-components';

export default styled.div`
& {
    padding: 30px;
    background-color: white; 
    border-top: 1px solid rgba(0,0,0,0.07);

    @media (max-width: 540px) {
        border-bottom: 1px solid rgba(0,0,0,0.07);
        border-top: none;   
    }
}
`;
