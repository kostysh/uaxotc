import en from './etherlink.en.json';
import ru from './etherlink.ru.json';
import uk from './etherlink.uk.json';

export default {
    en,
    ru,
    uk
};