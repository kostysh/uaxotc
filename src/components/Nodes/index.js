import React, { PureComponent, createRef } from 'react';
import styled from 'styled-components';

const Particle = function(canvas, context, dpi, colors, particleCount, particles) {
    this.radius = Math.round((Math.random() * 3) + 2);
    this.x = Math.floor((Math.random() * ((+getComputedStyle(canvas).getPropertyValue('width').slice(0, -2) * dpi) - this.radius + 1) + this.radius));
    this.y = Math.floor((Math.random() * ((+getComputedStyle(canvas).getPropertyValue('height').slice(0, -2) * dpi) - this.radius + 1) + this.radius));
    this.color = colors[Math.round(Math.random() * colors.length)];
    this.speedx = Math.round((Math.random() * 201) + 0) / 100;
    this.speedy = Math.round((Math.random() * 201) + 0) / 100;

    switch (Math.round(Math.random() * colors.length)) {
        case 1:
            this.speedx *= 1;
            this.speedy *= 1;
            break;
        case 2:
            this.speedx *= -1;
            this.speedy *= 1;
            break;
        case 3:
            this.speedx *= 1;
            this.speedy *= -1;
            break;
        case 4:
            this.speedx *= -1;
            this.speedy *= -1;
            break;
        default:
    }

    this.move = () => {    
        context.beginPath();
        context.globalCompositeOperation = 'source-over';
        context.fillStyle = this.color;
        context.globalAlpha = 1;
        context.arc(this.x, this.y, this.radius, 0, Math.PI * 2, false);
        context.fill();
        context.closePath();

        this.x = this.x + this.speedx;
        this.y = this.y + this.speedy;

        if (this.x <= 0 + this.radius) {
            this.speedx *= -1;
        }

        if (this.x >= canvas.width - this.radius) {
            this.speedx *= -1;
        }

        if (this.y <= 0 + this.radius) {
            this.speedy *= -1;
        }

        if (this.y >= canvas.height - this.radius) {
            this.speedy *= -1;
        }

        for (var j = 0; j < particleCount; j++) {
            let particleActuelle = particles[j];
            let yd = particleActuelle.y - this.y;
            let xd = particleActuelle.x - this.x;
            let d = Math.sqrt(xd * xd + yd * yd);

            if (d < 200) {
                context.beginPath();
                context.globalAlpha = (200 - d) / (200 - 0);
                context.globalCompositeOperation = 'destination-over';
                context.lineWidth = 1;
                context.moveTo(this.x, this.y);
                context.lineTo(particleActuelle.x, particleActuelle.y);
                context.strokeStyle = this.color;
                context.lineCap = 'round';
                context.stroke();
                context.closePath();
            }
        }
    };
};

const CanvasOuter = styled.div`
    & {
        overflow:hidden;
        position:relative;
        z-index: 1;
    }

    ${({ width }) => (width || width === 0 ? `width: ${width}px;` : 'width: auto;')}
    ${({ height }) => (height || height === 0 ? `height: ${height}px;` : 'height: auto;')}
`;

const Canvas = styled.canvas`
    & {
        width: 100%;
        height: 100%;
        margin: 0;
        z-index: 0;
    }
    
    ${({ opacity }) => (`opacity: ${opacity || 1};`)}    
`;

const Overlay = styled.div`
    & {
        position: absolute;
        top: 0px;
        left: 0px;
        z-index: 5;
    }
`;

//<Nodes height={550} opacity={0.5} particleCount={30}>
//</Nodes>

export default class Nodes extends PureComponent {

    constructor (props) {
        super(props);
        this.canvasRef = createRef();
        this.canvas = null;
        this.context = null;
        this.dpi = window.devicePixelRatio || 1;
        this.particleCount = props.particleCount || 50;
        this.particles = [];
        this.colors = ['#ffb374', '#e6ff74', '#5aecff', '#126e8a'];
        
        window.requestAnimFrame = window.requestAnimationFrame ||
                window.webkitRequestAnimationFrame ||
                window.mozRequestAnimationFrame ||
                window.oRequestAnimationFrame ||
                window.msRequestAnimationFrame ||
                (callback => window.setTimeout(callback, 1000 / 60));
    }

    fixDpi = () => {        
        let styleHeight = +getComputedStyle(this.canvas).getPropertyValue('height').slice(0, -2);
        let styleWidth = +getComputedStyle(this.canvas).getPropertyValue('width').slice(0, -2);    
        this.canvas.setAttribute('height', styleHeight * this.dpi);
        this.canvas.setAttribute('width', styleWidth * this.dpi);
    }

    animate = () => {
        try {
            this.fixDpi();
            this.context = this.canvas.getContext('2d');// sometimes context misses clearRect function so we should update context
            this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
        
            for (var i = 0; i < this.particleCount; i++) {
                this.particles[i].move();
            }

            window.requestAnimFrame(this.animate.bind(this));
        } catch(err) {
            console.log(err);
            console.log(this.context, typeof this.context.clearRect);
            console.log(this.canvas);
        }        
    };

    componentDidMount = () => {
        this.canvas = this.canvasRef.current;
        this.context = this.canvas.getContext('2d');
        this.context.scale(this.dpi, this.dpi);

        for (var i = 0; i < this.particleCount; i++) {
            this.fixDpi();
            this.particles.push(new Particle(this.canvas, this.context, this.dpi, this.colors, this.particleCount, this.particles));
        }

        this.animate();
    }

    render() {
        return (
            <CanvasOuter {...this.props}>
                <Overlay>
                    {this.props.children}
                </Overlay>
                <Canvas ref={this.canvasRef} {...this.props} />
            </CanvasOuter>            
        );
    }
}