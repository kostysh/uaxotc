import React, { Component } from 'react';
import { Spring, animated } from 'react-spring/renderprops';
import styled from 'styled-components';
import { Button } from '@aragon/ui';
import { useTranslation } from 'react-i18next';

// Custom components
import { Grid, Row } from '../Grid';

const Label = styled.div`
    & {
        margin-right: 5px;
    }
`;

const Buttons = styled.div`
    & {
        display:flex;
        flex-direction: row;

        button {
            &:first-child {
                margin-right: 5px;
            }
        }
    }
`;

const SureBlock = ({ hideSure, doAction, action, data }) => {
    const { t } = useTranslation();

    return (
        <Grid>
            <Row justify="end" align="center">
                <Label size="normal">{t('notifications.center.header.button.sure.label')}</Label>
                <Buttons>
                    <Button 
                        mode="outline" 
                        emphasis="negative"
                        onClick={hideSure}
                    >{t('notifications.center.header.button.sure.cancel')}</Button>
                    <Button 
                        mode="strong" 
                        emphasis="positive"
                        onClick={e => doAction(e, action, data)}
                    >{t('notifications.center.header.button.sure.ok')}</Button>
                </Buttons>
            </Row>
        </Grid>
    );
};

export default class SureButton extends Component {
    
    constructor(props) {
        super(props);

        this.state = {
            isActive: props.isActive || false,
            shureBlock: props.isActive || false
        };
    }

    onAnimationEnd = () => {

        if (!this.state.isActive) {

            this.setState({
                shureBlock: false
            });
        }
    };

    showSure = e => {
        e.preventDefault();
        this.setState({
            isActive: true,
            shureBlock: true
        });
        this.props.callback(true);
    };

    hideSure = e => {
        e.preventDefault();
        this.setState({
            isActive: false
        });
        this.props.callback(false);
    };

    doAction = (e, action, data = []) => {
        action.apply(this, data);
        this.hideSure(e);
    };

    render() {
        const { isActive, shureBlock } = this.state;
        const { mode, emphasis, action = () => {}, data, disabled, children } = this.props;

        return (
            <div>
                <Spring
                    from={{
                        opacity: isActive ? 0 : 1,
                        display: isActive ? 'none': 'block'
                    }}
                    to={{
                        opacity: isActive ? 1 : 0,
                        display: isActive ? 'block' : 'none'
                    }} 
                    onRest={this.onAnimationEnd} 
                    config={{
                        duration: isActive ? 350 : 100
                    }}
                    native
                >
                    {props => (
                        <animated.div style={props}>
                            <SureBlock 
                                hideSure={this.hideSure} 
                                doAction={this.doAction} 
                                action={action} 
                                data={data} 
                            />                              
                        </animated.div>
                    )}
                </Spring>
                {(!isActive && !shureBlock) && 
                    <Button 
                        mode={mode} 
                        emphasis={emphasis} 
                        onClick={this.showSure} 
                        disabled={disabled}
                    >{children}</Button>
                }                 
            </div>
        );
    }
}

