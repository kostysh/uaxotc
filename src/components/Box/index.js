import styled from 'styled-components';
import {
    // core
    space,
    color,
    // layout
    display,
    width,
    maxWidth,
    minWidth,
    height,
    maxHeight,
    minHeight,
    size,
    verticalAlign,
    // flexbox
    flex,
    flexBasis,
    flexDirection,
    flexWrap,
    order,
    alignSelf,
    alignItems,
    alignContent,
    justifySelf,
    justifyItems,
    justifyContent,
    // typography
    fontSize,
    fontFamily,
    fontWeight,
    fontStyle,
    textAlign,
    lineHeight,
    letterSpacing,
    // background
    background,
    backgroundImage,
    backgroundSize,
    backgroundPosition,
    backgroundRepeat,
    // misc/border
    borders,
    borderColor,
    borderRadius,
    // misc
    boxShadow,
    opacity,
    overflow,
    // position
    position,
    zIndex,
    top,
    right,
    bottom,
    left,
} from 'styled-system';

export default styled.div`
    & {
        box-sizing: border-box;
    }
    ${'' /* core */}
    ${space}
    ${color}
    ${width}
    ${fontSize}
    ${'' /* layout */}
    ${display}
    ${maxWidth}
    ${minWidth}
    ${height}
    ${maxHeight}
    ${minHeight}
    ${size}
    ${verticalAlign}
    ${'' /* flexbox */}
    ${flex}
    ${flexBasis}
    ${flexDirection}
    ${flexWrap}
    ${order}
    ${alignSelf}
    ${alignItems}
    ${alignContent}
    ${justifySelf}
    ${justifyItems}
    ${justifyContent}
    ${'' /* position */}
    ${position}
    ${zIndex}
    ${top}
    ${right}
    ${bottom}
    ${left}
    ${'' /* background */}
    ${background}
    ${backgroundImage}
    ${backgroundSize}
    ${backgroundPosition}
    ${backgroundRepeat}
    ${'' /* borders */}
    ${borders}
    ${borderColor}
    ${borderRadius}
    ${'' /* typography */}
    ${textAlign}
    ${fontFamily}
    ${fontWeight}
    ${fontStyle}
    ${lineHeight}
    ${letterSpacing}
    ${'' /* misc */}
    ${opacity}
    ${boxShadow}
    ${overflow}
`;