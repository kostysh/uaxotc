import en from './txcard.en.json';
import ru from './txcard.ru.json';
import uk from './txcard.uk.json';

export default {
    en,
    ru,
    uk
};