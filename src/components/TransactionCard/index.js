import React, { Component } from 'react';
import styled from 'styled-components';
import { Text, Button } from '@aragon/ui';
import { withTranslation } from 'react-i18next';

// System imports
import config from '../../config';

// Custom components
import Box from '../../components/Box';
import Loader from '../../components/Loader';
import EtherLink from '../../components/EtherLink';

const TransactionCardOuter = styled(Box)`
& {
    display: flex;
    flex-direction: column;
    align-items: stretch;
    background-color: #FFF9EB;
    padding: 20px;
    margin-bottom: 20px;
}
`;

const TxHeaderOuter = styled.div`
& {
    margin-bottom: 10px;
}
`;

const TxInfo = styled.ul`
& {
    padding-left: 20px;
    margin-bottom: 10px;
}
`;

const TxActionBlock = styled.div`
& {
    display: flex;
    flex-direction: row;
    flex-wrap: nowrap;
    align-items: flex-start;

    @media (max-width: 540px) {
        flex-direction: column;
        align-items: center;
    }
}
`;

const EstimationBlock = styled.div`
& {
    display: flex;
    flex-direction: column;
    align-items: stretch;
    flex-grow: 1;
    padding-right: 10px;

    @media (max-width: 540px) {
        align-items: center;
        margin-bottom: 10px;        
    }
}
`;

const TxAction = styled.div`
& {
    display: flex;
    align-items: center;
    justify-content: center;
    flex-grow: 0;
}
`;

const LoaderOuter = styled.div`
& {
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
}
`;

const SuccessOuter = styled.div`
& {
    
}
`;

class TransactionCard extends Component {

    constructor(props) {
        super(props);

        this.state = {
            gasEstimation: 0,
            estimationLoading: false,
            loading: false,
            transactionHash: false,
            confirmations: 0,
            transaction: false,
            receipt: false,
            ...(props.savedState || {}),
            ...{
                loading: false
            },
            error: false
        };
    }

    getGasEstimation = async (isReset = false) => {
        const { action, options, resetOptions, onError = () => {} } = this.props.estimationConfig;
        
        try {
            this.setState({
                error: false,
                estimationLoading: true
            });
            const estimationOptions = isReset ? resetOptions : options;
            const estimation = await action.apply(this, estimationOptions);
            this.setState({
                estimationLoading: false,
                gasEstimation: window.web3.utils.fromWei(estimation.toString(), 'ether')
            });
        } catch(error) {
            onError(error);
            this.setState({
                estimationLoading: false,
                error
            });
        }
    };

    onSendAction = (isReset = false) => {
        const {
            action, 
            resetAction, 
            options, 
            onError = () => {}, 
            onSuccess = () => {}, 
            onReset = () => {} 
        } = this.props.transactionConfig;
        
        try {
            this.setState({
                error: false,
                loading: true,
                transactionHash: false,
                confirmations: 0,
                receipt: false
            });

            const txAction = isReset ? resetAction : action;

            txAction.apply(this, options)
                .on('receipt', receipt => {
                    this.setState({
                        receipt
                    });
                })
                .on('transactionHash', transactionHash => {
                    this.setState({
                        transactionHash
                    });
                })
                .on('confirmations', confirmations => {
                    this.setState({
                        confirmations
                    });
                })
                .on('transaction', transaction => {
                    this.setState({
                        transaction
                    });
                })                
                .then(() => {

                    this.setState({
                        loading: false
                    });
                    
                    if (!isReset) {

                        if (resetAction) {
                            this.getGasEstimation(true);
                        }
                        
                        onSuccess(this.state);
                    } else {
                        this.getGasEstimation();
                        this.setState({
                            transactionHash: false,
                            transaction: false,
                            confirmations: 0,
                            receipt: false
                        });
                        onReset(this.state);
                    }
                })
                .catch(error => {
                    this.setState({
                        loading: false,
                        error
                    });
                    onError(error);
                });

        } catch(error) {
            this.setState({
                loading: false,
                error
            });
            onError(error);
        }
    };

    componentDidMount = () => {
        this.getGasEstimation(!!this.props.resetAction);
    };

    render() {
        const { estimationLoading, loading, gasEstimation, 
            transaction, transactionHash, confirmations,
            error } = this.state;
        const { t, header, info = [] } = this.props;
        const { resetAction } = this.props.transactionConfig;

        return (
            <TransactionCardOuter>
                <TxHeaderOuter>
                    {header}
                </TxHeaderOuter>
                <TxInfo>
                    {info.map((i, index) => (
                        <li key={index}><Text size="normal">{t(i)}</Text></li>
                    ))}
                </TxInfo>
                <TxActionBlock>
                    <EstimationBlock onClick={() => this.getGasEstimation(!!resetAction)}>
                        <div><Text size="small">{t('txcard.estimation.title')}:</Text></div>
                        <div>
                            {estimationLoading && 
                                <Loader size="small" />
                            }
                            {!estimationLoading &&
                                <Text size="strong">{gasEstimation} ETH</Text>
                            }
                        </div>
                    </EstimationBlock>
                    <TxAction>
                        <div>
                            {(!loading && transaction && !resetAction) &&
                                <SuccessOuter>
                                    <Text size="xlarge">{t('txcard.common.success')}</Text>
                                </SuccessOuter>
                            }
                            {(transaction && resetAction) &&
                                <Button 
                                    mode={loading || estimationLoading || error ? 'outline' : 'strong' } 
                                    emphasis={loading ? undefined : 'negative' } 
                                    disabled={loading || estimationLoading || error}
                                    onClick={() => this.onSendAction(true)}
                                >
                                    {!loading &&
                                        <div>{t('txcard.buttons.reset')}</div>
                                    }
                                    {loading &&
                                        <LoaderOuter>
                                            <Loader size="small" /> 
                                            <Text size="small">{t('txcard.transaction.loader.confirmations')}: {confirmations}</Text>
                                            {transactionHash &&
                                                <EtherLink network={config.network} address={transactionHash} type="tx" short={true} />
                                            }
                                        </LoaderOuter>
                                    }
                                </Button>
                            }
                            {!transaction &&
                                <Button 
                                    mode={loading || estimationLoading || error ? 'outline' : 'strong' } 
                                    emphasis={loading ? undefined : 'positive' } 
                                    disabled={loading || estimationLoading || error}
                                    onClick={() => this.onSendAction()}
                                >
                                    {!loading &&
                                        <div>{t('txcard.buttons.send')}</div>
                                    }
                                    {loading &&
                                        <LoaderOuter>
                                            <Loader size="small" /> 
                                            <Text size="small">{t('txcard.transaction.loader.confirmations')}: {confirmations}</Text>
                                            {transactionHash &&
                                                <EtherLink network={config.network} address={transactionHash} type="tx" short={true} />
                                            }
                                        </LoaderOuter>
                                    }
                                </Button>
                            }
                        </div>                        
                    </TxAction>
                </TxActionBlock>
            </TransactionCardOuter>
        );
    }    
};

export default withTranslation()(TransactionCard);