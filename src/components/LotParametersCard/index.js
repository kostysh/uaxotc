import React from 'react';
import styled from 'styled-components';
import { Text } from '@aragon/ui';
import { useTranslation } from 'react-i18next';

// System imports
import { formatTokenBalance } from '../../utils/tokens.helpers';

// Custom components
import Box from '../Box';

const LotParametersOuter = styled(Box)`
& {
    padding: 30px;
    background-color: white; 
    border-top: 1px solid rgba(0,0,0,0.07);   

    p {
        margin-bottom: 10px;
        overflow: hidden;

        &:last-child {
            margin-bottom: 0;
        }
    }

    @media (max-width: 540px) {
        border-bottom: 1px solid rgba(0,0,0,0.07);
        border-top: none;   
    }
}
`;

export default ({ 
    lotValue, 
    lotPrice, 
    tokenDetails, 
    uaxDetails,
    serviceFee
}) => {
    const { t } = useTranslation();
    return (
        <LotParametersOuter>
            <Text.Paragraph>
                <Text size="large">
                    <strong>{t('create.wizard.forms.fields.labels.lotvalue')}:</strong> {formatTokenBalance(lotValue, 0)} {tokenDetails.symbol}
                </Text>
            </Text.Paragraph>
            <Text.Paragraph>
                <Text size="large">
                    <strong>{t('create.wizard.forms.fields.labels.lotprice')}:</strong> {formatTokenBalance(lotPrice, 0)} {uaxDetails.symbol}
                </Text>
            </Text.Paragraph>
            <Text.Paragraph>
                <Text size="large">
                    <strong>{t('create.wizard.forms.fields.labels.servicefee')}:</strong> {formatTokenBalance(serviceFee, uaxDetails.decimals)} {uaxDetails.symbol}
                </Text>
            </Text.Paragraph>
            <Text.Paragraph>
                <Text size="large">
                    <strong>{t('create.wizard.forms.fields.labels.youwillget')}:</strong> {formatTokenBalance(lotPrice - (serviceFee / Math.pow(10, uaxDetails.decimals)), 0)} {uaxDetails.symbol}
                </Text>
            </Text.Paragraph>
        </LotParametersOuter>        
    );
};