import en from './wallet.en.json';
import ru from './wallet.ru.json';
import uk from './wallet.uk.json';

export default {
    en,
    ru,
    uk
};