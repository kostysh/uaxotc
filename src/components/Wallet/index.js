import React, { Component } from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import { Text, IconWallet, Toast } from '@aragon/ui';
import { withTranslation } from 'react-i18next';

// System imports
import * as selectors from '../../store/selectors';
import { formatTokenBalance } from '../../utils/tokens.helpers';
import { isFirefoxMobile } from '../../utils/environment';

const WalletOuter = styled.div`
    & {
        display: flex;
        flex-direction: row;
        align-items: center;
        height: 100%;
    }
`;

const IconOuter = styled.div`
    & {
        margin-right: 5px;
        line-height: 1;
    }

    ${({ withError }) => (withError ? `
        & {
            svg g {
                path {
                    &:last-child {
                        fill: red;
                    }
                }
            }
        }    
    ` : '')}
`;

class Wallet extends Component {

    onWalletClick = toast => {
        const { web3Error, uaxError } = this.props;

        if (web3Error && !isFirefoxMobile) {

            toast(web3Error);
        }

        if (uaxError && !isFirefoxMobile) {

            toast(uaxError);
        }
    }

    render() {
        const { t, web3Error, uaxError, uaxBalance, uaxDetails } = this.props;
        const isError = web3Error || uaxError;

        return (
            <Toast>
                {toast => (
                    <WalletOuter title={isError ? t('wallet.title.errors') : ''} onClick={() => this.onWalletClick(toast)}>                
                        <IconOuter withError={isError}>
                            <IconWallet width={26} height={26} />
                        </IconOuter>                
                        <Text size="small" color={isError ? 'red' : ''} >
                            {formatTokenBalance(uaxBalance, uaxDetails.decimals)} UAX
                        </Text>               
                    </WalletOuter>
                )}
            </Toast>
        );
    }
};

function mapStateToProps(state) {

    return {
        web3Error: selectors.web3Error(state),
        uaxError: selectors.uaxError(state),
        uaxBalanceFetching: selectors.uaxBalanceFetching(state),
        uaxBalance: selectors.uaxBalance(state),
        uaxDetails: selectors.uaxDetails(state)
    };
}

const mapDispatchToProps = dispatch => {

    return {
        
    };
};

const connectedWallet = withTranslation()(connect(mapStateToProps, mapDispatchToProps)(Wallet));

export default connectedWallet;

