import styled from 'styled-components';

export default styled.table`
    & {
        border-collapse: collapse;
        width: 100%; 
        z-index: 1000;       
    }

    th,
    td {
        border: solid;
        border-width: 1px 0;
        border-color: rgba(0,0,0,0.15);
        border-top: none;
        text-align: left;
        padding: 2px 5px;
        width: auto;

        &:first-child {
            width: 1px;
        }

        &:last-child {
            width: 1px;
        }

        &.wrap {
            white-space: pre-wrap;
        }
    }

    th {
        line-height: 1;

        &:nth-child(2) {
            padding-left: 15px;
        }
    }

    tr {
        &:hover {
            background-color: rgba(0,0,0,0.02);
        }

        &:last-child {

            td {
                border-bottom: none;
            }            
        }
    }

    tbody {
        td {
            &:first-child {
                padding-left: 20px;
            }
        }
    }
`;
