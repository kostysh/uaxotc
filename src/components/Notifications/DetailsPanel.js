import React from 'react';
import styled from 'styled-components';
import { Card } from '@aragon/ui';
import { stringifyCircular } from '../../utils/json';

// Custom components
import Markdown from '../Markdown';

const DetailsCard = styled(Card)`
${({ background }) => ( background ? `background: ${background}` : '')};
& {
    overflow-y: auto;
    width: 100vw;
    height: 100vh;
    padding: 2em 3em;

    @media (max-width: 540px) {
        padding: 1em;
    }
}
`;

export default ({ title, message, details }) => {
    const content = typeof details === 'object' ? `\`\`\`json\n${stringifyCircular(details, 2)}\n\`\`\`` : '{}';
    return (
        <DetailsCard>
            <h1>{title}</h1>
            <Markdown>
                {message}
            </Markdown>
            <Markdown stripSpaces={false}>
                {content}
            </Markdown>
        </DetailsCard>        
    );
};

