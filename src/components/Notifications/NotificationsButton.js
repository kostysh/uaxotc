import React, { Component } from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import { Button, IconNotifications, IconAttention, IconError } from '@aragon/ui';

// System imports
import * as actions from '../../store/actions';
import * as selectors from '../../store/selectors';

const StyledButton = styled(Button)`
    & {
        @media (max-width: 375px) {
            padding-right: 0;
        }
    }
`;

const IconOuter = styled.div`
    & {
        justify-content: center;
        display: flex;        
    }
`;

class NotificationsButton extends Component {

    render() {

        const { seenNotifications, hasErrors, width = 28, height = 28 } = this.props;

        return (
            <StyledButton mode="text" onClick={this.props.toggleNotificationsCenter}>
                <IconOuter>
                    {(!seenNotifications && !hasErrors) &&
                        <IconAttention width={width} height={height} />
                    }
                    {(!seenNotifications && hasErrors) &&
                        <IconError width={width} height={height} />
                    }
                    {seenNotifications &&
                        <IconNotifications width={width} height={height} />
                    }
                </IconOuter>                
            </StyledButton>
        );
    }
};

function mapStateToProps(state) {

    return {
        seenNotifications: selectors.appSeenNotifications(state),
        hasErrors: selectors.appNotificationsHasErrors(state)
    };
}

const mapDispatchToProps = dispatch => {

    return {
        toggleNotificationsCenter: () => dispatch(actions.appNotifcationsToggle())
    };
};

const connectedNotificationsButton = connect(mapStateToProps, mapDispatchToProps)(NotificationsButton);

export default connectedNotificationsButton;