import React, { Component } from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import { withTranslation } from 'react-i18next';

import './Notifications.scss';

// aragon-ui components
import { SidePanel, Button, Text } from '@aragon/ui';

// System imports
import * as actions from '../../store/actions';
import * as selectors from '../../store/selectors';
import { toJsonFile } from '../../utils/export';

// Custom components
import { Grid, Row, Column } from '../Grid';
import SureButton from '../SureButton';
import Notification from './Notification';
import { ModalContext } from '../Modal';

const InfoOuter = styled.div`
    & {
        margin-top: 1em;
        section {
            margin-bottom: 1em;
        }
        h1 {
            font-size: 1.1em;
        }
        h2 {
            font-size: 1em;
        }
        h3 {
            font-size: 0.9em;
        }
    }
`;

const StyledRow = styled(Row)`
    & {
        @media (max-width: 540px) {
            button {
                transform: scale(0.95);
            }
        }
        
    }
`;

class NotificationsCenter extends Component {

    state = {
        shureButtonActive: false
    };

    onExportJson = data => toJsonFile(data);

    render() {
        const { shureButtonActive } = this.state;
        const { t, opened, notifications } = this.props;

        return (
            <div className="notifications-sidebar">
                <SidePanel 
                    opened={opened} 
                    title={t('notifications.center.header.title')} 
                    onClose={this.props.toggleNotificationsCenter}
                    {...this.props}
                >
                    {notifications.length > 0 &&
                        <Grid>
                            <StyledRow>
                                <Column flex={0}>
                                    <div>
                                        {!shureButtonActive && 
                                            <Button 
                                                className="zzz"
                                                mode="outline" 
                                                emphasis="positive" 
                                                onClick={() => this.onExportJson(notifications)} 
                                                disabled={notifications.length === 0}
                                            >{t('notifications.center.header.button.export')}</Button>
                                        }                                        
                                    </div>
                                </Column>
                                <Column flex={1} textAlign="right">
                                    <SureButton 
                                        mode="outline" 
                                        emphasis="negative" 
                                        action={this.props.clearNotifications} 
                                        callback={isActive => this.setState({shureButtonActive: isActive})}
                                        disabled={notifications.length === 0}
                                    >{t('notifications.center.header.button.clear')}</SureButton>          
                                </Column>
                            </StyledRow>                    
                        </Grid>
                    }

                    {notifications.length === 0 &&
                        <Column align="center" justify="center" textAlign="center" >
                            <Text size="normal">{t('notifications.center.body.label.empty')}</Text>
                        </Column>
                    }

                    <InfoOuter>
                        {notifications.length > 0 &&
                            notifications.map((record, index) => (
                                <Notification
                                    key={index}
                                    openModal={this.context.openModal}
                                    {...record}
                                />
                            ))
                        }
                    </InfoOuter>
                </SidePanel>
            </div>            
        );
    }
};

NotificationsCenter.contextType = ModalContext;

function mapStateToProps(state) {

    return {
        notifications: selectors.appNotifications(state)
    };
}

const mapDispatchToProps = dispatch => {

    return {
        toggleNotificationsCenter: () => dispatch(actions.appNotifcationsToggle()),
        clearNotifications: () => dispatch(actions.appNotificationsClear())
    };
};

const connectedNotificationsCenter = connect(mapStateToProps, mapDispatchToProps)(NotificationsCenter);

export default withTranslation()(connectedNotificationsCenter);
