import React from 'react';
import styled from 'styled-components';
import { Info, Text, Button, IconAttention, IconError, IconCheck } from '@aragon/ui';
import { useTranslation } from 'react-i18next';

// Custom components
import { Grid, Row, Column } from '../Grid';
import DetailsPanel from './DetailsPanel';
import { MarkdownCompact } from '../Markdown';

const Header = styled.div`
    & {
        margin-bottom: 5px;
    }
`;

const Icon = styled.span`
    & {
        margin-right: 10px;
        display: flex;
    }
`;

const parseDate = date => String(date).slice(0, 16).replace(/[T]+/gi, ' ');

export default ({ openModal = () => {}, children, type, title, date, message, details }) => {
    let icon;
    let background;
    const { t } = useTranslation();

    switch (type) {
        case 'warn':
            icon = (
                <IconAttention />
            );
            background = 'hsl(43, 100%, 96%)';
            break;

        case 'error':
            icon = (
                <IconError />
            );
            background = 'rgba(252, 79, 67, 0.6)';
            break;

        case 'info':            
        default:
            icon = (
                <IconCheck />
            );
            break;
    }

    return (
        <Info background={background}>
            <Header>
                <Grid>
                    <Row>
                        <Column flex={0}>
                            {icon && <Icon>{icon}</Icon>}        
                        </Column>
                        <Column flex={6}>
                            <h2>{title}</h2>        
                        </Column>
                        {date &&
                            <Column flex={3} textAlign="right">
                                <Text size="xsmall" style={{opacity: 0.5}}>{parseDate(date)}</Text>
                            </Column>
                        }                        
                    </Row>
                </Grid>
            </Header>
            {message &&
                <MarkdownCompact linkTarget="_blank" stripSpaces={true}>
                    {message}
                </MarkdownCompact>
            }            
            {details &&
                <Grid>
                    <Row>
                        <Column textAlign="right">
                            <div>
                                <Button 
                                    size="mini" 
                                    onClick={_ => openModal((
                                        <DetailsPanel 
                                            title={title} 
                                            message={message} 
                                            details={details}>                                                            
                                        </DetailsPanel>
                                    ))}
                                >{t('notifications.center.card.button.details')}</Button>
                            </div>                                                                    
                        </Column>
                    </Row>
                </Grid>
            }
            {children}
        </Info>
    );
};
