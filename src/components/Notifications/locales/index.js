import en from './notifications.en.json';
import ru from './notifications.ru.json';
import uk from './notifications.uk.json';

export default {
    en,
    ru,
    uk
};