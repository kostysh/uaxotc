import React from 'react';
import styled from 'styled-components';
import { useTranslation } from 'react-i18next';

const InfoTextOuter = styled.ul`
& {
    padding: 2px 30px 20px 0;
    margin-left: 15px;

    @media (max-width: 540px) {
        padding: 2px 30px 20px 30px;
    }
}

span {
    line-height: 1.5;
    font-size: 20px;

    @media (max-width: 570px) {
        font-size: 16px;
    }
}
`;

const InfoText = ({ lines = [] }) => {
    const { t } = useTranslation();

    return (
        <InfoTextOuter>
            {lines.map((line, index) => (
                <li key={index}>
                    <span>{t(line)}</span>
                </li>
            ))}
        </InfoTextOuter>
    );
};

export default InfoText;