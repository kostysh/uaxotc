import en from './closecard.en.json';
import ru from './closecard.ru.json';
import uk from './closecard.uk.json';

export default {
    en,
    ru,
    uk
};