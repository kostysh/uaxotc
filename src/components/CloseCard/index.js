import React, { Component } from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import { withTranslation } from 'react-i18next';
import { Text } from '@aragon/ui';

// System imports
import config from '../../config';
import * as selectors from '../../store/selectors';
import * as actions from '../../store/actions';
import { ModalContext } from '../Modal';
import { formatTokenBalance } from '../../utils/tokens.helpers';
import { getLotDetails, estimateMarketMethod, removeLot } from '../../store/services/market';

// Custom components
import { PageBar, Page } from '../Layout';
import Loader from '../Loader';
import { Notification } from '../Notifications';
import TransactionCard from '../TransactionCard';
import { etherLink } from '../EtherLink';

const CloseCardOuter = styled.div`
& {
    width: 100vw;
    height: 100vh;
    background-color: white;
    overflow-y: auto;

    @media (max-width: 540px) {
        
    }
}
`;

const LotDetailsOuter = styled.div`
& {
    @media (max-width: 540px) {
        padding: 20px;    
    }
}
`;

const RemovalTxOuter = styled.div`
& {
    display: flex;
    flex-direction: column;
    align-items: flex-start;
    margin-top: 20px;
    max-width: 60%;

    @media (max-width: 540px) {
        max-width: 100%;
    }
}
`;

class CloseCard extends Component {

    state = {
        loading: true,
        lot: false,
        error: false
    };

    setStateAsync = state => new Promise(resolve => this.setState(state, resolve));

    fetchLotDetails = async () => {
        const { t, lotId } = this.props;

        try {

            await this.setStateAsync({
                loading: true,
                error: false
            });
    
            const lot = await getLotDetails(lotId);
    
            await this.setStateAsync({
                loading: false,
                lot                
            });
        } catch(error) {

            this.setState({
                loading: false,
                lot: false,
                estimation: 0,
                error
            });

            this.props.notificationAdd({
                type: 'error',
                title: t('closecard.errors.details'),
                date: new Date().toISOString(),
                message: error.message,
                details: {
                    error
                }
            });
        }
    };

    componentDidMount = async () => {
        const { lot } = this.state;
        
        if (!lot) {

            await this.fetchLotDetails();
        }
    };

    render () {
        const { loading, lot, error } = this.state;
        const { t, uaxDetails, lotId, children } = this.props;

        return (
            <CloseCardOuter>
                <PageBar
                    title={t('closecard.title')}
                >
                    {loading &&
                        <Loader />
                    }
                    {lot &&
                        <Text>
                            {formatTokenBalance(lot.value, lot.tokenDetails.decimals)}&nbsp;
                            {lot.tokenDetails.symbol}
                        </Text>
                    }
                </PageBar>
                <Page>
                    {loading &&
                        <Loader
                            label={t('closecard.labels.loading')}
                        />
                    }
                    {lot &&
                        <LotDetailsOuter>
                            <Text.Paragraph>
                                <Text size="large">
                                    <strong>{t('create.wizard.forms.fields.labels.lotvalue')}:</strong> {formatTokenBalance(lot.value, lot.tokenDetails.decimals)} {lot.tokenDetails.symbol}
                                </Text>
                            </Text.Paragraph>
                            <Text.Paragraph>
                                <Text size="large">
                                    <strong>{t('create.wizard.forms.fields.labels.lotprice')}:</strong> {formatTokenBalance(lot.price, uaxDetails.decimals)} {uaxDetails.symbol}
                                </Text>
                            </Text.Paragraph>
                            <Text.Paragraph>
                                <Text size="large">
                                    <strong>{t('create.wizard.forms.fields.labels.servicefee')}:</strong> {formatTokenBalance(lot.fee, uaxDetails.decimals)} {uaxDetails.symbol}
                                </Text>
                            </Text.Paragraph>
                            <RemovalTxOuter>
                                <TransactionCard 
                                    header={
                                        <h2>
                                            {t('closecard.tx.title')}
                                        </h2>
                                    }
                                    info={[
                                        'closecard.tx.info.0',
                                        'closecard.tx.info.1',
                                        'closecard.tx.info.2',
                                        'closecard.tx.info.3',
                                        'closecard.tx.info.4',
                                    ]} 
                                    estimationConfig={{
                                        action: estimateMarketMethod,
                                        options: [
                                            'remove',
                                            [
                                                lotId
                                            ]
                                        ],
                                        onError: error => {
                                            error.title = t('closecard.labels.estimation');
                                            this.setState({
                                                error
                                            });
                                            this.props.notificationAdd({
                                                type: 'error',
                                                title: t('closecard.notifications.remove', {
                                                    title: t('closecard.labels.estimation')
                                                }),
                                                date: new Date().toISOString(),
                                                message: t('closecard.errors.estimation'),
                                                details: {
                                                    error
                                                }
                                            });
                                        }
                                    }}
                                    transactionConfig={{
                                        action: removeLot,
                                        options: [
                                            lotId
                                        ],
                                        onError: error => {
                                            error.title = t('closecard.notifications.title');
                                            this.setState({
                                                error
                                            });
                                            this.props.notificationAdd({
                                                type: 'error',
                                                title: t('closecard.notifications.remove', {
                                                    title: t('closecard.labels.removal')
                                                }),
                                                date: new Date().toISOString(),
                                                message: t('closecard.errors.removal'),
                                                details: {
                                                    error
                                                }
                                            });
                                        },
                                        onSuccess: state => {
                                            this.setState({
                                                removed: true
                                            });
                                            this.props.notificationAdd({
                                                type: 'info',
                                                title: t('closecard.notifications.remove', {
                                                    title: t('closecard.labels.removal')
                                                }),
                                                date: new Date().toISOString(),
                                                message: t('closecard.notifications.success', {
                                                    etherLink: etherLink(config.network, 'tx', state.transactionHash)
                                                }),
                                                details: {
                                                    receipt: state.receipt,
                                                    transaction: state.transaction
                                                }
                                            });
                                            this.props.mylotsRefresh();
                                            this.context.closeModal();
                                        }
                                    }}                                        
                                />
                            </RemovalTxOuter>                            
                        </LotDetailsOuter>
                    }
                    {error &&
                        <Notification 
                            type="error"
                            title={`Error occured: ${error.title}`}
                            message={error.message} 
                        >
                            <Text weight="bold">{t('closecard.labels.details')}</Text>
                        </Notification>
                    }                    
                    {children}
                </Page>                
            </CloseCardOuter>
        );
    }
}

CloseCard.contextType = ModalContext;

function mapStateToProps(state) {

    return {
        uaxDetails: selectors.uaxDetails(state)
    };
}

const mapDispatchToProps = dispatch => {

    return {
        notificationAdd: record => dispatch(actions.appNotificationAdd(record)),
        mylotsRefresh: () => dispatch(actions.mylotsRefresh())
    };
};

const connectedCloseCard=withTranslation()(connect(mapStateToProps, mapDispatchToProps)(CloseCard));

export default connectedCloseCard;
