import React from 'react';
import ReactMarkdown from 'react-markdown';
import styled from 'styled-components';

const ParsedMarkdown = ({ stripSpaces = true, source, children, ...props }) => {
    source = source ? source : children;

    if (stripSpaces) {
        source = source.replace(/^\s+/gm, '');
        children = undefined;
    }

    return (
        <ReactMarkdown source={source} {...props} />
    );
};

export const Markdown = styled(ParsedMarkdown)`
    code {
        padding: 0 3px;
        border-radius: 5px;
        color: hsl(0, 0%, 44%);
        background: hsl(180, 5%, 13%);
        white-space: pre-wrap;
    }
    pre {
        padding: 20px;
        border-radius: 5px;
        border: 1px solid rgba(0,0,0,0.5);
        overflow: hidden;
        word-break: break-all;
    }
    pre code {
        color: inherit;
        background: inherit;        
    }
    h2 code,
    h3 code {
        padding: 0;
        background: none;
    }
    p,
    ul {
        list-style-position: inside;
        line-height: 2;
        margin-bottom: 30px;
    }
`;

export const MarkdownCompact = styled(Markdown)`
    h1 {
        font-size: 1.1em;
        font-weight: bold;
    }
    h2 {
        font-size: 1em;
        font-weight: bold;
    }
    h3 {
        font-size: 0.9em;
    }
    code {
        padding: 0 3px;
        border-radius: 3px;
        color: hsl(0, 0%, 44%);
        background: hsl(180, 5%, 13%);
        white-space: pre-wrap;
    }
    pre {
        padding: 3px;
        border-radius: 3px;
        border: 1px solid rgba(0,0,0,0.5);
        overflow: hidden;
    }
    pre code {
        color: inherit;
        background: inherit;
    }
    h2 code,
    h3 code {
        padding: 0;
        background: none;
    }
    p,
    ul {
        list-style-position: inside;
        line-height: 1.2;
        margin-bottom: 3px;
    }
`;

export default Markdown;

