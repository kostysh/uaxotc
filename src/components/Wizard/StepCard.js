import React from 'react';
import styled from 'styled-components';
import { Text, Button, IconError } from '@aragon/ui';
import { useTranslation } from 'react-i18next';

// Custom components
import Box from '../Box';

const ErrorsPanel = styled(Box)`
& {
    margin-top: 15px;
    margin-bottom: 15px;
}
`;

const StepError = styled(Box)`
& {
    margin-top: 5px;

    span {
        margin-top: 3px;
    }
}    
`;

const StepCardOuter = styled(Box)`
& {
    width: 100%;
    padding: 30px;        
}
`;

const FinalCardOuter = styled.div`

`;

const isAllDone = (steps = {}) => {
    const allDone = Object.entries(steps).filter(r => r[1].done);
    return allDone > 0 && allDone === Object.keys(steps).length;
};

const isStepValidated = (config, validations) => {
    let allValidated = 0;
    
    for (let key of config) {

        if (validations[key]) {
            allValidated += 1;
        }
    }

    return allValidated === config.length;
};

export default ({
    children, 
    finalCard,
    config = [],
    step = 0, 
    steps={},
    validations={},
    errors = {}, 
    onCancel = () => {}, 
    onNext = () => {}
}) => {

    const convertedErrors = Object.entries(errors[step] || {}).map(r => r[1]);
    const isLoading = steps[step] && steps[step].loading;
    const isValidated = isStepValidated(config[step].validations, validations);
    const isDisabledMove = !isValidated || isLoading;
    const isFinalStep = step === config.length - 1;
    const { t } = useTranslation();

    return (
        <StepCardOuter>
            {(isFinalStep && finalCard && isValidated) &&
                <FinalCardOuter>
                    {finalCard}
                </FinalCardOuter>
            }
            {(!isValidated || (isValidated && !isFinalStep)) &&
                <div>
                    {children}
                    <ErrorsPanel>
                        {convertedErrors.length > 0 && 
                            convertedErrors.map((err, index) => (
                                <StepError 
                                    key={index}
                                    display="flex" 
                                    flexDirection="row" 
                                    alignItems="stretch"                                                
                                >
                                    <IconError width={24} height={24} />
                                    <Text>{t(err)}</Text>
                                </StepError>
                            ))
                        }
                    </ErrorsPanel>
                    <Box
                        display="flex" 
                        flexDirection="row"
                        alignItems="center"
                    >
                        <Box flex={1}>
                            <Button 
                                onClick={onCancel} 
                                disabled={isAllDone(steps) || isLoading}
                                mode="outline" 
                                emphasis="negative"
                            >{step === 0 ? t('create.wizard.buttons.cancel') : t('create.wizard.buttons.back')}</Button>
                        </Box>
                        <Box flex={1} textAlign="right">
                            <Button 
                                title={isDisabledMove ? t('create.wizard.buttons.disabled') : undefined}
                                onClick={onNext} 
                                disabled={isDisabledMove}
                                mode={isDisabledMove ? 'outline' : 'strong'}
                                emphasis="positive"
                            >{t('create.wizard.buttons.next')}</Button>
                        </Box>
                    </Box>
                </div>
            }        
        </StepCardOuter>
    );
};