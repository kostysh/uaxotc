import React from 'react';
import styled from 'styled-components';
import { Text, IconCheck, IconArrowRight } from '@aragon/ui';
import { useTranslation } from 'react-i18next';

// Custom components
import Box from '../Box';

const StepIndicatorOuter = styled.div`
& {
    display: flex;
    flex-direction: row;
    flex-wrap: nowrap;
    align-items: center;
    padding-bottom: 10px;

    &:last-of-type {
        padding-bottom: 0;
    }
}
`;

const StepsListIcon = styled.div`
& {
    border-radius: 50%;
    border: 1px solid rgba(0,0,0,0.3);
    min-width: 28px;
    width: 28px;
    height: 28px;
    margin-right: 15px;
    flex-grow: 0;
}

svg {
    margin-left: 6px;
}
`;

const CurrentStepIcon = styled(IconArrowRight)`
& {
    transform: rotate(180deg);
    margin-left: 10px;
}
`;

const StepIndicator = ({ label, done = false, isCurrent }) => {
    return (
        <StepIndicatorOuter>
            {!done &&
                <StepsListIcon />
            }
            {done &&
                <StepsListIcon>
                    <IconCheck width={22} height={22} />
                </StepsListIcon>                
            }
            <Text size="large">{label}</Text> 
            {isCurrent &&
                <CurrentStepIcon width={16} height={16} />
            }
        </StepIndicatorOuter>
    );
};

const StepsListOuter = styled(Box)`
& {
    padding: 30px;
}
`;

export default ({ config = [], step = 0, steps = {} }) => {
    const { t } = useTranslation();
    return (
        <StepsListOuter>
            {
                config.map((s, index) => (
                    <StepIndicator 
                        key={index}
                        label={t(s.title)}
                        done={steps[index] && steps[index].done}
                        isCurrent={step === index}
                    />
                ))
            }
        </StepsListOuter>
    );
};
