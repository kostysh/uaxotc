import React from 'react';
import styled from 'styled-components';
import { Text } from '@aragon/ui';
import { useTranslation } from 'react-i18next';

// Custom components
import Box from '../Box';

const StepInfoList = styled.ul`
& {
    margin-left: 20px;        
}

li {
    margin-bottom: 10px;

    &: last-child {
        margin-bottom: 0;
    }

    span {
        font-size: 18px;
    }
}
`;

const StepInfoOuter = styled(Box)`
& {
    padding: 30px 0 30px 0;

    @media (max-width: 540px) {
        padding: 0 30px 30px 30px;
    }
}
`;

export default ({ step = 0, config = [] }) => {
    const { t } = useTranslation();
    return (
        <StepInfoOuter>
            <StepInfoList>
                {(config[step] && Array.isArray(config[step].info)) &&
                    config[step].info.map((inf, index) => (
                        <li key={index}>
                            <Text size="xlarge">{t(inf)}</Text>
                        </li>
                    ))
                }
            </StepInfoList>
        </StepInfoOuter>
    );
};