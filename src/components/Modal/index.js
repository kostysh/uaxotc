import React, { Component } from 'react';
import styled from 'styled-components';
import { IconClose } from '@aragon/ui';

export const ModalContext = React.createContext();

const PanelCloseButton = styled.button`
  & {
    position: absolute;
    padding: 15px;
    margin: 10px;
    width: 30px;
    height: 30px;
    line-height: 1.2;
    border-radius: 50%;
    top: 0;
    right: 20px;
    cursor: pointer;
    background: none;
    border: none;
    outline: 0;
    &::-moz-focus-inner {
      border: 0;
    }
  }
`;

const StyledOverlay = styled.div`
    & {
        position: fixed;
        top: 0;
        bottom: 0;
        left: 0;
        right: 0;
        height: 100vh;
        width: 100vw;
        display: flex;
        flex-flow: column;
        justify-content: center;
        align-items: center;
        background: rgba(68,81,89,0.65);
    }
`;

export class ModalProvider extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isModalOpen: props.isModalOpen || false,
            modalContent: null,
            zIndex: 98989898
        };
    }

    closeModal = () => {
        this.setState({
            isModalOpen: false,
            modalContent: null
        });
    };

    openModal = (modalContent) => {
        this.setState({
            isModalOpen: true,
            zIndex: this.state.zIndex + 1,
            modalContent 
        });
    };

    render() {
        const { isModalOpen, modalContent, zIndex } = this.state;
        const { children } = this.props;

        return (
            <ModalContext.Provider value={{
                openModal: this.openModal,
                closeModal: this.closeModal
            }}>
                {isModalOpen &&
                    <StyledOverlay style={{ zIndex }}>
                        <PanelCloseButton type="button" onClick={this.closeModal}>
                            <IconClose />
                        </PanelCloseButton>
                        {modalContent}
                    </StyledOverlay>                    
                }                
                {children}
            </ModalContext.Provider>            
        );
    }
}

export const ModalConsumer = ModalContext.Consumer;

