import React, { Component } from 'react';
import { DropDown } from '@aragon/ui';
import { withTranslation } from 'react-i18next';

class LangSelector extends Component {

    constructor (props) {
        super(props);
        const { i18n } = this.props;
        const items = Object.keys(i18n ? i18n.store.data : {});
        const active = items.indexOf(i18n.language);

        this.state = {
            activeItem: active !== -1 ? active : 0,
            items 
        };
    }

    handleChange = (index, lng) => {
        this.setState({
            activeItem: index
        });
        this.props.i18n.changeLanguage(lng);
    }

    render() {
        const { activeItem, items } = this.state;

        return ( 
            <DropDown 
                items = {items}
                active = {activeItem}
                onChange = {i => this.handleChange(i, items[i])}
                wide
            />
        );
    }
}

export default withTranslation()(LangSelector);