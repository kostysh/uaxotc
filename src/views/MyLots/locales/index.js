import en from './mylots.en.json';
import ru from './mylots.ru.json';
import uk from './mylots.uk.json';

export default {
    en,
    ru,
    uk
};