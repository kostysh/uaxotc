import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withTranslation } from 'react-i18next';
import { Text } from '@aragon/ui';

// System imports
import * as selectors from '../../store/selectors';
import * as actions from '../../store/actions';

// Custom components
import { Page, PageBar, PageContent, LeftPanel, RightPanel } from '../../components/Layout';
import { LotsTable, TableLoader } from '../../components/LotsTable';
import CloseCard from '../../components/CloseCard';
import { ModalContext } from '../../components/Modal';

class MyLots extends Component {

    onSort = (prop, mode) => {

        if (!mode) {

            return this.props.resetSort(prop);
        }

        this.props.sort(prop, mode);
    };

    render() {
        const { 
            t, 
            uaxDetailsFetching, uaxDetails, 
            lotsFetching, lotsFetchingProgress, lots, 
            lotsFetch, 
            lotsParams,
            clearError, lotsError 
        } = this.props;
        const isLoading = uaxDetailsFetching || lotsFetching;
        const lotsProgress = `Loaded lots: ${lotsFetchingProgress}%`;

        return (
            <div>
                <PageBar
                    title={t('mylots.labels.title')}
                >
                    <Text>{lotsParams.closed ? t('mylots.labels.all') : t('mylots.labels.open')}</Text>
                </PageBar>
                <Page>
                    <PageContent>
                        <LeftPanel>
                            <LotsTable 
                                owned={true}
                                loader={<TableLoader label={lotsProgress} size="small" />}
                                isLoading={isLoading}
                                uaxDetails={uaxDetails}
                                records={lots} 
                                lotsParams={lotsParams}
                                error={lotsError} 
                                onReload={() => {
                                    clearError();
                                    lotsFetch();
                                }}
                                openModal={this.context.openModal} 
                                onSortLot={mode => this.onSort('value', mode)}
                                onSortPrice={mode => this.onSort('price', mode)}
                                onSortOne={mode => this.onSort('one', mode)}
                                onStatusFilter={status => this.onSort('closed', status)}
                                onClose={lotId => this.context.openModal((
                                    <CloseCard
                                        lotId={lotId}
                                    />
                                ))}
                            />
                        </LeftPanel>
                        <RightPanel>

                        </RightPanel>
                    </PageContent>
                </Page>
            </div>                                                
        );
    }
}

MyLots.contextType = ModalContext;

function mapStateToProps(state) {

    return {
        uaxDetailsFetching: selectors.uaxDetailsFetching(state),
        uaxDetails: selectors.uaxDetails(state),
        lots: selectors.mylots(state),
        pagination: selectors.mylotsPagination(state),
        lotsParams: selectors.mylotsParams(state),
        lotsFetching: selectors.mylotsFetching(state),
        lotsFetchingProgress: selectors.mylotsFetchingProgress(state),
        lotsError: selectors.mylotsError(state)
    };
}

const mapDispatchToProps = dispatch => {

    return {
        fetchUaxDetails: () => dispatch(actions.uaxDetailsFetch()),
        lotsFetch: () => dispatch(actions.mylotsFetch()),
        clearError: () => dispatch(actions.mylotsErrorInvalidate()),
        sort: (prop, mode) => dispatch(actions.mylotsParamsAdd({
            [prop]: mode
        })),
        resetSort: prop => dispatch(actions.mylotsParamsRemove([[prop]]))
    };
};

const connectedMyLots= withTranslation()(connect(mapStateToProps, mapDispatchToProps)(MyLots));

export default connectedMyLots;