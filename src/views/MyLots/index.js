import React from 'react';
import loadable from '@loadable/component';
import { PageLoader } from '../../components/Loader';

const LoadableMyLots = loadable(() => import('./MyLots'), {
    fallback: <PageLoader />
});

export const route = [
    {
        path: '/lots',
        exact: true,
        label: 'mylots.route.label',
        component: LoadableMyLots
    }
];
