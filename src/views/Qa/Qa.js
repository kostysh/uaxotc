import React, { Component, useContext } from 'react';
import { connect } from 'react-redux';
import { push } from 'connected-react-router';
import { withTranslation, useTranslation } from 'react-i18next';
import styled from 'styled-components';
import { Text } from '@aragon/ui';

// Custom components
import { Page, PageBar, PageContent, LeftPanel, RightPanel } from '../../components/Layout';
import { Markdown } from '../../components/Markdown';

const qaRecords = Array(11).fill(0).map((_, i) => ({
    title: `qa.questions.q${i+1}.title`,
    content: `qa.questions.q${i+1}.answer`,
    url: `qa.questions.q${i+1}.url`
}));

const AccordionContext = React.createContext();

const Accordion = ({
    url,
    routePush = () => {}, 
    records = []
}) => {
    const { t } = useTranslation();
    records = records.map(r => {
        r.opened = t(r.url) === url; 
        return r;
    });

    return (
        <AccordionContext.Provider value={{ routePush }}>
            {records.map((record, index) => (
                <AccordionSection 
                    key={index}
                    opened={record.opened}
                    record={record}
                >
                    <Markdown>{t(record.content)}</Markdown>
                </AccordionSection>
            ))}
        </AccordionContext.Provider>
    );
};

const SectionOuter = styled.div`
& {
    background-color: white;
}
`;

const SectionHeaderOuter = styled.div`
& {
    display: flex;
    flex-direction: row;
    align-items: center;
    cursor: pointer;
    padding: 10px 0 10px 0;
    border-top: 1px solid rgba(220,234,239,0.5);

    &.opened {
        background-color: rgba(220,234,239,0.5);
    }
}

.icon {
    disply: block;
    width: 16px;
    height: 16px;
    margin: 0 10px 0 10px;
    background-image: none;
    background-repeat: no-repeat;
    background-position: 50% 50%;

    &.open {
        background-image: url(data:image/svg+xml,%3Csvg%20width%3D%229%22%20height%3D%225%22%20viewBox%3D%220%200%209%205%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3E%3Cpath%20d%3D%22M0%200h8.36L4.18%204.18z%22%20fill%3D%22%23B3B3B3%22%20fill-rule%3D%22evenodd%22%2F%3E%3C%2Fsvg%3E);
        transform: rotate(270deg);
    }

    &.close {
        background-image: url(data:image/svg+xml,%3Csvg%20width%3D%229%22%20height%3D%225%22%20viewBox%3D%220%200%209%205%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3E%3Cpath%20d%3D%22M0%200h8.36L4.18%204.18z%22%20fill%3D%22%23B3B3B3%22%20fill-rule%3D%22evenodd%22%2F%3E%3C%2Fsvg%3E);        
    }
}
`;

const SectionHeader = ({
    opened,
    onChange,
    children
}) => {
    return (
        <SectionHeaderOuter 
            ref={node => {
                if (node && opened && typeof node.scrollIntoViewIfNeeded === 'function') {
                    setTimeout(() => node.scrollIntoViewIfNeeded(true), 350);
                }
            }}
            className={opened ? 'opened' : ''}
            onClick={onChange}
        >
            <div className={opened ? 'icon close' : 'icon open'}></div>
            <div>{children}</div>
        </SectionHeaderOuter>
    );
};

const SectionContentOuter = styled.div`
& {
    padding: 10px 10px 20px 36px;
    max-height: 30vh;
    overflow-y: auto;
    background-color: rgba(220,234,239,0.2);
}
`;

const SectionContent = ({
    opened,
    children
}) => {

    if (!opened) {
        return null;
    }

    return (
        <SectionContentOuter>
            {children}
        </SectionContentOuter>
    );
};

const AccordionSection = ({
    opened = false, 
    record, 
    children
}) => {
    const { routePush } = useContext(AccordionContext);
    const { t } = useTranslation();
        
    return (
        <SectionOuter>
            <SectionHeader 
                opened={opened}
                onChange={() => opened ? routePush('/qa') : routePush(`/qa/${t(record.url)}`)}
            >
                {t(record.title)}
            </SectionHeader>
            <SectionContent
                opened={opened}
            >
                {children}
            </SectionContent>
        </SectionOuter>
    );
};

class Qa extends Component {

    render() {
        const { t, routePush, match: { params: { url }} } = this.props;
        
        return (
            <div>
                <PageBar
                    onTitleClick={() => routePush('/qa')}
                    title={t('qa.route.label')}
                >
                    <Text></Text>
                </PageBar>
                <Page>
                    <PageContent>
                        <LeftPanel>
                            <Accordion 
                                url={url}
                                routePush={routePush}
                                records={qaRecords}
                            />
                        </LeftPanel>
                        <RightPanel>

                        </RightPanel>
                    </PageContent>
                </Page>
            </div> 
        );
    }
}

function mapStateToProps(state) {

    return {
        
    };
}

const mapDispatchToProps = dispatch => {

    return {
        routePush: newRoute => dispatch(push(newRoute))
    };
};

const connectedQa= withTranslation()(connect(mapStateToProps, mapDispatchToProps)(Qa));

export default connectedQa;