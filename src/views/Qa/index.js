import React from 'react';
import loadable from '@loadable/component';
import { PageLoader } from '../../components/Loader';

const LoadableQa = loadable(() => import('./Qa'), {
    fallback: <PageLoader />
});

export const route = [
    {
        path: '/qa',
        label: 'qa.route.label',
        exact: true,
        bottomMenu: true,
        component: LoadableQa
    },
    {
        path: '/qa/:url',
        label: 'qa.route.label',
        noMenu: true,
        component: LoadableQa
    }
];
