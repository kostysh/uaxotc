export default {
    url: 'how-to-setup-and-use-metamask',
    title: 'How to set up and use the MetaMask plugin?',
    answer: `
To set up a plugin follow the link [https://metamask.io/](https://metamask.io/) and choose a plugin version dedicated to your web browser version.  
Currently, only Chrome, Firefox, Brave и Opera browsers are supported.  
After the plugin install follows the wallet creation notes. When the wallet is created, you can save a mnemonic phrase in a safe place.  
It will allow you to restore access to the wallet in case if you will forget a password or if you will want to use a wallet on the other device.  
If you want to use already existed Ethereum addresses (accounts) that holds your tokens or ETH, then you able to import these addresses using related private keys. 
`
};