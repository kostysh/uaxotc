import config from '../../../../config';

export default {
    url: 'which-contract-address-uax-otc',
    title: 'Яка адреса у смарт контракту торговельного майданчика?',
    answer: `
    Адреса смарт контракту торговельного майданчика:  
    ${config.contract}
`
};