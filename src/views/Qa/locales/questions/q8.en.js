export default {
    url: 'how-to-buy-tokens',
    title: 'How to buy tokens?',
    answer: `
Before you will start buying, make sure that the following requirements are met:  
- MetaMask plugin is installed and configured;
- You are logged in MetaMask;
- Account that containing sufficient UAX amount is selected in the MetaMask;
- This account also containing some ETH (it is required to pay for the transaction gas);
If any of these requirements not met you are will get related notifications in the UI.

Buying procedure:  
- On the "Trades" page are available following controls: token name filter, sorting by sum value (click on the column title), sorting by the lot price. List of lots displayed with pagination;  
- Choose a wanted Lot from the list and be sure that chosen token is definitely that token you want to buy. Tokens can have the same symbols and names but different smart contract addresses. Follow the link of the chosen token and check details on the Etherscan page;  
- Press the "Buy" button and follow to the buying UI;  
- Follow the "wizard";  
- During the purchase process, you will need to send two transactions. First one - for the confirmation of the UAX transfer (this confirmation you can unset in case if you change your mind to buy a lot). Second one - this is a purchase transaction (cannot be undone after finished);  
- Each transaction will require payment of gas, which is consumed by the transaction. Approximate calculation of gas costs will be shown in the transaction UI. Gas cost is calculated dynamically in depending on Ethereum network state;  
- In the result of the second transaction, UAX in the amount of the Lot price will be sent from your account to the Lot seller account and tokens you bought will be sent to you immediately (do not forget to register token you buying in the MetaMask and you will able to see balance);  
- All transaction data will be saved in the "Notification centre" for your comfort (bell icon at the right top corner);  
`
};