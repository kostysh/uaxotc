export default {
    url: 'how-to-import-existed-account-to-metamask',
    title: 'How to import existed Ethereum account into MetaMask wallet?',
    answer: `
Click on "Import account" option in the MetaMask's accounts menu.  
Then you have to add your account private key in the import form.  
After account will be added you should add all your tokens using "Add token" menu.
`
};