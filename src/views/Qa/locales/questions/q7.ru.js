export default {
    url: 'which-contract-address-uax',
    title: 'Какой адрес у смарт контракта токена UAX?',
    answer: `
    Адрес смарт контракта токена UAX:  
    0xdfcab0b79acf7b189f4c50b512fe059f8b28e081
`
};