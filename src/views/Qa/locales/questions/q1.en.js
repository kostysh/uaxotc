export default {
    url: 'what-is-uax',
    title: 'What is UAX?',
    answer: `
UAX it is a cryptocurrency ERC20 token, stable coin of the Ukrainian hryvnia.  
You can buy UAX at the [Kuna](https://kuna.io/), buy out from the token holders directly, also, you can get UAX in exchange for other tokens here on the trading market.  
You can use official UAX wallet for storing of your UAX tokens, also, you can use any other wallets that support ERC20 standard tokens    
`
};