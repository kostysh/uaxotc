export default {
    url: 'how-to-sell-tokens',
    title: 'How to sell tokens?',
    answer: `
Before you will start selling, make sure that the following requirements are met:  
- MetaMask plugin is installed and configured;  
- You are logged in MetaMask;  
- An account that containing tokens you want to sell is selected in the MetaMask;  
- This account also containing some ETH (it is required to pay for the transaction gas);
If any of these requirements not met you are will get related notifications in the UI.

Lot creation and publication:  
- Open the "Create lot" page;  
- Follow the "wizard";  
- During the lot creation and publication process, you will need to send two transactions. First one - for the confirmation of transfer tokens you want to sell (this allowance you can unset if you will change your mind). Second one transaction - for the lot publication;  
- Each transaction will require payment of gas, which is consumed by the transaction. Approximate calculation of gas costs will be shown in the transaction UI. Gas cost is calculated dynamically in depending on Ethereum network state;  
- After the second transaction successfully is done, tokens you want to sell becomes blocked on the market smart contract until the moment of buying or the lot deletion;  
- Your published lots are displaying on the "My lots" page;  
- Any of your active lots can be deleted by you (you have to send a transaction for deletion);  
- If you delete the lot - then all blocked tokens will be returned to your account immediately;  
- The Lot cannot be deleted if it already been bought;  
- After the someone will buy your lot all blocked tokens will be sent to him and your account will be credited with a sum in the amount of the lot price (except service fee, fee sum is shown in the form of the lot publication);  
- All transaction data will be saved in the "Notification centre" for your comfort (bell icon at the right top corner); 
`
};