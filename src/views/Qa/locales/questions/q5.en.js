export default {
    url: 'how-to-setup-tokens-display-in-metamask',
    title: 'How to set up tokens display in the MetaMask?',
    answer: `
You should register address of the token in the “Add token” menu (“Custom Token” tab).  
After the address of the token will be registered, you can see information about the balance and perform operations with the token.  
`
};