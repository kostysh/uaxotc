export default {
    url: 'how-to-delete-lot',
    title: 'How to delete my lot?',
    answer: `
List of all lots published from your account (that selected in the MetaMask) is displayed on the "My Lots" page.  
This list will be refreshed if you will switch to another account.  
By default, bought or deleted lota are not show in the list. If you want to enable displaying of these lots you should switch on "Closed" checkbox in the head of the list.  

Lot deletion:  
- Press the button "Close" near the lot you want to delete; 
- Deletion transaction requires payment of the gas spend. Approximate calculation of gas costs will be shown in the transaction UI. Gas cost is calculated dynamically in depending on Ethereum network state;  
- All transaction data will be saved in the "Notification centre" for your comfort (bell icon at the right top corner); 
`
};