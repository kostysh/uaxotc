export default {
    url: 'which-contract-address-uax',
    title: 'Яка адреса у смарт контракту токена UAX?',
    answer: `
    Адреса смарт контракту токена UAX:  
    0xdfcab0b79acf7b189f4c50b512fe059f8b28e081
`
};