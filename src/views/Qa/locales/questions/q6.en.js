import config from '../../../../config';

export default {
    url: 'which-contract-address-uax-otc',
    title: 'Which a smart contract address of the OTC market?',
    answer: `
    OTC market smart contract address:  
    ${config.contract}
`
};