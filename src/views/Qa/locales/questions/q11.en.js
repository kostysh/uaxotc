export default {
    url: 'how-fee-is-calculated',
    title: 'How is calculated service fee?',
    answer: `
Service fee is calculated in depends on the lot price.
- 0.5% (up to 500 UAX)
- 0.45% (from 501 to 1000 UAX)
- 0.4% (from 1001 to 5000 UAX)
- 0.35% (from 5001 to 10000 UAX)
- 0.3% (from 10000 to 20000 UAX)

Minimal service fee: 0.01 UAX
`
};