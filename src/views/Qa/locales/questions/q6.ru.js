import config from '../../../../config';

export default {
    url: 'which-contract-address-uax-otc',
    title: 'Какой адрес у смарт контракта торговой площадки?',
    answer: `
    Адрес смарт контракта торговой площадки:  
    ${config.contract}
`
};