import en from './qa.en.json';
import ru from './qa.ru.json';
import uk from './qa.uk.json';

import { default as q1en } from './questions/q1.en';
import { default as q1ru } from './questions/q1.ru';
import { default as q1uk } from './questions/q1.uk';

import { default as q2en } from './questions/q2.en';
import { default as q2ru } from './questions/q2.ru';
import { default as q2uk } from './questions/q2.uk';

import { default as q3en } from './questions/q3.en';
import { default as q3ru } from './questions/q3.ru';
import { default as q3uk } from './questions/q3.uk';

import { default as q4en } from './questions/q4.en';
import { default as q4ru } from './questions/q4.ru';
import { default as q4uk } from './questions/q4.uk';

import { default as q5en } from './questions/q5.en';
import { default as q5ru } from './questions/q5.ru';
import { default as q5uk } from './questions/q5.uk';

import { default as q6en } from './questions/q6.en';
import { default as q6ru } from './questions/q6.ru';
import { default as q6uk } from './questions/q6.uk';

import { default as q7en } from './questions/q7.en';
import { default as q7ru } from './questions/q7.ru';
import { default as q7uk } from './questions/q7.uk';

import { default as q8en } from './questions/q8.en';
import { default as q8ru } from './questions/q8.ru';
import { default as q8uk } from './questions/q8.uk';

import { default as q9en } from './questions/q9.en';
import { default as q9ru } from './questions/q9.ru';
import { default as q9uk } from './questions/q9.uk';

import { default as q10en } from './questions/q10.en';
import { default as q10ru } from './questions/q10.ru';
import { default as q10uk } from './questions/q10.uk';

import { default as q11en } from './questions/q11.en';
import { default as q11ru } from './questions/q11.ru';
import { default as q11uk } from './questions/q11.uk';

export default {
    en: {
        qa: {
            questions: {
                q1: q1en,
                q2: q2en,
                q3: q3en,
                q4: q4en,
                q5: q5en,
                q6: q6en,
                q7: q7en,
                q8: q8en,
                q9: q9en,
                q10: q10en,
                q11: q11en
            },
            ...en.qa
        }        
    },
    ru: {
        qa: {
            questions: {
                q1: q1ru,
                q2: q2ru,
                q3: q3ru,
                q4: q4ru,
                q5: q5ru,
                q6: q6ru,
                q7: q7ru,
                q8: q8ru,
                q9: q9ru,
                q10: q10ru,
                q11: q11ru
            },
            ...ru.qa
        }
    },
    uk: {
        qa: {
            questions: {
                q1: q1uk,
                q2: q2uk,
                q3: q3uk,
                q4: q4uk,
                q5: q5uk,
                q6: q6uk,
                q7: q7uk,
                q8: q8uk,
                q9: q9uk,
                q10: q10uk,
                q11: q11uk
            },
            ...uk.qa
        }
    }
};