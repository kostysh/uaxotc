import en from './trades.en.json';
import ru from './trades.ru.json';
import uk from './trades.uk.json';

export default {
    en,
    ru,
    uk
};