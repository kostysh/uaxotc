import React, { Component } from 'react';
import { connect } from 'react-redux';
import { push } from 'connected-react-router';
import { withTranslation } from 'react-i18next';
import { Text } from '@aragon/ui';

// System imports
import * as selectors from '../../store/selectors';
import * as actions from '../../store/actions';

// Custom components
import { Page, PageBar, PageContent, LeftPanel, RightPanel } from '../../components/Layout';
import { LotsTable, TableLoader } from '../../components/LotsTable';
import { ModalContext } from '../../components/Modal';
import InfoText from '../../components/InfoText';

class Trades extends Component {

    onSort = (prop, mode) => {

        if (!mode) {

            return this.props.resetSort(prop);
        }

        this.props.sort(prop, mode);
    };
    
    render() {
        const { 
            t, routePush, 
            uaxDetailsFetching, uaxDetails, 
            lotsFetching, lotsFetchingProgress, lots, 
            lotsFetch, clearError,
            lotsError 
        } = this.props;
        const isLoading = uaxDetailsFetching || lotsFetching;
        const lotsProgress = `${t('trades.labels.loaded')}: ${lotsFetchingProgress}%`;

        return (
            <div>
                <PageBar 
                    title={t('trades.route.label')}
                >
                    <Text>{t('trades.title')}</Text>
                </PageBar>
                <Page>                
                    <PageContent>
                        <LeftPanel>
                            <LotsTable 
                                owned={false}
                                loader={<TableLoader label={lotsProgress} size="small" />}
                                isLoading={isLoading}
                                uaxDetails={uaxDetails}
                                records={lots} 
                                error={lotsError} 
                                onReload={() => {
                                    clearError();
                                    lotsFetch();
                                }}
                                openModal={this.context.openModal} 
                                onSortLot={mode => this.onSort('value', mode)}
                                onSortPrice={mode => this.onSort('price', mode)}
                                onSortOne={mode => this.onSort('one', mode)}
                                onBuy={lotId => routePush(`/lot/${lotId}`)}
                            />
                        </LeftPanel>
                        <RightPanel>
                            <InfoText 
                                size="xlarge" 
                                lines={[
                                    'trades.infotext.0',
                                    'trades.infotext.1',
                                    'trades.infotext.2',
                                    'trades.infotext.3',
                                    'trades.infotext.4',
                                    'trades.infotext.5',
                                ]}
                            />
                        </RightPanel>
                    </PageContent>                
                </Page>                                
            </div>                                                
        );
    }
}

Trades.contextType = ModalContext;

function mapStateToProps(state) {

    return {
        uaxDetailsFetching: selectors.uaxDetailsFetching(state),
        uaxDetails: selectors.uaxDetails(state),
        lots: selectors.tradesLots(state),
        pagination: selectors.tradesLotsPagination(state),
        lotsParams: selectors.tradesLotsParams(state),
        lotsFetching: selectors.tradesLotsFetching(state),
        lotsFetchingProgress: selectors.tradesLotsFetchingProgress(state),
        lotsError: selectors.tradesError(state)
    };
}

const mapDispatchToProps = dispatch => {

    return {
        fetchUaxDetails: () => dispatch(actions.uaxDetailsFetch()),
        lotsFetch: () => dispatch(actions.tradesLotsFetch()),
        clearError: () => dispatch(actions.tradesErrorInvalidate()),
        sort: (prop, mode) => dispatch(actions.tradesLotsParamsAdd({
            [prop]: mode
        })),
        resetSort: prop => dispatch(actions.tradesLotsParamsRemove([[prop]])),
        routePush: newRoute => dispatch(push(newRoute))
    };
};

const connectedTrades= withTranslation()(connect(mapStateToProps, mapDispatchToProps)(Trades));

export default connectedTrades;