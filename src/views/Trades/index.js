import React from 'react';
import loadable from '@loadable/component';
import { PageLoader } from '../../components/Loader';

const LoadableTrades = loadable(() => import('./Trades'), {
    fallback: <PageLoader />
});

export const route = [
    {
        path: '/',
        exact: true,
        label: 'trades.route.label',
        component: LoadableTrades
    }
];
