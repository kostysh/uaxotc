import React from 'react';
import loadable from '@loadable/component';
import { PageLoader } from '../../components/Loader';

const LoadableCreateLot = loadable(() => import('./CreateLot'), {
    fallback: <PageLoader />
});

export const route = [
    {
        path: '/create',
        exact: true,
        label: 'create.route.label',
        component: LoadableCreateLot
    }
];
