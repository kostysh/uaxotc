import en from './create.en.json';
import ru from './create.ru.json';
import uk from './create.uk.json';

export default {
    en,
    ru,
    uk
};