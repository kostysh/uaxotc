import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withTranslation } from 'react-i18next';
import { Text, Field, TextInput  } from '@aragon/ui';

// System imports
import config from '../../config';
import * as selectors from '../../store/selectors';
import * as actions from '../../store/actions';
import { valueToBN } from '../../utils/tokens.helpers';
import { formatTokenBalance } from '../../utils/tokens.helpers';
import {
    getERC20TokenDetails, 
    getErc20OwnerBalance, 
    calcFee, 
    estimateTokensMethod,
    estimateMarketMethod,
    allowTokens,
    tokenAllowance,
    disallowTokens,
    createLot
} from '../../store/services/market';
import { getSelectedAddress } from '../../store/services/web3';

// Custom components
import Loader from '../../components/Loader';
import { Page, PageBar, PageContent, LeftPanel, RightPanel, LeftPanelFooter } from '../../components/Layout';
import { StepInfo, StepsList, StepCard } from '../../components/Wizard';
import TokenDetailsCard from '../../components/TokenDetailsCard';
import LotParametersCard from '../../components/LotParametersCard';
import EtherLink, { etherLink } from '../../components/EtherLink';
import TransactionCard from '../../components/TransactionCard';
import AllowanceCard from '../../components/AllowanceCard';

class CreateLot extends Component {

    static config = [
        {
            title: 'create.steps.0.title',
            info: [
                'create.steps.0.info.0',
                'create.steps.0.info.1'
            ],
            errors: [
                'create.steps.0.errors.0',
                'create.steps.0.errors.1'
            ],
            validations: [
                'tokenAddress',
                'tokenDetails',
                'tokenBalance'
            ]
        },
        {
            title: 'create.steps.1.title',
            info: [
                'create.steps.1.info.0',
                'create.steps.1.info.1',
                'create.steps.1.info.2'
            ],
            errors: [
                'create.steps.1.errors.0',
                'create.steps.1.errors.1',
                'create.steps.1.errors.2'
            ],
            validations: [
                'lotValue',
                'lotPrice',
                'serviceFee'
            ]
        },
        {
            title: 'create.steps.2.title',
            info: [
                'create.steps.2.info.0',
                'create.steps.2.info.1',
                'create.steps.2.info.2'
            ],
            tx: {
                0: [
                    'create.steps.2.tx.0.0'
                ]
            },
            errors: [
                'create.steps.2.errors.0',
                'create.steps.2.errors.1'
            ],
            validations: [
                'tokensApproved'
            ]
        },
        {
            title: 'create.steps.3.title',
            info: [
                'create.steps.3.info.0',
                'create.steps.3.info.1',
                'create.steps.3.info.2'
            ],
            tx: {
                0: [
                    'create.steps.3.tx.0.0'
                ]
            },
            errors: [
                'create.steps.3.errors.0',
                'create.steps.3.errors.1'
            ],
            validations: [
                'lotPublished'
            ]
        }
    ];

    static defaultStepState = {
        loading: false,
        done: false
    };

    static defaultDataSet = {
        tokenAddress: '',
        tokenDetails: false,
        tokenBalance: false,
        lotValue: false,
        lotPrice: false,
        serviceFee: false,
        tokensApproved: false,
        lotPublished: false
    };

    constructor(props) {
        super(props);

        this.state = this.getDefaultState();
    }

    setStateAsync = state => new Promise(resolve => this.setState(state, resolve));

    isLoading = () => this.state.steps[this.state.step] && this.state.steps[this.state.step].loading;

    getDefaultStepState = (step = 0) => {
        return {
            ...CreateLot.defaultStepState
        };
    };

    getDefaultState = () => {
        return {
            step: 0,
            steps: {
                0: this.getDefaultStepState(0)
            },
            errors: {
                0: []
            },
            data: {
                ...CreateLot.defaultDataSet
            },
            validations: Object.fromEntries(Object.keys(CreateLot.defaultDataSet).map(r => [r, false]))
        };
    };

    getErrorByIndex = (step, index) => CreateLot.config[step].errors[index];

    goForward = () => {

        if (this.isLoading()) {
            return;
        }

        const currentStep = this.state.step;
        const nextStep = this.state.step + 1;
        
        if (nextStep <= CreateLot.config.length) {

            this.setState({
                step: nextStep,
                steps: {
                    ...this.state.steps,
                    [currentStep]: {
                        ...this.state.steps[currentStep],
                        ...{
                            done: true
                        }
                    },
                    [nextStep]: this.getDefaultStepState(nextStep)
                },
                errors: {
                    ...this.state.errors,
                    [currentStep]: {},
                    [nextStep]: {}
                }
            });
        }
    };

    goBack = () => {

        if (this.isLoading()) {
            return;
        }

        const nextStep = this.state.step - 1;

        if (nextStep >= 0) {

            const { [this.state.step]: prevSteps, ...nextSteps } = this.state.steps;
            const { [this.state.step]: prevErrors, ...nextErrors } = this.state.errors;

            this.setState({
                step: nextStep,
                results: nextSteps,
                errors: nextErrors
            });
        } else {

            this.setState(this.getDefaultState());
        }
    };

    setStepError = (step, index, async = false) => {
        
        const newState = {
            errors: {
                ...this.state.errors,
                ...{
                    [step]: {
                        ...this.state.errors[step],
                        [index]: this.getErrorByIndex(step, index)
                    }
                }
            },
            steps: {
                ...this.state.steps,
                ...{
                    [step]: {
                        ...this.state.steps[step],
                        ...{
                            done: false
                        }
                    }
                }                
            }               
        };
        
        return async ? this.setStateAsync(newState) : this.setState(newState);
    };

    unsetStepError = (step, index, async = false) => {

        const { [index]: removedError, ...restErrors } = this.state.errors[step];
        const newState = {
            errors: {
                ...this.state.errors,
                ...{
                    [step]: {
                        ...restErrors
                    }
                }                
            }                
        };

        return async ? this.setStateAsync(newState) : this.setState(newState);
    };

    setStepLoading = (step, async = false) => {

        const newState = {
            steps: {
                ...this.state.steps,
                ...{
                    [step]: {
                        ...this.state.steps[step],
                        ...{
                            loading: true
                        }
                    }
                }
            }
        };

        return async ? this.setStateAsync(newState) : this.setState(newState);
    };

    unsetStepLoading = (step, async = false) => {

        const newState = {
            steps: {
                ...this.state.steps,
                ...{
                    [step]: {
                        ...this.state.steps[step],
                        ...{
                            loading: false
                        }
                    }
                }
            }
        };

        return async ? this.setStateAsync(newState) : this.setState(newState);
    };

    setData = (data, async = false) => {

        const newState = {
            data: {
                ...this.state.data,
                ...data
            }
        };

        return async ? this.setStateAsync(newState) : this.setState(newState);
    };

    setValidated = (names = [], async = false) => {

        const newState = {
            validations: {
                ...this.state.validations,
                ...Object.fromEntries(names.map(r => [r, true]))
            }
        };

        return async ? this.setStateAsync(newState) : this.setState(newState);
    };

    unsetValidated = (names = [], async = false) => {

        const newState = {
            validations: {
                ...this.state.validations,
                ...Object.fromEntries(names.map(r => [r, false]))
            }
        };

        return async ? this.setStateAsync(newState) : this.setState(newState);
    };

    fetchTokenDetails = async (tokenAddress) => {
        
        try {
            
            await this.setStepLoading(0, true);
            const tokenDetails = await getERC20TokenDetails(tokenAddress);
            const tokenBalance = await getErc20OwnerBalance(tokenAddress);
            await this.unsetStepError(0, 1, true);
            await this.setData({
                tokenDetails,
                tokenBalance
            });            
            await this.unsetStepLoading(0, true);
            await this.setValidated('tokenAddress', true);
            await this.setValidated('tokenDetails', true);
            await this.setValidated('tokenBalance', true);
        } catch(err) {
            await this.unsetValidated('tokenAddress', true);
            await this.unsetValidated('tokenDetails', true);
            await this.unsetValidated('tokenBalance', true);
            await this.unsetStepLoading(0, true);
            await this.setStepError(0, 1, true);
        }
    };

    validateTokenAddress = tokenAddress => {
        let bounce;
        
        this.setData({
            tokenAddress,
            tokenDetails: false,
            tokenBalance: false
        });
        this.unsetValidated([
            'tokenAddress', 
            'tokenDetails', 
            'tokenBalance'
        ]);

        clearTimeout(bounce);
        bounce = setTimeout(async () => {

            if (!window.web3.utils.isAddress(tokenAddress)) {
                return this.setStepError(0, 0);
            }
    
            this.unsetStepError(0, 0);
            
            try {            
                await this.setStepLoading(0, true);
                const tokenDetails = await getERC20TokenDetails(tokenAddress);
                const tokenBalance = await getErc20OwnerBalance(tokenAddress);
                await this.unsetStepError(0, 1, true);
                await this.setData({
                    tokenDetails,
                    tokenBalance
                });            
                await this.unsetStepLoading(0, true);
                await this.setValidated([
                    'tokenAddress', 
                    'tokenDetails', 
                    'tokenBalance'
                ], true);
            } catch(err) {
                await this.unsetValidated([
                    'tokenAddress', 
                    'tokenDetails', 
                    'tokenBalance'
                ], true);
                await this.unsetStepLoading(0, true);
                await this.setStepError(0, 1, true);
            }
        }, 150);
    };

    checkAllowance = async (valueBn) => {
        const allowance = await tokenAllowance(this.state.data.tokenDetails.address, getSelectedAddress());
        let tokensApproved = false;

        if (allowance && window.web3.utils.toBN(allowance).gte(valueBn)) {
                    
            tokensApproved = {
                transaction: true
            };
        }

        return tokensApproved;
    };

    validateLotValue = lotValue => {
        let bounce;

        this.setData({
            lotValue
        });
        this.unsetValidated(['lotValue']);

        clearTimeout(bounce);
        bounce = setTimeout(() => {
            const rawValue = valueToBN(lotValue, this.state.data.tokenDetails.decimals);

            if (window.web3.utils.toBN(this.state.data.tokenBalance).lt(rawValue)) {
                return this.setStepError(1, 0);
            }

            this.checkAllowance(rawValue)
                .then(tokensApproved => {

                    if (tokensApproved) {
                        
                        this.setData({
                            tokensApproved
                        });
                        this.setValidated(['tokensApproved']);
                    }

                    this.unsetStepError(1, 0);
                    this.setValidated(['lotValue']);
                })
                .catch(error => {
                    this.setStepError(1, 0);
                });            
        }, 150);
    };

    validateLotPrice = lotPrice => {
        let bounce;

        this.setData({
            lotPrice
        });
        this.unsetValidated(['lotPrice', 'serviceFee']);

        clearTimeout(bounce);
        bounce = setTimeout(async () => {

            try {
                const rawPrice = valueToBN(lotPrice, this.props.uaxDetails.decimals); 
                const serviceFee = await calcFee(rawPrice.toString());
                
                if (window.web3.utils.toBN(serviceFee).eq(window.web3.utils.toBN('0'))) {
                    return this.setStepError(1, 1);
                }

                this.setData({
                    serviceFee
                });

                this.unsetStepError(1, 1);
                this.unsetStepError(1, 2);
                this.setValidated(['lotPrice', 'serviceFee']);
            } catch(err) {
                this.unsetValidated(['lotPrice', 'serviceFee']);
                this.setStepError(1, 2);
            }            
        }, 150);
    };

    isStepValidated = step => {
        const stepConfig = CreateLot.config[step].validations;
        let allValidated = 0;
    
        for (let key of stepConfig) {

            if (this.state.validations[key]) {
                allValidated += 1;
            }
        }

        return allValidated === stepConfig.length;
    }

    render() {
        const { t, uaxDetails } = this.props;
        const { step, steps, data, validations, errors } = this.state;
        const stepConfig = CreateLot.config[step];
        const isLoading = this.isLoading(step);
        const isStepValidated = this.isStepValidated(step);
        const isFinalised = step === 3 && isStepValidated;

        return (
            <div>
                <PageBar
                    title={t('create.route.label')}
                >
                    <Text>{t(stepConfig.title)}</Text>
                </PageBar>
                <Page>
                    <PageContent>
                        <LeftPanel>
                            {step === 0 &&
                                <StepCard
                                    config={CreateLot.config}
                                    step={0} 
                                    steps={steps}
                                    validations={validations}
                                    errors={errors} 
                                    onCancel={() => this.goBack()} 
                                    onNext={() => {
                                        this.props.notificationAdd({
                                            type: 'info',
                                            title: t('create.notifications.common.actiontitle', {
                                                title: t(stepConfig.title)
                                            }),
                                            date: new Date().toISOString(),
                                            message: t('create.notifications.info.tokenaddress', {
                                                etherLink: etherLink(config.network, 'token', data.tokenDetails.address)
                                            })
                                        });
                                        this.goForward();
                                    }}
                                >
                                    <Field label={t('create.wizard.forms.fields.labels.tokenaddress')} required>
                                        <TextInput 
                                            wide
                                            type="text" 
                                            placeholder={t('create.wizard.forms.fields.placeholders.tokenaddress')} 
                                            value={data.tokenAddress} 
                                            onChange={({ target: { value } }) => this.validateTokenAddress(value)}
                                        />                                                                           
                                    </Field>                                
                                </StepCard>    
                            } 
                            {step === 1 && 
                                <StepCard
                                    config={CreateLot.config}
                                    step={1} 
                                    steps={steps}
                                    validations={validations}
                                    errors={errors} 
                                    onCancel={() => this.goBack()} 
                                    onNext={() => {
                                        this.props.notificationAdd({
                                            type: 'info',
                                            title: t('create.notifications.common.actiontitle', {
                                                title: t(stepConfig.title)
                                            }),
                                            date: new Date().toISOString(),
                                            message: t('create.notifications.info.lotparams', {
                                                lotValue: formatTokenBalance(data.lotValue),
                                                lotPrice: formatTokenBalance(data.lotPrice),
                                                symbol: data.tokenDetails.symbol,
                                                uaxSymbol: uaxDetails.symbol
                                            })
                                        });
                                        this.goForward();
                                    }}
                                >   
                                    <Field label={t('create.wizard.forms.fields.labels.lotvalue')} required>
                                        <TextInput 
                                            wide
                                            type="number" 
                                            placeholder={t('create.wizard.forms.fields.placeholders.zero')} 
                                            value={data.lotValue} 
                                            onChange={({ target: { value } }) => this.validateLotValue(value)}
                                        />                                                                           
                                    </Field> 
                                    <Field label={t('create.wizard.forms.fields.labels.lotprice')} required>
                                        <TextInput 
                                            wide
                                            type="number" 
                                            placeholder={t('create.wizard.forms.fields.placeholders.zero')} 
                                            value={data.lotPrice} 
                                            onChange={({ target: { value } }) => this.validateLotPrice(value)}
                                        />                                                                           
                                    </Field>                                 
                                </StepCard>
                            } 
                            {step === 2 && 
                                <StepCard
                                    config={CreateLot.config}
                                    step={2} 
                                    steps={steps}
                                    validations={validations}
                                    errors={errors} 
                                    onCancel={() => this.goBack()} 
                                    onNext={() => this.goForward()}
                                >
                                    <TransactionCard 
                                        savedState={data.tokensApproved}
                                        header={
                                            <h2>
                                                {t('create.wizard.transactions.allowance.title', {
                                                    lotValue: formatTokenBalance(data.lotValue),
                                                    symbol: data.tokenDetails.symbol
                                                })}&nbsp; 
                                                <EtherLink network={config.network} address={config.contract} type="address" short={true}/>
                                            </h2>
                                        }
                                        info={CreateLot.config[2].tx[0]} 
                                        estimationConfig={{
                                            action: estimateTokensMethod,
                                            options: [
                                                'approve',
                                                data.tokenDetails.address,
                                                [
                                                    config.contract,
                                                    valueToBN(data.lotValue, data.tokenDetails.decimals).toString()
                                                ]
                                            ],
                                            resetOptions: [
                                                'decreaseAllowance',
                                                data.tokenDetails.address,
                                                [
                                                    config.contract,
                                                    valueToBN(data.lotValue, data.tokenDetails.decimals).toString()
                                                ]
                                            ],
                                            onError: error => {
                                                this.setStepError(2, 0, false, error);
                                                this.props.notificationAdd({
                                                    type: 'error',
                                                    title: t('create.notifications.common.actiontitle', {
                                                        title: t(stepConfig.title)
                                                    }),
                                                    date: new Date().toISOString(),
                                                    message: t('create.notifications.error.estimation'),
                                                    details: {
                                                        error
                                                    }
                                                });
                                            }
                                        }}
                                        transactionConfig={{
                                            action: allowTokens,
                                            resetAction: disallowTokens,
                                            options: [
                                                data.tokenDetails.address, 
                                                valueToBN(data.lotValue, data.tokenDetails.decimals).toString()
                                            ],
                                            onError: error => {
                                                this.setStepError(2, 1, false, error);
                                                this.props.notificationAdd({
                                                    type: 'error',
                                                    title: t('create.notifications.common.actiontitle', {
                                                        title: t(stepConfig.title)
                                                    }),
                                                    date: new Date().toISOString(),
                                                    message: t('create.notifications.error.txfail'),
                                                    details: {
                                                        error
                                                    }
                                                });
                                            },
                                            onSuccess: state => {
                                                this.setData({
                                                    tokensApproved: state
                                                });
                                                this.setValidated(['tokensApproved']);
                                                this.props.notificationAdd({
                                                    type: 'info',
                                                    title: t('create.notifications.common.actiontitle', {
                                                        title: t(stepConfig.title)
                                                    }),
                                                    date: new Date().toISOString(),
                                                    message: t('create.notifications.info.approval', {
                                                        symbol: data.tokenDetails.symbol,
                                                        lotValue: formatTokenBalance(data.lotValue),
                                                        etherLink: etherLink(config.network, 'tx', state.transactionHash)
                                                    }),
                                                    details: {
                                                        receipt: state.receipt,
                                                        transaction: state.transaction
                                                    }
                                                });
                                            },
                                            onReset: state => {
                                                this.setData({
                                                    tokensApproved: false
                                                });
                                                this.unsetValidated(['tokensApproved']);
                                                this.props.notificationAdd({
                                                    type: 'warn',
                                                    title: t('create.notifications.common.actiontitle', {
                                                        title: t(stepConfig.title)
                                                    }),
                                                    date: new Date().toISOString(),
                                                    message: t('create.notifications.warn.approval', {
                                                        symbol: data.tokenDetails.symbol,
                                                        lotValue: formatTokenBalance(data.lotValue),
                                                        etherLink: etherLink(config.network, 'tx', state.transactionHash)
                                                    }),
                                                    details: {
                                                        receipt: state.receipt,
                                                        transaction: state.transaction
                                                    }
                                                });
                                            }
                                        }}                                        
                                    />
                                </StepCard>
                            } 
                            {step === 3 && 
                                <StepCard
                                    config={CreateLot.config}    
                                    step={step} 
                                    steps={steps}
                                    validations={validations}
                                    errors={errors} 
                                    onCancel={() => this.goBack()} 
                                    onNext={() => {}}
                                    finalCard={
                                        <div>
                                            <h1>{t('create.wizard.success.title')}</h1>
                                            <TokenDetailsCard
                                                network={config.network}
                                                tokenDetails={data.tokenDetails}
                                                tokenBalance={data.tokenBalance}
                                            />
                                            <LotParametersCard 
                                                lotValue={data.lotValue}
                                                lotPrice={data.lotPrice}
                                                tokenDetails={data.tokenDetails}
                                                uaxDetails={uaxDetails}
                                                serviceFee={data.serviceFee}
                                            />
                                        </div>
                                    }
                                >
                                    <TransactionCard 
                                        savedState={data.lotPublished}
                                        header={
                                            <h2>
                                                {t('create.wizard.transactions.publishing.title', {
                                                    lotValue: formatTokenBalance(data.lotValue),
                                                    lotPrice: formatTokenBalance(data.lotPrice),
                                                    symbol: data.tokenDetails.symbol,
                                                    uaxSymbol: uaxDetails.symbol
                                                })}&nbsp; 
                                                <EtherLink network={config.network} address={config.contract} type="address" short={true}/>
                                            </h2>
                                        }
                                        info={CreateLot.config[3].tx[0]} 
                                        estimationConfig={{
                                            action: estimateMarketMethod,
                                            options: [
                                                'create',
                                                [
                                                    data.tokenDetails.address,
                                                    valueToBN(data.lotValue, data.tokenDetails.decimals).toString(),
                                                    valueToBN(data.lotPrice, uaxDetails.decimals).toString()
                                                ]
                                            ],
                                            onError: error => {
                                                this.setStepError(3, 0, false, error);
                                                this.props.notificationAdd({
                                                    type: 'error',
                                                    title: t('create.notifications.common.actiontitle', {
                                                        title: t(stepConfig.title)
                                                    }),
                                                    date: new Date().toISOString(),
                                                    message: t('create.notifications.error.estimation'),
                                                    details: {
                                                        error
                                                    }
                                                });
                                            }
                                        }}
                                        transactionConfig={{
                                            action: createLot,
                                            options: [
                                                data.tokenDetails.address,
                                                valueToBN(data.lotValue, data.tokenDetails.decimals).toString(),
                                                valueToBN(data.lotPrice, uaxDetails.decimals).toString()
                                            ],
                                            onError: error => {
                                                this.setStepError(3, 1, false, error);
                                                this.props.notificationAdd({
                                                    type: 'error',
                                                    title: t('create.notifications.common.actiontitle', {
                                                        title: t(stepConfig.title)
                                                    }),
                                                    date: new Date().toISOString(),
                                                    message: t('create.notifications.error.txfail'),
                                                    details: {
                                                        error
                                                    }
                                                });
                                            },
                                            onSuccess: state => {
                                                this.setData({
                                                    lotPublished: state
                                                });
                                                this.setValidated(['lotPublished']);
                                                this.props.notificationAdd({
                                                    type: 'info',
                                                    title: t('create.notifications.common.actiontitle', {
                                                        title: t(stepConfig.title)
                                                    }),
                                                    date: new Date().toISOString(),
                                                    message: t('create.notifications.info.publishing', {
                                                        etherLink: etherLink(config.network, 'tx', state.transactionHash)
                                                    }),
                                                    details: {
                                                        receipt: state.receipt,
                                                        transaction: state.transaction
                                                    }
                                                });
                                                this.props.mylotsRefresh();
                                            },
                                            onReset: () => {}
                                        }}                                        
                                    />
                                
                                </StepCard>
                            }
                            {!isFinalised &&
                                <LeftPanelFooter>
                                    <StepsList 
                                        config={CreateLot.config}
                                        step={step}
                                        steps={steps}                                    
                                    />
                                </LeftPanelFooter>
                            }                            
                        </LeftPanel>
                        <RightPanel>
                            {!isFinalised &&
                                <div>
                                    <StepInfo step={step} config={CreateLot.config} />
                                    {(isLoading && !data.tokenDetails) && 
                                        <Loader size="small" />
                                    }
                                    {(!isLoading && data.tokenDetails) &&
                                        <TokenDetailsCard
                                            network={config.network}
                                            tokenDetails={data.tokenDetails}
                                            tokenBalance={data.tokenBalance}
                                        />
                                    }
                                    {(data.lotValue && data.lotPrice) &&
                                        <LotParametersCard 
                                            lotValue={data.lotValue}
                                            lotPrice={data.lotPrice}
                                            tokenDetails={data.tokenDetails}
                                            uaxDetails={uaxDetails}
                                            serviceFee={data.serviceFee}
                                        />
                                    }
                                    {data.tokensApproved &&
                                        <AllowanceCard>
                                            <Text size='large'>
                                                {t('create.wizard.cards.allowance.content', {
                                                    lotValue: formatTokenBalance(data.lotValue),
                                                    symbol: data.tokenDetails.symbol
                                                })}&nbsp; 
                                                <EtherLink network={config.network} address={config.contract} type="address" short={true}/>
                                            </Text>                                    
                                        </AllowanceCard>
                                    }
                                </div>
                            }                            
                        </RightPanel>
                    </PageContent>                
                </Page>
            </div>                                                            
        );
    }
}

function mapStateToProps(state) {

    return {
        uaxDetails: selectors.uaxDetails(state)
    };
}

const mapDispatchToProps = dispatch => {

    return {
        notificationAdd: record => dispatch(actions.appNotificationAdd(record)),
        mylotsRefresh: () => dispatch(actions.mylotsRefresh())
    };
};

const connectedCreateLot=withTranslation()(connect(mapStateToProps, mapDispatchToProps)(CreateLot));

export default connectedCreateLot;