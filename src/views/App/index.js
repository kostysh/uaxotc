import React, { Component, useState } from 'react';
import { Switch, Route, withRouter } from 'react-router-dom';
import { push } from 'connected-react-router';
import { connect } from 'react-redux';
import styled from 'styled-components';
import { useTranslation } from 'react-i18next';
import { Main, AppBar, TabBar, Info, Button, Text } from '@aragon/ui';

// Styles
import './App.scss';

// System imports
import packageJson from '../../../package.json';
import config from '../../config';
import routes from '../../router';
import * as selectors from '../../store/selectors';

// Custom components
import { NotificationsButton, NotificationsCenter } from '../../components/Notifications';
import { ModalProvider } from '../../components/Modal';
import { Grid, Row, Column } from '../../components/Grid';
import LangSelector from '../../components/LangSelector';
import Wallet from '../../components/Wallet';
import EtherLink from '../../components/EtherLink';

export const MakeRouteWithSubRoutes = route => {
    return (
        <Route
            path={route.path}
            exact={route.exact}
            render={props => (
                <route.component {...props} routes={route.routes} />
            )}
        />
    );
};

// Hack for langs dropdown visibility
const AppBarOuter = styled.div`
& {
    div {
        &:first-child {
            overflow: visible;
        }
    }
}

> div {
    &:after {
        display: none;
    }    
}
`;

const HideableColumn = styled(Column)`
& {
    @media (max-width: 375px) {
        display: none;
    }
}
`;

const ActionsBlock = () => (
    <Grid>
        <Row>
            <Column flex={0}>
                <NotificationsButton />
            </Column>
            <HideableColumn justify="center">
                <LangSelector />
            </HideableColumn>
        </Row>        
    </Grid>
);

const FooterMenu = styled.ul`
& {
    list-style-type: none;
    margin: 0 20px 0 0;
    padding: 0;
}

li {
    display: inline;
    margin-right: 10px;

    button {
        padding: 0;

        &:hover {
            background-color: #E6E6E6;
            text-decoration: underline;
        }
    }
}
`;

const AppTabs = ({ goToRoute, location, bottomMenu = false, children }) => {
    const { t } = useTranslation();
    const filteredRoutes = routes.filter(r => (
        !r.noMenu && 
        (bottomMenu ? r.bottomMenu : !r.bottomMenu) && 
        r.label
    ));
    const items = filteredRoutes.map(r => t(r.label));
    let currentIndex = 0;

    filteredRoutes.forEach((r, i) => {

        if (r.path === location.pathname) {
            currentIndex = i;
        }
    });

    const [selected, setSelected] = useState(currentIndex);    
    
    const changeRoute = index => {
        setSelected(index);
        goToRoute(filteredRoutes[index].path);
    };

    if (bottomMenu) {

        return (
            <Grid>
                <Row>
                    <Column flex="0">
                        <FooterMenu>
                            {filteredRoutes.map((r, index) => (
                                <li key={index}>
                                    <Button mode="text" onClick={() => changeRoute(index)}>
                                        {t(r.label)}
                                    </Button>
                                </li>
                            ))}                    
                        </FooterMenu>
                    </Column>
                    <Column justify="center">
                        {children}
                    </Column>
                </Row>                
            </Grid>
        );
    }

    return (
        <TabBar 
            items={items}
            selected={selected}
            onChange={changeRoute}
        />        
    );
};

const Logo = styled.div`
    & {
        background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAdUAAAEACAYAAADlSuxrAAAViXpUWHRSYXcgcHJvZmlsZSB0eXBlIGV4aWYAAHjarZppkiSpdoX/swotgXlYDnDBTDvQ8vUd98isoavbWs9UaVURFYHjcIcz4OnO//z3df/FnxJHdbm0Xketnj955BEnb7p//7yvwefn3+ePlc+78Ovn7vtt5DXxmt4v6nlfw+Tz8uOClj+fr18/d22/b2L/TPT5gomfP0l31vvPuP6ZKMX38/D5vxuf62b+aTufv3F/b+Od+7f/50Yw2GtkWhdPCsnzb9VdEitII01eE/+mpEEhNd5HXvVJ+nPsfsTot+D19b3FX2Ln52dE+jUUztfPgPpbjD6fh/Lb518TKkI/ryj8yNovX4wZvi75S+zutX7veXc3cyVS1X029RXC5x0D2Vh+o1H5afwtvG/Pz+Cns8VNxoxsLn62CyNE7n1DDhZmuOE8rztslpjjiY3XGDex1mc9tTjifpKS9RNubKTHXOrkY5O1xMfxey3hue947rdD584WGBkDkwWu+MuP+9OH/8nP90T3qnRDUDD7emLFuqJqmmUoc/qXUSQk3E9MyxPf58d9p/XHHyU2kcHyhLmzwenXO8Uq4UdtpSfPiXHFZ+ff1gjNPhMQIu5dWExIZMDXkEqowbcYWwjEsZOfycpjynGRgVBKtOAuuUmpkpwedW+uaeEZG0t8PwZaSERJlVbpaiCSlXOhflru1NAsqWRXSqmllV5GmTXVXEuttVVh1Gyp5VZaba31Ntrsqedeeu2t9z76HHEkIKyMOpobfYwxJzedTD25ejJizhVXWnmVVVdbfY01N+Wz8y677rb7HntatGS0v1VrzroNmyccSunkU0497fQzzrzU2k0333Lrbbffced31j5Z/TVr4bfM/XPWwidrylh+xrUfWePj1r6mCIKTopyRsZgDGW/KAAUdlTPfQ85RmVPO/Ig0RYlkLRQlx4IyRgbzCbHc8J27H5n7x7y5kv9PeYt/lzmn1P1/ZM4pdZ/M/TVvf8iazYdR0pMgdaFi6tMF2Bhw+ox9ipP+5es6xyy1a0TtrlUudFSarVHbtLKy33nFPJtQjGo5qbDAROTz7KXOErXbaO3mxIxGcE6KJTBFdyslSqTtzLq0tlaClTJSKT0cwkx6q18hmWd3xuBFWu85e9wBJod5xx5M55TNOnM/h2y2an2tDLK3sM4o+YI8v14pEs/V8gxRr6QsnJ7udnWl1hcldPtAIcRxqcJOfx5r4c7UuEEjE0S93j3XtlvNn0NZxLzAYWYhSagR9nWHRUFkOyRmTFIb58x7nyzA57tNbZy0N/8rfDfI/ipn1qUCOxRWu45bNdqTffR1SMEdQZVz7/J1X21lRpurdFZza79QNislXZMFhLF7zNf3ulyl8fo9+4zdCHvZugqmz4dKvVTzqXmPtKi/S5Ke/F8jjMP8fMrBP6/u681/8Jqst+NL7WXm4ygmWypYYmOzUALp1kZNH7XxOntf+mfTnuzDK9ysap0V2O2JEn03xLGvq9TTocRXsm3EUlHxI9zRT2CvtW6mPTatrUOvzFTJlpJ/xKFtoRzJ+jAXyAIc3QHMotGtnt6Kxb5ZUguZNGbWpXrcl8DedQj5Eldzy0T4VMgzu7uKtrZvIcTUTn0ylazUtdMyzya3P7RTvim0n2ekVE+lsK5nhYem7cb+SX8iUMc04LRJ67VxTgy1Ma4jMgCsRhWOlJdCjBQ5dxq53JNOWK6vSyuwwT4BIG7s7yzZTqNMjMu5ZHfIoVF9LJLiAIX6oe1npFj5oGcGRGfmOyr3qaJe6O3YQQFbNJ5P7H0MYudzgWlzpeJ2BEhzpj9hXW/WSVCtd7hKEKDeIpxDCF3akNYl1eE09XYIty870QtmrrWTLFWLLLvPTQtmFFXdka3tdChidtzOlGQ8tmMud0WgazMp4QE/2tsuzKwY0rArPeH2bVIdgK6Lyyp7piikkNkKvUd77LIgjqzJbmpPKa9DukL+G9R0/x5eP69nIPC4a7G8NsVUF516u6tWM7VU95P42rNq2QjirgF0aOzKGuUMrYFwAIn6yNeGqiHw68Q4Ae2WHdeXbiCgtX77pvgCQBOnGgjJz87vBh3B3wwsZNC2Ij0Jigqqw6WteAONHcQMWt4TMrNtA++oVdY2+tN10/y4gsC3ltvzXZr9rE1U/YGDU56+H7fhWfK9Cb4NkgAEXZpBM3cDLGPIg4DT+GKLqnuCf8aSofZeFuOSpnC2N5uMwOw0wCrcWvqlZm6aA8AAEdlHWohvOgyGiAKPBrLQSDif3BdBWP26CJ2UPNSKbcMU7AjiBk2YLl1EHXgdAmTGxJMk2J3eUqByZ9vrfGD3BPfsXsKSNq37kMC1elsjNKTDRGzUSAQ8GNCDgW+VDqBZm3rxZ7x3/wD4E9uRRgl0M21s41IBcCZGrqNkyF6r6fD1U63uwrq0+t+WH/uj7GFGrn7bg/fngQ5cKV/O2PQ5VpSmWREk4kLCjZpObeXRkFTrt8FfY7/mZuWa/ZkbYGN6aJoFnNz71xz5Qb5mOKGK3kFy5AaRpsD3UBNsoJyBx2ceKtXMAXpYbyIMr4CWaLuAcieIA0RkiQaI0U3NEADgtS/gaoTiZ47VYG7oIqNGkoNez+1cLqwA1EU4NysyiAxJQFsVqIRlX0GxaEKmr4OZJ1DY9OJtO7pod7gDqgLZfAeoLw6tMPcoSDzfEigzM6tDsRYumhVwyWdQGzIPcH/roTqRJSiHJCFPF8lotCDNnTQpSiLXbuDiGdQTyvTYmIcOT8fT3PQEzLMoweF2GpThjVVdDZmIzzb7yPNSdKiQLgMaaK6U0NroKADXl0bHwBisK1DnKFTX5pmtU4Y7qU4tP/1NGTwEaCuODJtdO0APNbBfscXCyqG2kWivbhquHSsfQVUGQSTTm0wY2hFPTsXTQLg5MI0qwEbgGeJzxAJ5TIQK+uW0uAH/w82g6prQams0RHQHDWtZsOJIUgBUNfUfklQAzX7LHDL87A7CYzyM3zp4RHWgikjzpJqg2jkg1zMaYMld88JinAUx3DWfPkSqZTXJU9mUwu3EWJWtLwBfYrFZHII47Vk7QOfvkSDcUB6lQZSFfQBMllhgey+981WJpbs2ULLAlxZOWonTYKcsv0OBQ0zqd5p0TVFZgU2A0AEU79PCQ+BA0ZzpNoUEnrElYAXgJjCZmxnaaaG1KRS+VX8MQyGEamkh1SkZlDe6l8lXrGCdo3Zh2hUmUmTujGyqdBMMk8YxVpV8pRKoIYQTZqiiQSLtGePqyNlWJDkwOcnZQNN0fdxyyAXhVBZ4WUTwOAl/iLKmOUEAHlHj0P8k2PIA3PbpwLS2e97ADkeNKaq5FVa6iyZQJ4AXwD/9DWJLiTONly6Hl9b9WQm4X6UAfBiICV0+V0UmhgHiolfpLzTNxOZ1WpGbzEZO2EHguzBYruNtNJS1bemQDWzU4TMhvhF2BszxiwO2BHZxLYwZavKeaDC8EojL9JAKlE1YgxEkWiR7IokgagQMkA2bRgQtAwljSsYsaVmgGLE8AA8QxiOdgjrJeRZa5TiQMbTQIAhC3NTx3eGQA3ok4mrQpVPggUjjcq4Hd5AZtYIwnqUDbMRnjiqpTj3KE+VS4VtkG2Z8N/2bEXolJmQrbgdM0enBBPnB2kci0MzOsP8Z4puXvmgUhDcWAl0DXChBRAKBHqECT9hmQOhMio4WpGmRkXBbl7UpDvfSloodbcjUlaKhHAPiDO22WCIOLkhbkZW1yEhMi3pBYeDxUQO3VGjdR3fkCEbXuRLgHBVnPHWsKyMuEQZHPUAdV5oEls15jVxAsMU7WkdoifDoABu3JbfzUsER6QgZklObsWANAPbYf77GBx1G5PrSJXt+gQUd9BQkTUMlkl4AMCMgRq4UOx1GLS9qyx4N+NLcAnDBH+4wEb9bJ4g1j1Id8AEkJ1q/wERUHiQZ2HnZ2TCtFzMPTCJ9cOkdapX/nnw+AR/YduqghHUVtzpvIoHiHzvjURYser7ubrBuCnjhFmiPqK+r10Qz4CFBDrSFtN5kRYgpwshwLhg2AGIc+62sDSH0Wuz606BByOrvgwotgkosPlMYPQgfxaUh+txxOoSpAEgVJisx1kHb0Z7IzYLGTnIRFfFZJpo3SdXScoiQkIwcz+kXGnSSHbDedpg7hUFgW4HjSRxaXa6s1hIpf3RWZ7mQUnAC+4srQ1RPMJJ+1iFPPc+9iD8YPnIibEY/S/AQHCAbup52ePEZIgd4UWwTHAtoKOi/COL7xpwJc5Di05NbNC3SnGJJHUWqIm+0LGXLLQ0blhQ8FBuyELyG5xPMNz3o0IpkBUQ6apg41UQl0DVjrCnThqRHj1Bb6fREL0zqZzvS4LnSTCcts1kpeI6FeEbECK/tJn+zHwbloLhpawAWt4EWA2woKL+kPsz1ivb2UuFU8u7U941L+h/Eg4c8lEBYhKheRQqEo5VQ+nJ42HRQoWKad3CBuZ+JH2mGiYRsVYcwBkRYAqojMQX7AdsaigeY80keh4pHEUCwKqTg9gKQwNkMO8haT6CBQmF4SzIa9AkckaCTBS1MiVboD4SjCZGNAaA8EoMILYxyQF7ugTGLUMmWTD5SHGaYDHtOOh/8BI/vczY+e9oIMxDD8IAAtBUHKxNzdsHK2M8SphV5wgrlLA9Ko85aiGb49tWC4hiVxUGYkLzsMKRBi+hEzqAMBFtl46OC0mDsopU34h1cftgLDlCs9B5poJMEjId/igCyRLs5+A1UK/AoxBde5kwCow9g/cCrSprjgMKKIYTwfsJZinNf+RnX8f2ioLZKp5GZFOjvz60LSiX3zyq8CuxAcJuO6LDOAlopbcB70KML4wc7Q35woacK0dFVhots4MfioC+jxDCObd1RRdzQAZJMqLCn3F9GkfuGhsyEpj66AQhgCyvH5xQJP9K2TBgCFI+OxAtkAoMQ3jOment9N15Rz066JNE/SIuTlqzsaJ8BTTb+ERniJ/FgWI8z+gz5GvB8/YA/32OwqtqQCu57oUHE7zX96d4/3VqzRsOKPGLUJBIfM34wkDKYZWOpCFB8hniPhidwj12/7+RH59w/huxnomfMO+L5PqIjD5K6oTtQxlgazf515z/emBncRZnilohOr0VxvI9CiM9A+ADw+5uofILy+dr9U2AtqhabfO3qY+gQ+nSQtsmi0ljxWSE8cLo9wUaoyCigdSn6S7B3BNO4DsVgMfQR08QoaoIKAOEUdfgFHTyqU9q64LLPVQwU0vqKne8x74ivAXw9kdeduEFprFA+Hh+JWmyoK4dS1OMqqzQqaG41ZY2qOXUdfdK8tcJ9IAZNCFMcif2goymvs7ub8bB0KgUprF/MANHhQkvQ8wYYfEbJmVAwpDHJDaHGm7GYRfWvJlX0HBewLlo5dQeJNSCf0B5ENFwG+ICXwHxsg/uJjnXOh8z1ST4SbaCn8HKEJT4KIQOiHcWmp9kH9tKDO5kjFJpvhwH4TNoV4vU6LdmBy2Ee8X5GBxa0mgRUAsfzHu72Dj+iMy3AYVQL4GT1EcEAuyfnoAnwkushDQBQEm/gfNse8GMEcgMG2ZxC+zh4GSEfEOfDNpGA3qsgdGAH6qt2DF3bdab7/A8IKV8n2xAOwKaUx7CWDlLueP+fCTO2sYAxU7qtV7kYDLIaSWepDaKPKImY0TemZ0dhv8hJ1WxKm0DrvBg1tqi0DTFa+9xY+mv87am7+zfH8qhIgq1EK4bhefyyjkzKhX5oETbtbjm46gF3GBYAYR0hYlwiIgKvtOS3wdseg0I1s4oUA0AQFywjG6A44iadTmdDeE5aJhdDzBKmGB5wLiNhKd+dQ4iI8vO095kB8849T4JQakNQ0JOBrJ2IyKE3sOyFCqfmdgik1Ovc7ZBPrCdUWJts705aeKdpWgRNKCIRD7TtKKqKKXj5B88Tv1kQ+aY3BT2y5/NZgZRMmPVFfS/xPbTnTOfmBeFweKPqR/+BJTVfj7UdYcFSNlFhEUI/dPHQgVmBCuN7rOf3AybO7nuYiuudXSqvhS7xoqdqDWiiMDzygstXvu8BDDtGPl413ML4olTgVydvAB0Cd6AEtgcE02F/fx/TRZ0/9QELa94RkOYMqfRH12TnOXY96JrmsG7PA7KIrpkC6V7QwhFORShKFbfMp1ib2UUcgXvUVcAPPSWc6IxKsA7Kv24KemC40PNLxuzi7rJKAO+5fC1+zyz7qAPSTug3KmHyQ6pSgXbRhZYaxk92vHidxsFEwfdNymklLRPFgSqSh406F7ZD18Nb7FAzlOPBTxqrUxHb3RorXsCqHnDUhDRf4aA3MDh7XrKlc2cxPl55Vi4jktvAKFJB0++oQrz63RqdQyGS9BDNlpxv1UPSLo1yhp5/YirxRVs+t+s5MzvROUG8pXlF0DLLOKyo6sQvSn8uHcJg2o80vtwQ1dXoMAsgax2g41OWENS3aPp+1W9ExaZDAJAS64NYBmIGhBR1eI1JI3ikCbLpqBuPh4EDEcAHU/2IKRThyCAkNo2QSPbOY0PWGQDDp6NyOu0MPqsWd6nPwVMnlDosxDidNIkA1EL7cwd8P+yDjaP4WRQOIddOFxsCj3WQlFV3pvq38o+qxM4B4cEIw9hYtgZhmOlII16k6z55jr6wIUCKZmWraA7TaQEFPZlszZJhbT2/QwQO9LVOdvpTRjSco02KjjKaAQkZXE14IeR01mMKw5OcXCP6V6SJ5dNJEjK4eTw5GexHz7NLas2N9KIqHfLXp55RImXl8+iA9Z5eXR0FHNNhm2TOK8DOdcIlrFkE/cE3PVxsUlEV82TvCZc9codW1pmDTRpQDw+vnhGY6AGzmWVq4BkbE9SVlMgqVUPwWMBuebitg7E0opeRZM84L1XSQE2ghdMX4uXh8hf2fb9ioeso8DamRBYfUosKIs4FyyrBcVNHBAlO9FBJx7MJoUUG42VOtdUQRJaNmaRKKJOpw8kbaDCP2p/0p7pk6OhlpaZfVqNtdCp5K0KLVGN2BPukHSWMQwZv5vPwHFoq2G20a8M1X6x3O2VfXBNwXeQywIbw/NIglbogNf1q0J1yYFyGKcgpbBSYDgewuogx8zqnRu48jxY8hHCqNguOjTtXdXqesHU6PLDfOkKNrHczG3eDrgkBcnfpsZ5+oYgugDTS0olo06ODBFhRqym5hs8vOhsnLDKvFJ9+SUZ234S7EkubXcGMfePkpLKgR8iTxFoXWAF3fbs97MGuo0fOTacSGX03AWAUbX6U5+o0IU3HqnE656pLSBqwzLw6+4IxvcurwCoDi1lYtkfanYvd2rlrLX0IUUfEkE7G1RykWGPJsoB0I5EqXWdhyYnJAZIq3cJewexQ1DAtgi2Azvk8n5ox/+PzUjf1NOnacP8L5iBOGnCc3ZIAAAAJcEhZcwAALiMAAC4jAXilP3YAACAASURBVHja7d15lFxXfSfw7+/e914tvbcsySs2HmKDZWxMHONYkrEnJnjIYcLxMKxhy3gNJDaWZQMBZGdIZkjYMxkwY5BkBwZEAlkmE84EkD1hPdhqiXgM42GzsWTJ2rq79vfevb/5o1oblqq71VXdr6q+n3PaavdS3X3r1f2+e9+9vycg6mO6c4O2/ILT7279ALvu7u0Gmn5oaX/+/gdbf37ZVV39/bIGwldhbzFsAiIiIoYqERERQ5WIiIihSkRERAxVIiIihioRERFDlYiIiKFKREREDFUiIiKGKhEREUOViIiIGKpEREQMVSIiIoYqERERQ5WIiIgYqkRERAxVIiIihioRERFDlYiIiBiqREREDFUiIiKGKhERETFUiYiIOkHYBNTP9JtQtgItWQe8hn0wR6pERETEUCUiImKoEhERMVSJiIgYqkRERMRQJSIiYqgSERF1Ne6Rop6mOze03od6+t2tH2DX3b3dQNMPLe3P3/9g688vu6qrv5/7UDlSJSIiIoYqERERQ5WIiIihSkRERAxVIiIihioRERFDlYiIiKFKREREDFUiIiKGKhEREUOViIiIoUpEREQMVSIiIoYqERERQ5WIiIgYqkRERAxVIiIihioRERFDlYiIiBiqREREDFUiIiKGKhEREUOViIiI2kLYBNTL9JtQtgItWQe7hn0sR6pERETEUCUiImKoEhERMVSJiIiIoUpERMRQJSIiYqgSERH1NO6hor6mOze03sd6+t2tH2DX3b3dQNMPLe3P3/9g688vu6qrv5/7WDlSJSIiIoYqERERQ5WIiIihSkRExFAlIiIihioRERFDlYiIiKFKREREDFUiIiKGKhEREUOViIiIoUpEREQMVSIiIoYqERERQ5WIiIihSkRERAxVIiIihioRERFDlYiIiBiqREREDFUiIiKGKhEREUOViIiI5kTYBNTP9JtQtgItWQe8hn0wR6pERETEUCUiImKoEhERMVSJiIgYqkRERMRQJSIiYqgSERF1NXn1H7yH+/Ra+KtP/An3kbUwve0zy60GZ8PoWU7lNBE9FSIrRTEO8aNezZhRP+SBooEpeoM8PCyMt/CwM6d2DmpSoz4FTOyBijG+rDAlUZQgegDQvaqyD8BeA+yExZPem18MXvymvSJywmNYd25oeXzvL9za+g/c/aGWnw4Dg0LeILRdephMP7S0P3//g60/v+yqrv5+7kPtPH10S1Rq1P6VNXK+B84Q+DEAoxAxqtoQkSqAnerxpNHksYFfvX5XJ3+fgE8JzXrQqsr0xMbniZGLRM0LRfACDz1fvD5PjBloppbASPNfHP7HND8m5vCUiDn6P+ao41AQQMyhrxkHDI48nAAQyMxDKwB4QKAobb+/Xt6++ScKPA7gcfH4IQx+UCxOPmbOu7XR6bZJUo+k7Ls/XIm65Tzw4c+dIjb51yKy1kPXltLKhcYaq9CZ3ufIa1DkqPcNoAgxvW3jUwb4lgr+PgX+YeySt00yVKmjDjx870hk8qtVsEZUX1LZselSa+zw0cergWAmRZeUEeQBrBJg1ZHUBsrV4bS8beOPGqUKbNiACZpvnQ9XQbEQIOCFFaK2qTxy3+ne2lcLcJ1HvEZg7OF+aJ6TAcaYMwG8VoDXWu/j8vZNfyWifz5w8du+y1Cl9oxEt2yxpfOrq8XLtSr+ZfDmxYcGmc3hYfeNvgxMAIML0/oQ0vrQzFmrhwlrsFEdNqrB2LgD4aqYKiWIQoNCziKwPL6ITqpf0g2msv2cf6MqNzmjrzCHg7SN/YQxEYA3qMobyts3fk2MvGvgorc+wlClefOPfzxXK49eqwavrvjaK4zKOASQY6dle+xFauDiAbh4oBmyJkVQ/AG0sBIuvxyHpp/bIU484sQzXInm2zd9+4uFSrF6Q2mH3G5EzhYBZFFO7M01LvXfL09s/lQtn9y1/AXXlxiqNEuoqJR3PHA14N9Urep1MBhGu0/9uqk9fICg/ARQfgKhCeEKK+GLZ8wEbHtexMeGq0HAa65Ex7Xn0S2DA0n17VWt3i4wK5bilWKMEQC35BrBKyrbPvvvB178u99nqNKzlH7w+ZXikuvLE5v+gxjzXKA7p3Q7e3qcwFaegq08hcBE8ANnIh08u223sGG4Ep3g5HbLFls9r3aDxpV7YMyKdp3QLihcIWc72H8uT9x/8+Alb97EUCUAQGXHxsvVy63eJ/9OBKEIV8/MhfgYtvRT2NJP4c1ZSM0kvJQBWXjEHgrXXGRQiAwsw5X6WHn7xmsrqH0IwCqYbPVPxiAH6MbyxP0rBi95858yVNtk4kc/7ug+3kue/7y29qqqG0xpx7mvEvg7VM2vQ1jhY0EvLF9E5ItQSZDag/BmEtqG8Wsj9mjEPgO9WmdDXQAYEUCa2x2MaX5QtPm+TS2sUVjxWRig0CKpTWw8J1X5BCCvzP5Ztn6wPHE/5hqsDNUeoY9uiSpp/U2VCXenMTiPUdru11WIMF0BlWVw5gCcnYTCs2FmOy4BOFUcPg/55SZrHOmCrOhMwCoCqwit50WKXjseHr43rIS59Ynqe41IoYs6gA+WJzbtHbzkrRsZqj3Of/uLhcpA7cZSXLnDGHNm1qZQei9cLQK3HNaNIbXNcAVYlKwdnAqcm4nRdKaDqgiiAAgDILBs5242te3+KypGPw3FKrMUp0vew7saNGlAXQLvE8A7QB2ahSMMVATGBBAbQoI8TFSEmLD57aqfqmy//7GBF735ewzVHnTg4XtHojD39qrWbxOV5WJ4Tr+o4YoAoVuBwI8itXvhTJmN0gGpB9IYQNzc6B+FijAAIvZcXWPPo1sGB9Lqf/be/d5iVoxReGhcg0sq0LgGdUnLE+BDM0/eJUBSAzDdfK0HOdj8KGxuKHJev3Tg4XtfOH7pTVMM1R5R2fHAmc77W434G6EyzBZZ6pFrhDA9A8aUkQZ7oIeGWNT+gQaAeiKoJ82uuZBT5BS8FpthpW33v0zTyn2AeY5ZjDxVhUsq8I0StFFtyyUaTRtIy3vgavthB5afFYSDnwDwFoZqF1NVKW/ffJWI3Ow0vc6ICXjNNFusH4SNi0jsXjg7BU4JdzhgFajUBdVKDvnIIR86GGGbZ2l0WkxrHxLoTbIIfZW6GK42BRdPA9536GekSKefhs0Pv7m07d4Hhl5809cYql2mPLHxRQq5rjxx/xvFyLkAYBimGWYQupWwOoTEPg0Vjlo73pkKUEssaolFZD0GcinDdYlN79h0pcSVTc098Z188gGXlOHrk/BxbdH+Plefhk/iL+vWDSvk6nvqDNWsdQpbtwb7C08Vg1BXBMafq5ALBLjMi78KMKcdfeMX6pJo9UXk9Bwk9mk4U2GDLJLYGSSVCIXIIR+mnBZe7L7s0S1ROa1+QJ2sE9PJTfEKVy/D1Q9A03hp/lZXH9pvo68CuIqhmjGVsSeT/OH/O1LhsmtGpN7B+wTqU4h3UJ8239Q3p2HUQVUB9VB4iM7s8tSjTjelOeQQEeih+sNiINL8F6b5fnPWO4CIBWwAMQHE2GyedahFmJ6JMOvPX3T+Qg+AmZWTrnn9Sh2ABKoNqDQAjaFag2oFQAUeZagvA0g6NnKtJhaN1KAYpYjYxSzO6HTbAxeU0urnDeTizsWpwtVLcLUDM4uOlpa4+KUH/ukP3zj+sj/+HEOV5n0wq0uhaQPqGvAuhqYJ4JNmeM5vxuYEH9SZ8D2yCm8uk3giApgQxoaAjSBBBLE5SBBCOFW+GOPymVMai2NupyuHzxOPfxxoDYqD0KEz4dOn4JMnoG4XoO2ZunUqKDVChFVgqMDFTB0dGGzffLND+hED07F9pz4uIa3sz0SYHvN7CT6xZ+uGv1159T1lhiq1SD4Pn9agSQ0+acCndUCzWehAVQEXw7kYwNFTrQKxEUyQg4S55p6zIAfOpWeDSAGCAlA8HYdv4KNV+Pin8MmP4eMfwic/X/DPSZxgsgwMFppbcah9Dk5sHA1VPqPAdZ2aWdOkhrSyFz5tZPM4howHaXojgI8wVOnYkWhSh08q8HEN3tV7YPGqQl0DzjWAQ69HMTBhofkWFSE2x6c+Uz1UESZ3IUzuQgCvgrppeNkD734Bp08BJ7k9wgOYrgkKEVDMcRFTW0anOzZe7lS+AJGzO/LqdSnSyj74uNQNfQ1DlXDUfq4yfFLp2DL0zI3A4wp8XAEqgBgLEw3CRAMwYZFzhFnLWDsMi2FY8ysINYbTn8K5n8Dr3pN6vFoMJKlgqKAsPHbS3YZKZfsD6526PzaQoAM/AGn9IHz1QHMWqisOVHP+vv/13hec8psf+CFDtQ9HpD6pwddLcHE5s1O6i5exDq4+BVefao5iowHY3BBMVASnibPWcUWw8nxY83x43Q/nHoPzP5v3w6QemK4aDBU9LIN1XiZ/8Lmx8o77HxDBb3ViutcnNaTlPZm7bjqnw1NkLQCGav+ERwrXmIavT3flAbtoo9hGCb5Rao5gc0Ow+WFOEWeQkWUwwVoEuBRpMQ9fexCqc98T7FQxXREMFdkBzlXlkc2Xep9+SYBzOvHaSyv7mie3XctdcOg9HlM9zCd1uPokfKMMVviZ5wi2NglXm4SEBQSFEZhokKPXrI0OUEA49BrowDVIy38HV//OnFcPewDTVWDIGYSWdxtqGagTm25yio8boO1nmL5RRlrZC/XdXSjFizmVodqziQD4uAxXO9hctUsLa86khiSpQUwIWxiFyY80t/FQdsLVjCMcfits4Sqk038Jnz555JMHv93qpYJphBjOJycO1v0P9u+x/7ON+fKUfFIhb2172V51SEp7u2Qh0uyM9zmGas+9AhSuMQ1Xm4S6mO3R9tFrgrSyF6gdQFAYmwlXXpTLVMcWnoNo2Xvgql9HWv7KnKeES7UQQ4WEI9ajVB/5y7NLB9MvGyMvbvdju7gMV34G6l3v9A/mSOk0hmrXD0w9fG0arnaw66dQuoJ3SCv7ILWDCArLYArD4LRwpqIVtvgySPR8JJP3Qd3Ts7+GpBmsI8UY1vAySWnb5mu8JF8wYpa1+cwUaXkvXGO659pMoE8eOQKpW4dOSKsHEB/4eU9ck+i+5ndIKs8gnnwSPq6yQbIWrcFZiJb9IWz+srk9nwJM1yJ47e8TpPLE5vVq/FfR5kD1SQ3xwSd6MlABQFUf4Ui1azvztLmIpj7V91tiMvF8pDGS6Z0wuUEEA8ub9YkpG6MHiRCOXA+xK5FW/gdmW6znAUzXQ4zk477bsqwP31usmOizELy2rdtlVJFW98PVD/bsWkkP9WHef4Oh2l2H/FEreSvgSt4MvrAaZcRxFcHAKbD5ETZIhgSDr4TYZUhKm2ddHey8oNwIMZTvn61ntYmN55RE/sZALm77CWd5NzSjJQbbd/KW3zG69n0HGapdMQpqNFfyNqahjtO7XTCNgLT8DHxcQTC4gqPWDLGFKwAJkExtBNB6gUzsDOqJRT50Pd8upYlNVzvRLQZySjsf19Wn4Cp7u6cq0kkPdwTi7duPOYnjy23pnxaoQtVBXdK8C0zSgCY1Xift1lFrXEEy+SSCoVOb5Q8pG8GavwyAmwnW1qqNAIFRBD28Iri0Y9OtqvohQRvP/tQjKe+Z2Rvf+0xu8Avja+/6DkM1Qxr7fsxG6MlBq0MytbP3/9CZ+72JNu+FCzTviwuxzeuSYmfuh2ub9741BiIBxAaAhBDbvB+umBCwIcRYKALAyOG7CysAUQ9VB3gH+BTqk5NaU2Dzvw71U0hLX25+YOyKE53qomyAkdrW1tdXl13V+jj45oMth2qyZvGXjuvPNuZLk/JpUXmTtPHHa1pDMr2n+dz0wYFvouLfja296/W//BmGKhEtoCdt/kebN8JtfkgBIJ3DlX+DYHAFguHTITY6NqefldszN6034ZGPaQpN45lrdnMP2KB4LTTZDVf/dsuvcx6oxQGKud6ZMar+y2fPKk2arxjBr7bzIEirk3DV/eiP9R6CIBr+i5Er17/juMcXewUiWmwmGkA4/jyYcAH3tZYAEgaQoAB1DWham/PoNRh+I9TtnDWKa6lFFPiemAae3rb5pd7hS0awvH156pCUdvfNtjJVo7Y48u6R1es+eOJTRSKiRWQL44hWrFpYoB4TrtK8CX1uFBIUMJdiHCIhwpEbIEdukX5C5Ub3jz1KOzbdCuO/BrQvUDWpIT7YP/u0VcQHAyO/M9YiUDlSJaLFDdT8MKJTnteZ83kRSFiE2Agal5vXYFt9uV2BAJchcd9p+XVOpWtXA+vD9xYrNn8fFK9v3/VThatNIq30y3QvANjEDgy/fPTydVtn+0qOVIloUYiNEC07r/PdjgmA/Chgw9m7Sns+RFbO+nW1OEC37Q6Z2r75vIqJvgfB69uX0g7J9G6klX19E6hioqpG4y8am0OgMlSJaNGEo2cDi7R3VwCYaBgIZr9bWWQvx2xTxh5ALemeib3pHZtebeG/D2MubNdj+rQxM91b7ptjVm20Pz7ltHNPufLWx+b6PZz+JaKOM9EAbHHZ4v/ccLC5GKlFVR8xYwjMryD1j7d8rHrcnAI2kt0Rmj66JaoktT+D4g/aeaMHV59q3qVJ+6eam4b5ny8bSFbJRTfM66JxMLXvGb7iW3hi156OPv5z2MTUB4Kh05cu0MNBePWAO/H+SWsvhvM/hrZYD6wC1BOLYpTNLTa1H3zm3FJa+aIRc2nbggUerrS3Zwvhn1BYfHjZlY2XiNwz72XfHKkSUUeJDWGL40v7O4SDUHcAJxq9iQzAmPPg/I9mH60GDiZjt4grTWz6HZfKfzUGQ20LVJcgKT3d87V7f+lIaBZ1uPLdv33SJ5B8yRNRR0eKhXEs9T1nRQxgC3DV/TC5geN3hnbVrKGqAjScQcFkYyXwwYmNoyHkkxB5XTub2McVJKU9gLo+OlJbF3WY8/HOlzwRdVKQH81GlxkNQEWQlp/B8VauigzBmrNmfZxGRhYslbdvvNaKeRQir2vbgyrgqvuRTD/dV4GqatQURt+10EDlSJWIOn3yD5MbysyvEwyfgcauCSRTuxAOnwbIseOK5hTwL1o+hlMgTg2iYGmqLB2c2DgaiPkIgLe1dVSkDklpD3xc6atDVEV8UBx50+gV6z7fjsfjSJWIOsYEhUXbRjOnjBeDYPh0+KSKZOqpZ43GrDkTIrNXemqkdkl+/9K2zW8IVX4kwNvaGixpA/HkL/ouUJtFHUavaVegcqRKRJ0NsSB7t76zAyuRTO+ETxvNEevIGUeNWAVGzoHTH7Z8jNgZeJVFG5WUdnz2haLy5wBe2u7r065egis/03Llc08emyaq+mDk18Yun/seVIYqES3tSDXMZ7AzNQiKpyAt74ZP60hKuxAOn4FDYWXN2XD+h7M+Tpwa5PTwjXqgAFTl8B17vBfkFvi7Vh6573RvwnvU4XfFtDnDVZFW9sLVp/ruuFQb7U+Wnbbq1ItuaPueSYYqEXVOkM/kr2UHliMt7wYA+LiGtLIPwUCz1rwxKwAJAW19X9BKHKBSPt6o8cjHTjZUKzseOFPV3+7gbzZAQdq8eFp9inT6afi03n+BepJFHRiqRLT0o8Kj7pOaqRF0NNAsvO9iAICrTULCAmw0CMDA4jQ4PLm4nb2qlHc8cLVAb3KaXmdgAtOBCWafVJFM7+6z7TIzFlDUgaFKREsfXia7XYzJD8NV9h3+/7T8DOxoETAGxqyEc4sTqqWJTasg8rryxKY3ijHPBYBOhCkUSGsH4Gr7++fmMkfNHiy0qANDlYgykFxhhn+1ATjsO2oI55BW9yEYXAExK4AODuS8eS7S8MUob3/l/wHkgpkzkA4Og5t3l/FJtQ8PwvYUdWCoEtHSd2c2u7v2JHx2ZSVXn4bNj0LsONq541BRgAtWwdmL4IKLoDJ26FMXdPrv1KSGpLQb6tO+O/5UjdriyLtHZrmxOEOViLqjU4NZ4gKFLUaqQXTc3zitHUA4dCpERk56llRgILICcXQdXLAK3pyLRS8LcGi6t3oAfTjf2/aiDgxVIlr60WCW68ucYGraN8rQgXReoSqwELMCRlbCmFMhWA4Ri2SpAsWnSEt7+nS6F2gWdRh++egcbyzOUCWibknV7P5qYk44xHP1EkxuuGU5BBOcBRtcBpGVMDKGrBSo83EVaXk31Lv+POQ6VNSBoUpEdNLBVIKV1qM8E50Lay7Izi+tirS6D642hX6c7gU6W9SBoUpEGejlMjxa1RY3JE8bQPHc1t/uJrPzp6SN5mKkmX23fXmodbioA0OViDKQqT6z11X9LAEkPj9LJk9nooXT6gG46sG+HZ0CWJSiDgxVIlpyov5Zt1fLjFlK9Pk0nSVUl3YRkKYNJOU9zVF1/x5hi1bUgaFKREs/jvIpJKNVldwstzlTN8vaXa0tTZuqwlUPwNX6fHS6yEUdGKpElI1Qzejv5uPSgkJVdfGvX7q4DFfeB/VJfx9XS1DUgaFKRBkI1Wx2/qoeOluozlZwXhfvb1PXQFrZBx9XeUwtUVEHhioRLX0HmNHrfb4xBfWzrWlZ6OfbcVLi4Kr74Ool9PdU7yFLV9SBoUrUBsuu+YCwFehEyts7k3TqU7jaQfjaNBSeDY2lL+rAUCUi6qZRvXpoXIGrl2bKC3JkerhtMlDUgaFKRJT1sHAxfFyFj6vQtApVBumz2igjRR0YqkREmUoHD582oGkdPq1Dk3pf3o5tXjJU1IGhSkS0FNmpHnAJ1MXNtzSGcw3ApeCU7lxlr6gDQ5WIqMNcY7oZoD6Bd+nM+xx9LjRQs1jUgaFKRNRhaWkPG6Gto/zsFnVgqBIRUfcEasaLOjBUidpg/9fey4tgRB2X/aIODFUiIsq8binqwFAlIqJM66aiDgxVIiLKbqB2WVEHhioREWVTFxZ1YKgSEVHGdG9Rh7kyfJKJiGgxAjWIhv+ilwOVoUpERB2natQURt/VrVWS5oPTv0RE1LlA7ZGiDgxVIiJaYr1T1IGhSkRES6bXijowVImIaEn0YlEHhioRES1+oPZoUQeGKhERLa4eLurAUCUiokXS+0Ud5or7VImIaEGB2g9FHRiqRETUUf1U1GGuOP1LRETzD9Q+K+rAUCUiog7pv6IODFUiImq7fi3qwFAlIqK26ueiDgxVIiJqX6D2eVEHhioREbUHizowVImIaKFY1GG+uE+ViIiOG6gs6sBQJSKiBWJRh5PH6V8iIjoSqCzqwFAlIqJ2YFEHhioRES0YizowVImIqA1Y1IGhSkRE7QhUFnVgqBIRURuwqANDlYiIFkIAG+2xYf7u0TXrP8X2YKgSEdHJhGlY+InYYN34mjv/lu3BUCUionlnqQGCwvZAgptHrlz/PTYIQ5WIiOZJRbyExYessTeMrrnzJ2wRhioREc03TBGkQa74NyY1twxfecc+tghDlYiI5kmCqC4mv2nULX+nrH1bnS3CUCUiovmGqc1N2yD66PCa6h+J3MmtMQxVIiKaZ5TCBLnd3hbuHl+77l62B0OViIhOIkwR5n8ipvDOsbW3/z3bg6FKRETzzlIDBMWJQOwt3BbDUCUiopPAbTEMVSIiWmiYclsMQ5WIiBZIwroJC9wWw1AlIqKTztIgnEJQ+Oj4mjvvYWswVImIaP5RCglzuwNbeP/wmnX/je3BUCUiopMIU4S5HwdB8baR1bf/A9uDoUpttOyaD0gnH//gdz98tcbuJvHxS13SWCmiwlYnWnyqopIrTlhbuHl09W3fZ4swVKkLjV2+biuArQCgD28oTleHr/eIX+eTxougrgAoG4mok2Eq4iUceDAw5kZui2GoUg+RS++pAvjEzBv2/e+PX2DR+H3nkpfD188WVcNWImpTmCJITb74FWvszaNXrDvAFmGoUo875cpbHwNwCwCobjBT/zz0Wo/4LerdS5DGoxzFEp3M2WtUN1Fu42i64nZZw20xDFXqz35A7vEA/vvMG575/kdOC2P3dnXJb/ukcb6oC9lKRC1eQzaaMlH+w6Or7/yPbA2GKtExVvza7U8DeO/MGxc8ER0/SiFhbrfY8H1ja+66j+1BDFWak19e8DRZG7hRffpaTeOLueCJ+jFMuS2GGKrUnu6kueDpYzNv2P+tj11oXPx2LniiXqcqKmFxmw0Lt3BbDB23f3zZG67nEKOFf/r8fZzmnFenc2jBU/pWr8llknDBE3U/b8QbU9xq8sENY5ev/xlbhDhSpcU5S+OCJ+qlMEWQ2qjw5TAIbuG2GGKo0pI70YIndY2rNI1XcMETZfPsMKoHYeEzw7vLt8tr7orZIMRQpUzigifKMg1zk0GQ+wi3xRBDlbpvMMAFT5SNIxFi809LGLyf22KIoUo9Y9nq2x7F0RWevjP8Op8mb+GCJ+pMlgoQFP4frL1tfM1d/5MNQgxV6uH+7h4P4PMzb1zwRG2jKipR4RGbi24evfyOR9gixFClvnP8BU/pzeril3LBE80pTEW8CQa+YXL2Rm6LIYYq0VGOWfC09b8MTgbV67ngiY6nuS2m+Nc2sL/HbTHEUCWahVz9jjKOt+BJ42vVNZ5jPBc89eeBEdRMvvCZ8Z3VdfKaO7kthhiqRCeDC576vVfLTVobfXh07V0fYGMQQ5WonYOVEy14SuJXeRefxwVPPfNMw9jcLhPl3zey+o7Psj2IoUq0CH55wdPeb33sN6xr3MgFT1171gSxucc1Ktw6dsW6r7JBiKFKtISWr77t6wC+DnDBUzdRNSpRnttiiKFKlNlBz3EWPIk23uFd8nIueMpImM5si5HAXj++ev0TbBFiqBJ1iZkFTzc3R0Zc8LSUDm2LMUPhzWOXvHOSLUIMVaJuHsUeZ8GTraW/Lz75t1zw1EEmrJmweN/40+U7uC2GGKpEPWpmwdN7Zt644KndbHTQhsUPja6540/YGMRQJeozz1rwFMY3wNVf411yMXzKBU9zmg4AxOR3SZR779jq9RvZIMRQJaJDC54+OvPGBU+zNphAQYlK4gAAAUxJREFUTO5xzRVuHee2GGKoElErJ1rwBBe/RF0y0q+jWFWjJio+LEFw09jqOyZ4pBBDlYjmOSjjgicV8SYsfF1seAO3xRBDlYjapp8WPKnYxEbFv8ZgdAu3xRBDlYg6ricXPElQs7nip0d2Ve6U19zFbTHU07jsn6hLdN2CJxsdtEHhz0bXrv9PfPaIoUpEmaW6wRz87vAbkDTeDJdelpUFTyoCBLmfG5O7e3zt+s18poihSkRdZ0kXPIlATbjfBrl/RGjeP3b5+p/xGSGGKhH1jL3f+thvWI1v0qRxpbp4haCNC57EQKydgg3/r4X9x9rI+CdPveiGPWx1IoYqUc/TrRuCg+HAb4nqy+HdC53XMwRuVNUXFRrAqRFpTh2rCsSIFyOpwDYAKYsEe2Hk58bIv/jAfmP0JdMPzWwNIiIiIiIiIiIiIiIi6gf/H52NPae5DUG7AAAAAElFTkSuQmCC);
        height: 100%;
        width: 120px;
        background-position: 30px center;
        background-size: 50%;
        background-repeat: no-repeat;
    }
`;

const ErrorOuter = styled.div`
    & {
        section {
            border-radius: 0;
            color: white;

            svg {
                margin-top: -3px;

                rect {
                    fill: white;
                }

                path {
                    fill: red;
                }
            }
        }
    }
`;

const AppWrapper = styled.div`
& {
    display: flex;
    flex-direction: column;
    height: 100vh;
}
`;

const Content = styled.div`
& {
    flex: 1 0 auto;
}
`;

const Footer = styled.div`
& {
    flex: 0 0 auto;
    padding: 30px;
    background-color: #DCEAEF;

    @media (max-width: 540px) {
        padding: 20px;
    }
}
`;

class App extends Component {
    render() {
        const { web3Error, openedNotificationsCenter, routePush, location } = this.props;

        return (
            <Main>
                <ModalProvider>
                    <AppWrapper>
                        <Content>
                            <NotificationsCenter opened={openedNotificationsCenter} />
                            <AppBarOuter>
                                <AppBar 
                                    tabs={<AppTabs goToRoute={routePush} location={location} />}
                                    endContent={<ActionsBlock />}
                                >
                                    <Logo />
                                    <Wallet />
                                </AppBar>
                            </AppBarOuter>
                            {web3Error &&
                                <ErrorOuter>
                                    <Info.Alert background="hsl(0, 94%, 73%)">{web3Error}</Info.Alert>
                                </ErrorOuter>
                            }
                            <Switch>
                                {routes.map((route, index) => (
                                    <MakeRouteWithSubRoutes key={index} {...route} />                          
                                ))}                        
                            </Switch>
                        </Content>                        
                        <Footer>
                            <AppTabs goToRoute={routePush} location={location} bottomMenu>
                                <EtherLink 
                                    label="app.labels.otc"
                                    network={config.network} 
                                    address={config.contract} 
                                    type="address" short={true} 
                                />
                                <Text size="xxsmall">Dapp v{packageJson.version}</Text>
                            </AppTabs>                            
                        </Footer>
                    </AppWrapper>
                </ModalProvider>
            </Main>
        );
    }
};

function mapStateToProps(state) {

    return {
        web3Error: selectors.web3Error(state),
        openedNotificationsCenter: selectors.appOpenedNotificationsCenter(state)
    };
}

const mapDispatchToProps = dispatch => {

    return {
        routePush: newRoute => dispatch(push(newRoute))
    };
};

const connectedApp = connect(mapStateToProps, mapDispatchToProps)(App);

export default withRouter(connectedApp);

