import en from './lot.en.json';
import ru from './lot.ru.json';
import uk from './lot.uk.json';

export default {
    en,
    ru,
    uk
};