import React from 'react';
import loadable from '@loadable/component';
import { PageLoader } from '../../components/Loader';

const LoadableLot = loadable(() => import('./Lot'), {
    fallback: <PageLoader />
});

export const route = [
    {
        path: '/lot/:lotId',
        component: LoadableLot,
        noMenu: true
    }
];
