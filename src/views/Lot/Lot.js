import React, { Component } from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import { withTranslation } from 'react-i18next';
import { Text, IconArrowRight, Button, Info } from '@aragon/ui';

// System imports
import config from '../../config';
import * as selectors from '../../store/selectors';
import * as actions from '../../store/actions';
import { formatTokenBalance } from '../../utils/tokens.helpers';
import {
    getLotDetails, 
    estimateTokensMethod, 
    estimateMarketMethod, 
    allowTokens,
    disallowTokens,
    tokenAllowance, 
    purchaseLot
} from '../../store/services/market';
import { getSelectedAddress } from '../../store/services/web3';

// Custom components
import { Page, PageBar, PageContent, LeftPanel, RightPanel, LeftPanelFooter } from '../../components/Layout';
import Loader from '../../components/Loader';
import { StepInfo, StepsList, StepCard } from '../../components/Wizard';
import EtherLink, { EtherAnchor, etherLink } from '../../components/EtherLink';
import TransactionCard from '../../components/TransactionCard';

const LotDetailsOuter = styled.div`
& {
    padding: 30px 30px 0 30px;
}
`;

const LotTitleOuter = styled.div`
& {
    display: flex;
    flex-direction: row;
    flex-wrap: nowrap;
    align-items: stretch;    
    justify-content: center;
}

div {
    margin-right: 10px;
    word-break: break-all;
}
`;

const LotTitle = ({ items = [] }) => {

    return (
        <LotTitleOuter>
            {items.map((cmp, index) => (
                <div key={index}>
                    {cmp}
                </div>
            ))}
        </LotTitleOuter>
    );
};

class Lot extends Component {

    static config = [
        {
            title: 'lot.steps.0.title',
            info: [
                'lot.steps.0.info.0',
                'lot.steps.0.info.1',
                'lot.steps.0.info.2'
            ],
            tx: {
                0: [
                    'lot.steps.0.tx.0.0',
                    'lot.steps.0.tx.0.1',
                    'lot.steps.0.tx.0.2',
                    'lot.steps.0.tx.0.3'
                ],
                1: [
                    'lot.steps.0.tx.1.0',
                    'lot.steps.0.tx.1.1',
                    'lot.steps.0.tx.1.2'
                ]
            },
            errors: [
                'lot.steps.0.errors.0',
                'lot.steps.0.errors.1',
                'lot.steps.0.errors.2',
                'lot.steps.0.errors.3',
                'lot.steps.0.errors.4'
            ],
            validations: [
                'lotDetails',
                'lotActive',
                'positiveBalance',
                'uaxApproved'
            ]
        },
        {
            title: 'lot.steps.1.title',
            info: [
                'lot.steps.1.info.0',
                'lot.steps.1.info.1'
            ],
            tx: {
                0: [
                    'lot.steps.1.tx.0.0',
                    'lot.steps.1.tx.0.1',
                    'lot.steps.1.tx.0.2',
                    'lot.steps.1.tx.0.3'
                ]
            },
            errors: [
                'lot.steps.1.errors.0',
                'lot.steps.1.errors.1'
            ],
            validations: [
                'lotBought'
            ]
        }
    ];

    static defaultStepState = {
        loading: false,
        done: false
    };

    static defaultDataSet = {
        lotDetails: false,
        lotActive: false,
        positiveBalance: false,
        uaxApproved: false,
        lotBought: false
    };

    constructor(props) {
        super(props);

        this.state = this.getDefaultState();
    }

    setStateAsync = state => new Promise(resolve => this.setState(state, resolve));

    isLoading = () => this.state.steps[this.state.step] && this.state.steps[this.state.step].loading;

    getDefaultStepState = (step = 0) => {
        return {
            ...Lot.defaultStepState
        };
    };

    getDefaultState = () => {
        return {
            step: 0,
            steps: {
                0: this.getDefaultStepState(0)
            },
            errors: {
                0: []
            },
            data: {
                ...Lot.defaultDataSet
            },
            validations: Object.fromEntries(Object.keys(Lot.defaultDataSet).map(r => [r, false]))
        };
    };

    getErrorByIndex = (step, index) => Lot.config[step].errors[index];

    goForward = () => {

        if (this.isLoading()) {
            return;
        }

        const currentStep = this.state.step;
        const nextStep = this.state.step + 1;
        
        if (nextStep <= Lot.config.length) {

            this.setState({
                step: nextStep,
                steps: {
                    ...this.state.steps,
                    [currentStep]: {
                        ...this.state.steps[currentStep],
                        ...{
                            done: true
                        }
                    },
                    [nextStep]: this.getDefaultStepState(nextStep)
                },
                errors: {
                    ...this.state.errors,
                    [currentStep]: {},
                    [nextStep]: {}
                }
            });
        }
    };

    goBack = () => {

        if (this.isLoading()) {
            return;
        }

        const nextStep = this.state.step - 1;

        if (nextStep >= 0) {

            const { [this.state.step]: prevSteps, ...nextSteps } = this.state.steps;
            const { [this.state.step]: prevErrors, ...nextErrors } = this.state.errors;

            this.setState({
                step: nextStep,
                results: nextSteps,
                errors: nextErrors
            });
        } else {

            this.setState(this.getDefaultState());
        }
    };

    setStepError = (step, index, async = false) => {
        
        const newState = {
            errors: {
                ...this.state.errors,
                ...{
                    [step]: {
                        ...this.state.errors[step],
                        [index]: this.getErrorByIndex(step, index)
                    }
                }
            },
            steps: {
                ...this.state.steps,
                ...{
                    [step]: {
                        ...this.state.steps[step],
                        ...{
                            done: false
                        }
                    }
                }                
            }               
        };
        
        return async ? this.setStateAsync(newState) : this.setState(newState);
    };

    unsetStepError = (step, index, async = false) => {

        const { [index]: removedError, ...restErrors } = this.state.errors[step];
        const newState = {
            errors: {
                ...this.state.errors,
                ...{
                    [step]: {
                        ...restErrors
                    }
                }                
            }                
        };

        return async ? this.setStateAsync(newState) : this.setState(newState);
    };

    setStepLoading = (step, async = false) => {

        const newState = {
            steps: {
                ...this.state.steps,
                ...{
                    [step]: {
                        ...this.state.steps[step],
                        ...{
                            loading: true
                        }
                    }
                }
            }
        };

        return async ? this.setStateAsync(newState) : this.setState(newState);
    };

    unsetStepLoading = (step, async = false) => {

        const newState = {
            steps: {
                ...this.state.steps,
                ...{
                    [step]: {
                        ...this.state.steps[step],
                        ...{
                            loading: false
                        }
                    }
                }
            }
        };

        return async ? this.setStateAsync(newState) : this.setState(newState);
    };

    setData = (data, async = false) => {

        const newState = {
            data: {
                ...this.state.data,
                ...data
            }
        };

        return async ? this.setStateAsync(newState) : this.setState(newState);
    };

    setValidated = (names = [], async = false) => {

        const newState = {
            validations: {
                ...this.state.validations,
                ...Object.fromEntries(names.map(r => [r, true]))
            }
        };

        return async ? this.setStateAsync(newState) : this.setState(newState);
    };

    unsetValidated = (names = [], async = false) => {

        const newState = {
            validations: {
                ...this.state.validations,
                ...Object.fromEntries(names.map(r => [r, false]))
            }
        };

        return async ? this.setStateAsync(newState) : this.setState(newState);
    };

    validateBalance = async () => {
        const { uaxBalance, uaxBalanceFetching, uaxDetails } = this.props;
        const { data: { lotDetails } } = this.state;
        
        try {

            await new Promise(resolve => setTimeout(resolve, 150));

            if (uaxBalance !== undefined && uaxBalanceFetching) {
                await new Promise(resolve => setTimeout(resolve, 1000));
            }

            if (window.web3.utils.toBN(uaxBalance).gte(window.web3.utils.toBN(lotDetails.price))) {

                const allowance = await tokenAllowance(uaxDetails.address, getSelectedAddress());
                let uaxApproved = false;

                if (allowance && window.web3.utils.toBN(allowance).gte(window.web3.utils.toBN(lotDetails.price))) {
                    
                    uaxApproved = {
                        transaction: true
                    };
                }
                
                await this.unsetStepError(0, 2, true);
                await this.setData({
                    positiveBalance: true,
                    uaxApproved
                });
                await this.setValidated(['positiveBalance'], true);
                
                if (uaxApproved) {
                    await this.setValidated(['uaxApproved'], true);
                }
                
            } else {

                await this.setStepError(0, 2, true);
                await this.unsetValidated(['positiveBalance'], true);
                await this.unsetValidated(['uaxApproved'], true);
            }

        } catch(error) {
            await this.setStepError(0, 2, true);
            await this.unsetValidated(['positiveBalance'], true);
            await this.unsetValidated(['uaxApproved'], true);
        }
    };

    revalidateBalance = () => {
        const { uaxBalanceFetch } = this.props;

        this.setStepLoading(0);
        uaxBalanceFetch();

        setTimeout(async () => {
            await this.validateBalance().catch(console.log);
            await this.unsetStepLoading(0, true);
        }, 450);
    };

    validateLotStatus = async (lot) => {

        try {

            if (lot && lot.closed === false) {
                
                await this.unsetStepError(0, 1, true);
                await this.setData({
                    lotActive: true
                });
                await this.setValidated(['lotActive'], true);
                await this.validateBalance();
            } else {

                await this.setStepError(0, 1, true);
                await this.unsetValidated(['lotActive'], true);
            }

        } catch(error) {
            await this.setStepError(0, 1, true);
            await this.unsetValidated(['lotActive'], true);
        }
    };

    fetchLotDetails = async () => {
        const { match: { params: { lotId }} } = this.props;

        try {

            await this.setStepLoading(0, true);    
            const lotDetails = await getLotDetails(lotId);
            await this.unsetStepError(0, 0, true);
            await this.setData({
                lotDetails
            });
            await this.setValidated(['lotDetails'], true);
            await this.unsetStepLoading(0, true);
            await this.validateLotStatus(lotDetails);
        } catch(error) {
            await this.setStepError(0, 0, true);
            await this.unsetValidated(['lotDetails'], true);
            await this.unsetStepLoading(0, true);
        }
    };

    isStepValidated = step => {
        const stepConfig = Lot.config[step].validations;
        let allValidated = 0;

        for (let key of stepConfig) {

            if (this.state.validations[key]) {
                allValidated += 1;
            }
        }

        return allValidated === stepConfig.length;
    }

    componentDidMount = async () => {
        const { lotDetails } = this.state;

        if (!lotDetails) {

            await this.fetchLotDetails().catch(console.log);
        }
    };

    render() {
        const {
            loadingDetails,   
            step, steps, data, validations, errors
        } = this.state;
        const { t, uaxDetails } = this.props;
        const stepConfig = Lot.config[step];
        const isLoading = this.isLoading(step);
        const isStepValidated = this.isStepValidated(step);
        const isFinalised = step === 1 && isStepValidated;

        return (
            <div>
                <PageBar
                    title={t('trades.route.label')}
                >
                    {loadingDetails &&
                        <Loader />
                    }
                    {data.lotDetails &&
                        <LotTitle 
                            items={[
                                <Text>{t('lot.title')}</Text>,
                                <IconArrowRight width={12} height={12} />,
                                <Text>
                                    {formatTokenBalance(data.lotDetails.value, data.lotDetails.tokenDetails.decimals)}&nbsp;
                                    {data.lotDetails.tokenDetails.symbol}
                                </Text>
                            ]}
                        />
                    }
                </PageBar>
                <Page>
                    <PageContent>
                        <LeftPanel>
                            {loadingDetails &&
                                <Loader
                                    label={t('trades.labels.loading')}
                                />
                            }
                            {data.lotDetails &&
                                <LotDetailsOuter>
                                    <Text.Paragraph>
                                        <Text size="large">
                                            <strong>{t('create.wizard.forms.fields.labels.lotvalue')}:</strong>&nbsp; 
                                            {formatTokenBalance(data.lotDetails.value, data.lotDetails.tokenDetails.decimals)}&nbsp; 
                                            <EtherAnchor
                                                network={config.network}
                                                type="token"
                                                address={data.lotDetails.tokenDetails.address}
                                            >
                                                <Text>{data.lotDetails.tokenDetails.symbol}</Text>
                                            </EtherAnchor> 
                                        </Text>
                                    </Text.Paragraph>
                                    <Text.Paragraph>
                                        <Text size="large">
                                            <strong>{t('create.wizard.forms.fields.labels.lotprice')}:</strong> {formatTokenBalance(data.lotDetails.price, uaxDetails.decimals)} {uaxDetails.symbol}
                                        </Text>
                                    </Text.Paragraph>
                                    <Text.Paragraph>
                                        <Text size="large">
                                            <strong>{t('create.wizard.forms.fields.labels.servicefee')}:</strong> {formatTokenBalance(data.lotDetails.fee, uaxDetails.decimals)} {uaxDetails.symbol}
                                        </Text>
                                    </Text.Paragraph>
                                </LotDetailsOuter>
                            }
                            {step === 0 &&
                                <StepCard
                                    config={Lot.config}
                                    step={0} 
                                    steps={steps}
                                    validations={validations}
                                    errors={errors} 
                                    onCancel={() => this.goBack()} 
                                    onNext={() => this.goForward()}
                                >
                                    {isLoading && 
                                        <Loader />
                                    }
                                    {(data.lotDetails && data.lotActive && !data.positiveBalance) &&
                                        <Button
                                            mode="secondary"
                                            size="small" 
                                            onClick={() => this.revalidateBalance()}
                                        >
                                            Check balance
                                        </Button>
                                    }
                                    {data.uaxApproved &&
                                        <Info>
                                            {t('lot.steps.0.txtitles.allowed', {
                                                lotPrice: formatTokenBalance(data.lotDetails.price, uaxDetails.decimals),
                                                symbol: uaxDetails.symbol
                                            })}
                                        </Info>
                                    }
                                    {(data.lotDetails && data.positiveBalance) &&
                                        <TransactionCard 
                                            savedState={data.uaxApproved}
                                            header={
                                                <h2>
                                                    {t(`lot.steps.0.txtitles.${data.uaxApproved ? 'disallow' : 'allow'}`, {
                                                        lotPrice: formatTokenBalance(data.lotDetails.price, uaxDetails.decimals),
                                                        symbol: uaxDetails.symbol
                                                    })}&nbsp; 
                                                    <EtherLink network={config.network} address={config.contract} type="address" short={true}/>
                                                </h2>
                                            }
                                            info={Lot.config[0].tx[data.uaxApproved ? 1 : 0]} 
                                            estimationConfig={{
                                                action: estimateTokensMethod,
                                                options: [
                                                    'approve',
                                                    uaxDetails.address,
                                                    [
                                                        config.contract,
                                                        data.lotDetails.price
                                                    ]
                                                ],
                                                resetOptions: [
                                                    'decreaseAllowance',
                                                    uaxDetails.address,
                                                    [
                                                        config.contract,
                                                        data.lotDetails.price
                                                    ]
                                                ],
                                                onError: error => {
                                                    this.setStepError(0, 3, false);
                                                    this.props.notificationAdd({
                                                        type: 'error',
                                                        title: t('lot.notifications.common.actiontitle', {
                                                            title: t(stepConfig.title)
                                                        }),
                                                        date: new Date().toISOString(),
                                                        message: t('lot.notifications.error.estimation'),
                                                        details: {
                                                            error
                                                        }
                                                    });
                                                }
                                            }}
                                            transactionConfig={{
                                                action: allowTokens,
                                                resetAction: disallowTokens,
                                                options: [
                                                    uaxDetails.address, 
                                                    data.lotDetails.price
                                                ],
                                                onError: error => {
                                                    this.setStepError(0, 4, false);
                                                    this.props.notificationAdd({
                                                        type: 'error',
                                                        title: t('lot.notifications.common.actiontitle', {
                                                            title: t(stepConfig.title)
                                                        }),
                                                        date: new Date().toISOString(),
                                                        message: t('lot.notifications.error.txfail'),
                                                        details: {
                                                            error
                                                        }
                                                    });
                                                },
                                                onSuccess: state => {
                                                    this.setData({
                                                        uaxApproved: state
                                                    });
                                                    this.setValidated(['uaxApproved']);
                                                    this.props.notificationAdd({
                                                        type: 'info',
                                                        title: t('lot.notifications.common.actiontitle', {
                                                            title: t(stepConfig.title)
                                                        }),
                                                        date: new Date().toISOString(),
                                                        message: t('lot.notifications.info.approval', {
                                                            symbol: uaxDetails.symbol,
                                                            lotPrice: formatTokenBalance(data.lotDetails.price, uaxDetails.decimals),
                                                            etherLink: etherLink(config.network, 'tx', state.transactionHash)
                                                        }),
                                                        details: {
                                                            receipt: state.receipt,
                                                            transaction: state.transaction
                                                        }
                                                    });
                                                },
                                                onReset: state => {
                                                    this.setData({
                                                        uaxApproved: false
                                                    });
                                                    this.unsetValidated(['uaxApproved']);
                                                    this.props.notificationAdd({
                                                        type: 'warn',
                                                        title: t('lot.notifications.common.actiontitle', {
                                                            title: t(stepConfig.title)
                                                        }),
                                                        date: new Date().toISOString(),
                                                        message: t('lot.notifications.warn.approval', {
                                                            symbol: uaxDetails.symbol,
                                                            lotPrice: formatTokenBalance(data.lotDetails.price, data.lotDetails.tokenDetails.decimals),
                                                            etherLink: etherLink(config.network, 'tx', state.transactionHash)
                                                        }),
                                                        details: {
                                                            receipt: state.receipt,
                                                            transaction: state.transaction
                                                        }
                                                    });
                                                }
                                            }}                                        
                                        />
                                    }                                                                
                                </StepCard> 
                            }
                            {step === 1 &&
                                <StepCard
                                    config={Lot.config}
                                    step={1} 
                                    steps={steps}
                                    validations={validations}
                                    errors={errors} 
                                    onCancel={() => this.goBack()} 
                                    onNext={() => {}}
                                    finalCard={
                                        <div>
                                            <h1>{t('lot.final.success')}</h1>
                                            <h3>{t('lot.final.check')}</h3>
                                        </div>
                                    }
                                >
                                    {isLoading && 
                                        <Loader />
                                    }
                                    <TransactionCard 
                                        savedState={data.lotBought}
                                        header={
                                            <h2>
                                                {t('lot.steps.1.txtitles.buy', {
                                                    lotValue: formatTokenBalance(data.lotDetails.value, data.lotDetails.tokenDetails.decimals),
                                                    lotPrice: formatTokenBalance(data.lotDetails.price, uaxDetails.decimals),
                                                    symbol: data.lotDetails.tokenDetails.symbol,
                                                    uaxSymbol: uaxDetails.symbol
                                                })}
                                            </h2>
                                        }
                                        info={Lot.config[1].tx[0]} 
                                        estimationConfig={{
                                            action: estimateMarketMethod,
                                            options: [
                                                'purchase',
                                                [
                                                    data.lotDetails.id
                                                ]
                                            ],
                                            onError: error => {
                                                this.setStepError(1, 0, false);
                                                this.props.notificationAdd({
                                                    type: 'error',
                                                    title: t('lot.notifications.common.actiontitle', {
                                                        title: t(stepConfig.title)
                                                    }),
                                                    date: new Date().toISOString(),
                                                    message: t('lot.notifications.error.estimation'),
                                                    details: {
                                                        error
                                                    }
                                                });
                                            }
                                        }}
                                        transactionConfig={{
                                            action: purchaseLot,
                                            options: [
                                                data.lotDetails.id
                                            ],
                                            onError: error => {
                                                this.setStepError(1, 1, false);
                                                this.props.notificationAdd({
                                                    type: 'error',
                                                    title: t('lot.notifications.common.actiontitle', {
                                                        title: t(stepConfig.title)
                                                    }),
                                                    date: new Date().toISOString(),
                                                    message: t('lot.notifications.error.txfail'),
                                                    details: {
                                                        error
                                                    }
                                                });
                                            },
                                            onSuccess: state => {
                                                this.setData({
                                                    lotBought: state
                                                });
                                                this.setValidated(['lotBought']);
                                                this.props.notificationAdd({
                                                    type: 'info',
                                                    title: t('lot.notifications.common.actiontitle', {
                                                        title: t(stepConfig.title)
                                                    }),
                                                    date: new Date().toISOString(),
                                                    message: t('lot.notifications.info.buying', {
                                                        etherLink: etherLink(config.network, 'tx', state.transactionHash)
                                                    }),
                                                    details: {
                                                        receipt: state.receipt,
                                                        transaction: state.transaction
                                                    }
                                                });
                                            }
                                        }}                                        
                                    />                                                                
                                </StepCard>
                            }
                            {!isFinalised &&
                                <LeftPanelFooter>
                                    <StepsList 
                                        config={Lot.config}
                                        step={step}
                                        steps={steps}                                    
                                    />
                                </LeftPanelFooter>
                            }
                        </LeftPanel>
                        <RightPanel>
                            {!isFinalised &&
                                <div>
                                    <StepInfo step={step} config={Lot.config} />
                                </div>
                            }                            
                        </RightPanel>
                    </PageContent>
                </Page>
            </div>
        );
    }
}

function mapStateToProps(state) {

    return {
        uaxDetails: selectors.uaxDetails(state),
        uaxBalance: selectors.uaxBalance(state),
        uaxBalanceFetching: selectors.uaxBalanceFetching(state)
    };
}

const mapDispatchToProps = dispatch => {

    return {
        notificationAdd: record => dispatch(actions.appNotificationAdd(record)),
        uaxBalanceFetch: () => dispatch(actions.uaxBalanceFetch())
    };
};

const connectedLot= withTranslation()(connect(mapStateToProps, mapDispatchToProps)(Lot));

export default connectedLot;