export const isFirefoxMobile = 
    navigator.userAgent.toLowerCase().indexOf('firefox') > -1 && 
    navigator.userAgent.toLowerCase().indexOf('android') > -1;