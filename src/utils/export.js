import { stringifyCircular } from './json';

export const toJsonFile = (data = {}) => {
    const dataStr = stringifyCircular(data);
    let time = new Date();
    const formattedDate = new Date(time - time.getTimezoneOffset()*60*1000)
        .toISOString()
        .slice(0, 16)
        .replace(/[T:-]+/gi, '');
    const linkElement = document.createElement('a');
    linkElement.setAttribute('href', `data:application/json;charset=utf-8,${encodeURIComponent(dataStr)}`);
    linkElement.setAttribute('download', `notifications-${formattedDate}.json`);
    linkElement.click();
};

