/**
 * Format token balance
 * @param {Number} balance
 * @param {Number} decimals
 * @returns {String}
 */
export const formatTokenBalance = (balance, decimals = 0) => {
    return (Number(balance) / (decimals ? Math.pow(10, decimals) : 1)).toFixed(2);
};

export const perOne = (price, priceDecimals, value, valueDecimals) => {
    return (Number(formatTokenBalance(price, priceDecimals)) / Number(formatTokenBalance(value, valueDecimals))).toFixed(2);
};

/**
 * Format token balance
 * @param {Number} value
 * @param {Number} decimals
 * @returns {Object} BN number
 */
export const valueToBN = (value, decimals) => {
    const bnValue = window.web3.utils.toBN(value.toString());
    const multiplier = window.web3.utils.toBN(Math.pow(10, decimals));
    return bnValue.mul(multiplier);
};

