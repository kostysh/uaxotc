import PromiEvent from './promievent';

export const isTxConfirmed = async (transaction, minConfirmations, event = false) => {    
    const currentBlockNumber = await window.web3.eth.getBlockNumber();
    let confirmationsNumber = 0;

    if (transaction.blockHash !== null) {
        confirmationsNumber = currentBlockNumber - transaction.blockNumber;
        
        if (event) {
            event.emit('confirmations', confirmationsNumber);
        }
    }

    return confirmationsNumber >= minConfirmations;
};

export const fetchTransaction = async (transactionHash) => {
    return await window.web3.eth.getTransaction(transactionHash);
};

export const watchTransaction = (
    receipt, 
    minConfirmations, 
    pendingTimeout = 10*60*1000
) => new PromiEvent(async (resolve, reject, event) => {

    try {

        const startTime = Date.now();
        let watchInterval;               

        watchInterval = setInterval(async () => {

            try {

                if ((Date.now() - startTime) >= pendingTimeout) {

                    reject(new Error('Transaction pending time exceeded'));
                    clearInterval(watchInterval);
                    return;
                }

                const transaction = await fetchTransaction(receipt.transactionHash); 
                event.emit('transaction', transaction);
    
                if (await isTxConfirmed(transaction, minConfirmations, event)) {
                    
                    resolve(transaction);
                    clearInterval(watchInterval);
                    return;
                }
            } catch (err) {

                reject(err);
                clearInterval(watchInterval);
            }
        }, 1000);

    } catch (err) {
        reject(err);
    }
});

