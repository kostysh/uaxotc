module.exports = {
    network: 3,
    contract: '0xbf32A4876E2f1551a014513f6328214677cd81fB',
    confirmations: 10,
    owner: '0x2Ff7da33864Ca79FAbfa4DA12a485CA5ADF78b30',
    projectId: '9bf46a9f731e411bb1d7fa1924883f60',
    interval: {
        balance: 2000
    }
};
