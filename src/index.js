import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { ConnectedRouter } from 'connected-react-router';
import { I18nextProvider } from 'react-i18next';

import { store, history } from './store';
import App from './views/App';
import * as serviceWorker from './serviceWorker';
import i18n from './i18n';

const render = () => {
    ReactDOM.render(
        <I18nextProvider i18n={i18n}>
            <Provider store={store}>
                <ConnectedRouter history={history}>
                    <App />
                </ConnectedRouter>
            </Provider>
        </I18nextProvider>,
        document.getElementById('root'));
};

render();

if (process.env.NODE_ENV !== 'production' && module.hot) {

    module.hot.accept('./views/App', () => render());
}

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
