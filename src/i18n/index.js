import i18n from 'i18next';
import Backend from 'i18next-chained-backend';
import { initReactI18next } from 'react-i18next';
import LanguageDetector from 'i18next-browser-languagedetector';
import LocalStorageBackend from 'i18next-localstorage-backend';
import {
    en,
    ru,
    uk
} from './locales';

const options = {
    interpolation: {
        escapeValue: false
    },

    debug: false,

    resources: {
        en: {
            common: en,
        },
        ru: {
            common: ru,
        },
        uk: {
            common: uk,
        }
    },

    fallbackLng: 'en',

    ns: ['common'],

    defaultNS: 'common',

    react: {
        useSuspense: false,
        wait: false,
        bindI18n: 'languageChanged loaded',
        bindStore: 'added removed',
        nsMode: 'default'
    },

    backend: {
        backends: [
            LocalStorageBackend
        ],
        backendOptions: [
            {
                prefix: 'i18next_res_',
                expirationTime: 365*24*60*60*1000,
                store: window.localStorage
            }
        ]
    }
};

i18n
    .use(LanguageDetector)
    .use(initReactI18next)
    .use(Backend)
    .init(options)
    .catch(err => console.log('Unable to change language:', err));

export default i18n;