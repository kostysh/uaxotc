// Default global translations 
import enDefault from './default.en.json';
import ruDefault from './default.ru.json';
import ukDefault from './default.uk.json';

// Components translations
import Notifications from '../../components/Notifications/locales';
import Wallet from '../../components/Wallet/locales';
import EtherLink from '../../components/EtherLink/locales';
import TransactionCard from '../../components/TransactionCard/locales';
import LotsTable from '../../components/LotsTable/locales';
import CloseCard from '../../components/CloseCard/locales';

// Pages translations
import Trades from '../../views/Trades/locales';
import Lot from '../../views/Lot/locales';
import CreateLot from '../../views/CreateLot/locales';
import MyLots from '../../views/MyLots/locales';
import Qa from '../../views/Qa/locales';

// Services translations
import Services from '../../store/services/locales';

const defaultTranslations = {
    en: enDefault,
    ru: ruDefault,
    uk: ukDefault
};

const allTranslations = [
    defaultTranslations,
    Notifications,
    Wallet,
    EtherLink,
    TransactionCard,
    LotsTable,
    CloseCard,
    Trades,
    Lot,
    CreateLot,
    MyLots,
    Qa,
    Services
];

const joinTranslationsByLang = (translations, langCode) => {
    return Object.assign.apply(undefined, translations.map(t => t[langCode]));
};

export const en = joinTranslationsByLang(allTranslations, 'en');
export const ru = joinTranslationsByLang(allTranslations, 'ru');
export const uk = joinTranslationsByLang(allTranslations, 'uk');

