import {
    applyLotsParams,
    getLotsPaginated,
    getLotsTokens
} from './lots.helpers';

export const tradesLotsFetching = state => state.trades.lotsFetching;
export const tradesLotsFetchingProgress = state => state.trades.lotsFetchingTotal 
    ? Math.floor(state.trades.lotsFetchingCount * 100 / state.trades.lotsFetchingTotal) 
    : 0;
export const tradesLots = state => getLotsPaginated(state);
export const tradesLotsParams = state => state.trades.lotsParams;
export const tradesBlockNumber = state => state.trades.blockNumber;
export const tradesLotsPagination = state => ({
    ...state.trades.pagination,
    total: applyLotsParams(state).length
});
export const tradesLotsTokens = state => getLotsTokens(state);
export const tradesError = state => state.trades.error;
