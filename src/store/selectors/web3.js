export const web3Error = state => state.web3.errorMessage;
export const web3IsInitializing = state => state.web3.initializing;
export const web3Initialized = state => state.web3.initialized;
