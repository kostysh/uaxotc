import { formatTokenBalance } from '../../utils/tokens.helpers';

export const getPaginationArguments = (state, source = 'trades') => {
    const from = state[source].pagination.page * state[source].pagination.limit;
    const limit = from + state[source].pagination.limit;
    return [from, limit];
};

export const applyLotsParams = (state, source = 'trades') => {
    let records = Object.entries(state[source].records);
    const params = state[source].lotsParams;

    if (params.token) {
        records = records.filter(r => r[1].tokenDetails.symbol === params.token);
    }

    if (!params.closed) {
        records = records.filter(r => !r[1].closed);
    }

    if (params.value || params.price) {

        records = records.sort((ra, rb) => {
            let result = 1;
            const aVal = window.web3.utils.toBN(ra[1].value);
            const bVal = window.web3.utils.toBN(rb[1].value);
            const aPrice = window.web3.utils.toBN(ra[1].price);
            const bPrice = window.web3.utils.toBN(rb[1].price);

            if (params.value && 
                params.value === 'asc' &&
                aVal.sub(bVal).isNeg()) {
                
                result = -1;
            }

            if (params.value && 
                params.value === 'desc' &&
                bVal.sub(aVal).isNeg()) {
                
                result = -1;
            }

            if (params.price && 
                params.price === 'asc' &&
                aPrice.sub(bPrice).isNeg()) {
                
                result = -1;
            }

            if (params.price && 
                params.price === 'desc' &&
                bPrice.sub(aPrice).isNeg()) {
                
                result = -1;
            }

            return result;
        });
    }

    if (params.one) {

        records = records.sort((ra, rb) => {
            let result = 1;
            const aOne = Number(formatTokenBalance(ra[1].price, 2)) / Number(formatTokenBalance(ra[1].value, ra[1].tokenDetails.decimals));
            const bOne = Number(formatTokenBalance(rb[1].price, 2)) / Number(formatTokenBalance(rb[1].value, rb[1].tokenDetails.decimals));

            if (params.one === 'asc' &&
                aOne < bOne) {
                
                result = -1;
            }

            if (params.one === 'desc' &&
                bOne < aOne) {
                
                result = -1;
            }

            return result;
        });
    }

    return records;
};

export const getLotsPaginated = (state, source = 'trades') => {
    const records = applyLotsParams(state, source);
    return records.slice.apply(records, getPaginationArguments(state, source));
};

export const getLotsTokens = (state, source = 'trades') => [...new Set(Object.entries(state[source].records).map(r => r[1].tokenDetails.symbol))];