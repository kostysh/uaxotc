import {
    applyLotsParams,
    getLotsPaginated,
    getLotsTokens
} from './lots.helpers';

export const mylotsFetching = state => state.mylots.lotsFetching;
export const mylotsFetchingProgress = state => state.mylots.lotsFetchingTotal 
    ? Math.floor(state.mylots.lotsFetchingCount * 100 / state.mylots.lotsFetchingTotal) 
    : 0;
export const mylots = state => getLotsPaginated(state, 'mylots');
export const mylotsParams = state => state.mylots.lotsParams;
export const mylotsPagination = state => ({
    ...state.mylots.pagination,
    total: applyLotsParams(state, 'mylots').length
});
export const mylotsTokens = state => getLotsTokens(state, 'mylots');
export const mylotsError = state => state.mylots.error;
