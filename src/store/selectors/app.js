export const appOpenedNotificationsCenter = state => state.app.notifications.opened;
export const appSeenNotifications = state => state.app.notifications.records.length === 0 ? true : state.app.notifications.seen;
export const appNotificationsHasErrors = state => state.app.notifications.records.filter(r => r.type === 'error').length > 0;
export const appNotifications = state => state.app.notifications.records;

