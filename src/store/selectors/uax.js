export const uaxBalanceFetching = state => state.uax.balanceFetching;
export const uaxDetailsFetching = state => state.uax.detailsFetching;
export const uaxError = state => state.uax.error;
export const uaxBalance = state => state.uax.balance;
export const uaxDetails = state => state.uax.details;