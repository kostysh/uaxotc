import { reduxAction as action } from '../../utils/redux.helpers';

export const MYLOTS_FETCH = 'MYLOTS_FETCH';
export const MYLOTS_FETCH_TOTAL = 'MYLOTS_FETCH_TOTAL';
export const MYLOTS_FETCH_COUNT = 'MYLOTS_FETCH_COUNT';
export const MYLOTS_RECEIVED = 'MYLOTS_RECEIVED';
export const MYLOTS_PARAMS_ADD = 'MYLOTS_PARAMS_ADD';
export const MYLOTS_PARAMS_REMOVE = 'MYLOTS_PARAMS_REMOVE';
export const MYLOTS_PAGINATE = 'MYLOTS_PAGINATE';
export const MYLOTS_REFRSH = 'MYLOTS_REFRSH';
export const MYLOTS_ERROR = 'MYLOTS_ERROR';
export const MYLOTS_INVALIDATE_ERROR = 'MYLOTS_INVALIDATE_ERROR';

export const mylotsFetch = () => action(MYLOTS_FETCH);
export const mylotsFetchingTotal = total => action(MYLOTS_FETCH_TOTAL, { total });
export const mylotsFetchingCount = count => action(MYLOTS_FETCH_COUNT, { count });
export const mylotsReceived = records => action(MYLOTS_RECEIVED, { records });
export const mylotsParamsAdd = params => action(MYLOTS_PARAMS_ADD, { params });
export const mylotsParamsRemove = params => action(MYLOTS_PARAMS_REMOVE, { params });
export const mylotsPaginate = pagination => action(MYLOTS_PAGINATE, { pagination });
export const mylotsRefresh = () => action(MYLOTS_REFRSH);
export const mylotsError = error => action(MYLOTS_ERROR, { error });
export const mylotsErrorInvalidate = error => action(MYLOTS_INVALIDATE_ERROR);
