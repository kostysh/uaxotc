import { reduxAction as action } from '../../utils/redux.helpers';

export const UAX_BALANCE_FETCH_TASK = 'UAX_BALANCE_FETCH_TASK';
export const UAX_BALANCE_FETCH = 'UAX_BALANCE_FETCH';
export const UAX_BALANCE_RECEIVED = 'UAX_BALANCE_RECEIVED';
export const UAX_ERROR = 'UAX_ERROR';
export const UAX_DETAILS_FETCH = 'UAX_DETAILS_FETCH';
export const UAX_DETAILS_RECEIVED = 'UAX_DETAILS_RECEIVED';

export const uaxBalanceFetchTask = () => action(UAX_BALANCE_FETCH_TASK);
export const uaxBalanceFetch = () => action(UAX_BALANCE_FETCH);
export const uaxBalanceReceived = (balance, details) => action(UAX_BALANCE_RECEIVED, { balance, details });
export const uaxError = error => action(UAX_ERROR, { error });
export const uaxDetailsFetch = () => action(UAX_DETAILS_FETCH);
export const uaxDetailsReceived = details => action(UAX_DETAILS_RECEIVED, { details });