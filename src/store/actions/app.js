import { reduxAction as action } from '../../utils/redux.helpers';

export const APP_NOTIFICATIONS_TOGGLE = 'APP_NOTIFICATIONS_CENTER_TOGGLE';
export const APP_NOTIFICATIONS_CLEAR = 'APP_NOTIFICATIONS_CLEAR';
export const APP_NOTIFICATIONS_ADD = 'APP_NOTIFICATIONS_ADD';

export const appNotifcationsToggle = () => action(APP_NOTIFICATIONS_TOGGLE);
export const appNotificationsClear = () => action(APP_NOTIFICATIONS_CLEAR);
export const appNotificationAdd = record => action(APP_NOTIFICATIONS_ADD, { record });

