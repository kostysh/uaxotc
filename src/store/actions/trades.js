import { reduxAction as action } from '../../utils/redux.helpers';

export const TRADES_LOTS_SUBSCRIPTION_TASK = 'TRADES_LOTS_SUBSCRIPTION_TASK';
export const TRADES_LOTS_FETCH = 'TRADES_LOTS_FETCH';
export const TRADES_LOTS_FETCH_TOTAL = 'TRADES_LOTS_FETCH_TOTAL';
export const TRADES_LOTS_FETCH_COUNT = 'TRADES_LOTS_FETCH_COUNT';
export const TRADES_LOTS_RECEIVED = 'TRADES_LOTS_RECEIVED';
export const TRADES_LOTS_RESTORED = 'TRADES_LOTS_RESTORED';
export const TRADES_LOTS_REMOVE = 'TRADES_LOTS_REMOVE';
export const TRADES_LOTS_PARAMS_ADD = 'TRADES_LOTS_PARAMS_ADD';
export const TRADES_LOTS_PARAMS_REMOVE = 'TRADES_LOTS_PARAMS_REMOVE';
export const TRADES_LOTS_PAGINATE = 'TRADES_LOTS_PAGINATE';
export const TRADES_BLOCK_NUMBER = 'TRADES_BLOCK_NUMBER';
export const TRADES_ERROR = 'TRADES_ERROR';
export const TRADES_INVALIDATE_ERROR = 'TRADES_INVALIDATE_ERROR';

export const tradesLotsSubscriptionTask = () => action(TRADES_LOTS_SUBSCRIPTION_TASK);
export const tradesLotsParamsAdd = params => action(TRADES_LOTS_PARAMS_ADD, { params });
export const tradesLotsParamsRemove = params => action(TRADES_LOTS_PARAMS_REMOVE, { params });
export const tradesLotsFetch = () => action(TRADES_LOTS_FETCH);
export const tradesLotsFetchingTotal = total => action(TRADES_LOTS_FETCH_TOTAL, { total });
export const tradesLotsFetchingCount = count => action(TRADES_LOTS_FETCH_COUNT, { count });
export const tradesLotsReceived = records => action(TRADES_LOTS_RECEIVED, { records });
export const tradesLotsRestored = () => action(TRADES_LOTS_RESTORED);
export const tradesLotsRemove = ids => action(TRADES_LOTS_REMOVE, { ids });
export const tradesLotsPaginate = pagination => action(TRADES_LOTS_PAGINATE, { pagination });
export const tradesBlockNumber = blockNumber => action(TRADES_BLOCK_NUMBER, { blockNumber });
export const tradesError = error => action(TRADES_ERROR, { error });
export const tradesErrorInvalidate = error => action(TRADES_INVALIDATE_ERROR);
