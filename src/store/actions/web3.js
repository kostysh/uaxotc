import { reduxAction as action } from '../../utils/redux.helpers';

export const WEB3_SETUP = 'WEB3_SETUP';
export const WEB3_INITIALIZED = 'WEB3_INITIALIZED';
export const WEB3_ERROR = 'WEB3_ACCOUNTS_ERROR';
export const WEB3_INVALIDATE_ERROR = 'WEB3_ACCOUNTS_INVALIDATE_ERROR';
export const WEB3_ACCOUNT_CHANGED = 'WEB3_ACCOUNT_CHANGED';

export const web3Setup = () => action(WEB3_SETUP);
export const web3Initialized = () => action(WEB3_INITIALIZED);
export const web3Error = error => action(WEB3_ERROR, { error });
export const web3InvalidateError = () => action(WEB3_INVALIDATE_ERROR);
export const web3AccountChanged = () => action(WEB3_ACCOUNT_CHANGED);

