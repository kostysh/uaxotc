import { all } from 'redux-saga/effects';
import web3 from './web3';
import uax from './uax';
import trades from './trades';
import mylots from './mylots';

export default function* rootSaga() {
    yield all([
        ...web3,
        ...uax,
        ...trades,
        ...mylots
    ]);
}
