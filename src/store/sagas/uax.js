import { all, put, fork, takeLatest } from 'redux-saga/effects';
import { getSelectedAddress } from '../services/web3';
import { startGetUaxBalanceTask, getUaxBalance, getUaxDetails } from '../services/market';

import * as actions from '../actions';

function* startUaxBalanceTask() {
    try {
        yield startGetUaxBalanceTask();
    } catch(err) {
        yield put(actions.uaxError(err));
    }
}

function* fetchUaxBalance() {
    try {
        const balance = yield getUaxBalance(getSelectedAddress());
        const details = yield getUaxDetails();
        yield put(actions.uaxBalanceReceived(balance, details));
    } catch(err) {
        yield put(actions.uaxError(err));
    }
}

function* fetchUaxDetails() {
    try {
        const details = yield getUaxDetails();
        yield put(actions.uaxDetailsReceived(details));        
    } catch(err) {
        yield put(actions.uaxError(err));
    }
}

function* onAccountChanged() {
    yield all([
        put(actions.uaxBalanceFetchTask()),
        put(actions.uaxDetailsFetch())
    ]);
}

function* watchActions() {
    yield takeLatest(actions.UAX_BALANCE_FETCH_TASK, startUaxBalanceTask);
    yield takeLatest(actions.UAX_BALANCE_FETCH, fetchUaxBalance);
    yield takeLatest(actions.UAX_DETAILS_FETCH, fetchUaxDetails);
    yield takeLatest(actions.WEB3_ACCOUNT_CHANGED, onAccountChanged);
}

export default [
    fork(watchActions)
];

