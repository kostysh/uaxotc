import { put, fork, takeLatest, all } from 'redux-saga/effects';
import { setupWeb3, updateSelectedAccount } from '../services/web3';

import * as actions from '../actions';

function* setup() {
    try {
        yield setupWeb3();
        yield put(actions.web3Initialized());
        yield put(actions.web3AccountChanged());
    } catch(err) {
        yield put(actions.web3Error(err));
    }
}

function* updateAccount() {
    try {
        yield updateSelectedAccount();
    } catch(err) {
        yield put(actions.web3Error(err));
    }
}

function* onAccountChanged() {
    yield all([
        updateAccount()
    ]);
}

function* watchActions() {
    yield takeLatest(actions.WEB3_SETUP, setup);
    yield takeLatest(actions.WEB3_ACCOUNT_CHANGED, onAccountChanged);
}

export default [
    fork(watchActions)
];

