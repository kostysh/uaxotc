import { take, select, put, call, fork, takeLatest, all } from 'redux-saga/effects';
import { eventChannel, END } from 'redux-saga';
import {
    getLotsTask, 
    subscribeLotEventsTask
} from '../services/market';

import * as actions from '../actions';
import * as selectors from '../selectors';

const createLotEventsChannel = (eventEmitter) => {

    return eventChannel(emit => {

        const errorHandler = error => emit({ error });
        const createdLotHandler = created => emit({ created });
        const removedLotHandler = removed => emit({ removed });
        const purchasedLotHandler = purchased => emit({ purchased });

        try {
            eventEmitter
                .on('LotCreated', createdLotHandler)
                .on('LotRemoved', removedLotHandler)
                .on('LotPurchased', purchasedLotHandler)
                .on('error', errorHandler);
            eventEmitter.start();
        } catch(err) {
            errorHandler(err);
            emit(END);
        }

        return () => eventEmitter.removeAllListeners();
    });
};

function* onLotsEventsError(error) {
    yield put(actions.tradesError(error));
    yield put(actions.appNotificationAdd({
        type: 'error',
        title: 'Lots subscription error',
        date: new Date().toISOString(),
        message: error.message,
        details: {
            error
        }
    }));
}

function* subscribeLotsEvents() {

    try {
        const blockNumber = yield select(selectors.tradesBlockNumber);
        const lotsEventsTask = yield call(subscribeLotEventsTask, blockNumber);
        const lotsEventsChannel = yield call(createLotEventsChannel, lotsEventsTask);

        while (true) {
            const { created, removed, purchased, error } = yield take(lotsEventsChannel);

            if (created) {
                yield put(actions.tradesLotsReceived([created.lot]));
                yield put(actions.tradesBlockNumber(created.blockNumber));
            }

            if (removed) {
                yield put(actions.tradesLotsRemove([removed.id]));
                yield put(actions.tradesBlockNumber(removed.blockNumber));
            }

            if (purchased) {
                yield put(actions.tradesLotsRemove([purchased.id]));
                yield put(actions.tradesBlockNumber(purchased.blockNumber));
            }

            if (error) {
                yield onLotsEventsError(error);       
            }
        }
        
    } catch(error) {
        yield onLotsEventsError(error);
    }
}

const createFetchLotsChannel = (fetchEventEmitter) => {

    return eventChannel(emit => {

        const idsHandler = ids => emit({ ids });
        const blockNumberHandler = blockNumber => emit({ blockNumber });
        const lotHandler = lot => emit({ lot });
        const errorHandler = error => emit({ error });
        const lotsHandler = lots => emit({ lots });

        try {
            fetchEventEmitter
                .on('ids', idsHandler)
                .on('blockNumber', blockNumberHandler)
                .on('lot', lotHandler)
                .on('error', errorHandler)
                .on('lots', lots => {
                    lotsHandler(lots);
                    emit(END);
                });
            fetchEventEmitter.start();
        } catch(err) {
            errorHandler(err);
            emit(END);
        }

        return () => fetchEventEmitter.removeAllListeners();
    });
};

function* onFetchLotsError(error) {
    yield put(actions.tradesError(error));
    yield put(actions.appNotificationAdd({
        type: 'error',
        title: 'Lots fetching error',
        date: new Date().toISOString(),
        message: error.message,
        details: {
            error
        }
    }));
}

function* fetchLots() {
    
    try {
        const lots = yield select(selectors.tradesLots);
        
        if (lots && lots.length > 0) {
            
            yield put(actions.tradesLotsRestored());
            yield put(actions.tradesLotsSubscriptionTask());
            return;
        }

        const fetchLotsTask = yield call(getLotsTask);
        const fetchLotsChannel = yield call(createFetchLotsChannel, fetchLotsTask);

        while (true) {
            const { ids, blockNumber, lot, lots, error } = yield take(fetchLotsChannel);
            
            if (ids) {
                yield put(actions.tradesLotsFetchingTotal(ids.length));
            }

            if (blockNumber) {
                yield put(actions.tradesBlockNumber(blockNumber));
            }

            if (lot) {
                yield put(actions.tradesLotsFetchingCount(1));
            }

            if (lots) {
                yield put(actions.tradesLotsReceived(lots));
                yield put(actions.tradesLotsSubscriptionTask());
            }

            if (error) {
                yield onFetchLotsError(error);       
            }
        }
        
    } catch(error) {
        yield onFetchLotsError(error);
    }
}

function* onAccountChanged() {
    yield all([
        put(actions.tradesLotsFetch())
    ]);
}

function* watchActions() {
    yield takeLatest(actions.TRADES_LOTS_SUBSCRIPTION_TASK, subscribeLotsEvents);
    yield takeLatest(actions.TRADES_LOTS_FETCH, fetchLots);
    yield takeLatest(actions.WEB3_ACCOUNT_CHANGED, onAccountChanged);
}

export default [
    fork(watchActions)
];

