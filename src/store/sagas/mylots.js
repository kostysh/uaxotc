import { take, put, call, fork, takeLatest, all } from 'redux-saga/effects';
import { eventChannel, END } from 'redux-saga';
import {
    getLotsTask
} from '../services/market';

import * as actions from '../actions';

const createFetchMyLotsChannel = (fetchEventEmitter) => {

    return eventChannel(emit => {

        const idsHandler = ids => emit({ ids });
        const lotHandler = lot => emit({ lot });
        const errorHandler = error => emit({ error });
        const lotsHandler = lots => emit({ lots });

        try {
            fetchEventEmitter
                .on('ids', idsHandler)
                .on('lot', lotHandler)
                .on('error', errorHandler)
                .on('lots', lots => {
                    lotsHandler(lots);
                    emit(END);
                });
            fetchEventEmitter.start();
        } catch(err) {
            errorHandler(err);
            emit(END);
        }

        return () => {
            fetchEventEmitter.removeAllListeners();
        };
    });
};

function* onFetchMyLotsError(error) {
    yield put(actions.tradesError(error));
    yield put(actions.appNotificationAdd({
        type: 'error',
        title: 'My Lots fetching error',
        date: new Date().toISOString(),
        message: error.message,
        details: {
            error
        }
    }));
}

function* fetchMyLots() {
    
    try {
        
        const fetchMyLotsTask = yield call(getLotsTask, 'ownedLots()');
        const fetchMyLotsChannel = yield call(createFetchMyLotsChannel, fetchMyLotsTask);

        while (true) {
            const { ids, lot, lots, error } = yield take(fetchMyLotsChannel);
            
            if (ids) {
                yield put(actions.mylotsFetchingTotal(ids.length));
            }

            if (lot) {
                yield put(actions.mylotsFetchingCount(1));
            }

            if (lots) {
                yield put(actions.mylotsReceived(lots));
            }

            if (error) {
                yield onFetchMyLotsError(error);       
            }
        }
        
    } catch(error) {
        yield onFetchMyLotsError(error);
    }
}

function* onAccountChanged() {
    yield new Promise(resolve => setTimeout(resolve, 3000));
    yield all([
        put(actions.mylotsFetch())
    ]);
}

function* watchActions() {
    yield takeLatest(actions.MYLOTS_FETCH, fetchMyLots);
    yield takeLatest(actions.WEB3_ACCOUNT_CHANGED, onAccountChanged);
    yield takeLatest(actions.MYLOTS_REFRSH, onAccountChanged);
}

export default [
    fork(watchActions)
];

