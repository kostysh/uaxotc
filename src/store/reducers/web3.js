import {
    WEB3_INITIALIZED,
    WEB3_SETUP,
    WEB3_ERROR,
    WEB3_INVALIDATE_ERROR
} from '../actions';

const initialState = {
    initialized: false,
    initializing: false,
    errorMessage: null
};

export const reduce = (state = initialState, action = {}) => {

    switch (action.type) {

        case WEB3_INITIALIZED:
            return {
                ...state,
                initializing: false,
                initialized: true
            };
        
        case WEB3_SETUP:
            return { 
                ...state,
                initializing: true,
                errorMessage: null 
            };

        case WEB3_ERROR:
            return { 
                ...state, 
                initializing: false,
                errorMessage: action.error.message 
            };

        case WEB3_INVALIDATE_ERROR:
            return { 
                ...state, 
                errorMessage: null 
            };
        
        default:
            return state;
    }
};
