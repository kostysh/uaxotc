import {
    MYLOTS_FETCH,
    MYLOTS_FETCH_TOTAL,
    MYLOTS_FETCH_COUNT,
    MYLOTS_RECEIVED,
    MYLOTS_PARAMS_ADD,
    MYLOTS_PARAMS_REMOVE,
    MYLOTS_PAGINATE,
    MYLOTS_ERROR,
    MYLOTS_INVALIDATE_ERROR
} from '../actions';

import { toIndexObject } from '../../utils/redux.helpers';

const initialState = {
    lotsFetching: false,
    lotsFetchingTotal: 0,
    lotsFetchingCount: 0,
    lotsParams: {
        token: false,
        value: false,
        price: false,
        closed: false
    },
    records: {},
    pagination: {
        page: 0,
        limit: 10,
        start: 0
    },
    error: null
};

export const reduce = (state = initialState, action = {}) => {

    switch (action.type) {

        case MYLOTS_PAGINATE:
            return {
                ...state,
                pagination: {
                    ...state.pagination,
                    ...action.pagination
                }
            };

        case MYLOTS_FETCH:
            return {
                ...state,
                lotsFetching: true,
                lotsFetchingTotal: 0,
                lotsFetchingCount: 0
            };

        case MYLOTS_FETCH_TOTAL:
            return {
                ...state,
                lotsFetchingTotal: action.total
            };

        case MYLOTS_FETCH_COUNT:
            return {
                ...state,
                lotsFetchingCount: state.lotsFetchingCount + action.count
            };

        case MYLOTS_RECEIVED:
            return {
                ...state,
                lotsFetching: false,
                lotsFetchingTotal: 0,
                lotsFetchingCount: 0,
                records: toIndexObject(action.records, 'id')
            };

        case MYLOTS_PARAMS_ADD:
            return {
                ...state,
                lotsParams: {
                    ...state.lotsParams,
                    ...action.params
                },
                pagination: {
                    ...state.pagination,
                    ...{
                        page: 0,
                        start: 0
                    }
                }
            };

        case MYLOTS_PARAMS_REMOVE:
            return {
                ...state,
                lotsParams: {
                    ...state.lotsParams,
                    ...Object.fromEntries(action.params.map(p => [p, false]))
                },
                pagination: {
                    ...state.pagination,
                    ...{
                        page: 0,
                        start: 0
                    }
                }
            };

        case MYLOTS_ERROR:
            return {
                ...state,
                lotsFetching: false,
                lotsFetchingTotal: 0,
                lotsFetchingCount: 0,
                error: action.error
            };

        case MYLOTS_INVALIDATE_ERROR:
            return { 
                ...state, 
                error: null 
            };
        
        default:
            return state;
    }
};

