import {
    UAX_BALANCE_FETCH,
    UAX_BALANCE_RECEIVED,
    UAX_DETAILS_FETCH,
    UAX_DETAILS_RECEIVED,
    UAX_ERROR
} from '../actions';

const initialState = {
    balance: 0,
    details: false,
    balanceFetching: false,
    detailsFetching: false,
    error: false    
};

export const reduce = (state = initialState, action = {}) => {

    switch (action.type) {

        case UAX_BALANCE_FETCH:
            return {
                ...state,
                balanceFetching: true
            };

        case UAX_DETAILS_FETCH:
            return {
                ...state,
                detailsFetching: true
            };

        case UAX_BALANCE_RECEIVED:
            return {
                ...state,
                balance: action.balance,
                details: action.details,
                balanceFetching: false,
                detailsFetching: false,
                error: false
            };

        case UAX_DETAILS_RECEIVED:
            return {
                ...state,
                details: action.details,
                detailsFetching: false,
                error: false
            };

        case UAX_ERROR:
            return {
                ...state,
                balanceFetching: false,
                error: action.error
            };

        default:
            return state;
    }
};
