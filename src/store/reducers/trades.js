import {
    TRADES_LOTS_FETCH,
    TRADES_LOTS_FETCH_TOTAL,
    TRADES_LOTS_FETCH_COUNT,
    TRADES_LOTS_RECEIVED,
    TRADES_LOTS_RESTORED,
    TRADES_LOTS_REMOVE,
    TRADES_LOTS_PARAMS_ADD,
    TRADES_LOTS_PARAMS_REMOVE,
    TRADES_LOTS_PAGINATE,
    TRADES_BLOCK_NUMBER,
    TRADES_ERROR,
    TRADES_INVALIDATE_ERROR
} from '../actions';
// import recordsMocks from '../mocks/lots';

import { toIndexObject, removeIndexes } from '../../utils/redux.helpers';

const initialState = {
    lotsFetching: false,
    lotsFetchingTotal: 0,
    lotsFetchingCount: 0,
    blockNumber: 0,
    lotsParams: {
        token: false,
        value: false,
        price: false,
        one: false
    },
    records: {},//recordsMocks
    pagination: {
        page: 0,
        limit: 10,
        start: 0
    },
    error: null
};

export const reduce = (state = initialState, action = {}) => {

    switch (action.type) {

        case TRADES_LOTS_PAGINATE:
            return {
                ...state,
                pagination: {
                    ...state.pagination,
                    ...action.pagination
                }
            };

        case TRADES_LOTS_FETCH:
            return {
                ...state,
                lotsFetching: true,
                lotsFetchingTotal: 0,
                lotsFetchingCount: 0
            };

        case TRADES_LOTS_FETCH_TOTAL:
            return {
                ...state,
                lotsFetchingTotal: action.total
            };

        case TRADES_LOTS_FETCH_COUNT:
            return {
                ...state,
                lotsFetchingCount: state.lotsFetchingCount + action.count
            };

        case TRADES_LOTS_RECEIVED:
            return {
                ...state,
                lotsFetching: false,
                lotsFetchingTotal: 0,
                lotsFetchingCount: 0,
                records: {
                    ...toIndexObject(action.records, 'id'),
                    ...state.records
                }
            };

        case TRADES_LOTS_RESTORED:
            return {
                ...state,
                lotsFetching: false,
                lotsFetchingTotal: 0,
                lotsFetchingCount: 0
            };

        case TRADES_LOTS_REMOVE:
            return {
                ...state,
                records: removeIndexes(state.records, action.ids)
            };

        case TRADES_LOTS_PARAMS_ADD:
            return {
                ...state,
                lotsParams: {
                    ...state.lotsParams,
                    ...action.params
                },
                pagination: {
                    ...state.pagination,
                    ...{
                        page: 0,
                        start: 0
                    }
                }
            };

        case TRADES_LOTS_PARAMS_REMOVE:
            return {
                ...state,
                lotsParams: {
                    ...state.lotsParams,
                    ...Object.fromEntries(action.params.map(p => [p, false]))
                },
                pagination: {
                    ...state.pagination,
                    ...{
                        page: 0,
                        start: 0
                    }
                }
            };

        case TRADES_BLOCK_NUMBER:
            return {
                ...state,
                blockNumber: action.blockNumber
            };

        case TRADES_ERROR:
            return {
                ...state,
                lotsFetching: false,
                lotsFetchingTotal: 0,
                lotsFetchingCount: 0,
                error: action.error
            };

        case TRADES_INVALIDATE_ERROR:
            return { 
                ...state, 
                error: null 
            };
        
        default:
            return state;
    }
};

