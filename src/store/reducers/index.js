import { reduce as app } from './app';
import { reduce as web3 } from './web3';
import { reduce as uax } from './uax';
import { reduce as trades } from './trades';
import { reduce as mylots } from './mylots';

export default {
    app,
    web3,
    uax,
    trades,
    mylots
};
