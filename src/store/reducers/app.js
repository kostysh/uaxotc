import {
    APP_NOTIFICATIONS_CLEAR,
    APP_NOTIFICATIONS_TOGGLE,
    APP_NOTIFICATIONS_ADD
} from '../actions';

const initialState = {
    notifications: {
        opened: false,
        seen: false,
        records: []
    }
};

export const reduce = (state = initialState, action = {}) => {

    switch (action.type) {

        case APP_NOTIFICATIONS_CLEAR:
            return {
                ...state,
                notifications: {
                    ...state.notifications,
                    records: []
                }
            };

        case APP_NOTIFICATIONS_TOGGLE:
            return {
                ...state,
                notifications: {
                    ...state.notifications,
                    opened: !state.notifications.opened,
                    seen: !!state.notifications.opened
                }
            };

        case APP_NOTIFICATIONS_ADD:
            return {
                ...state,
                notifications: {
                    ...state.notifications,
                    seen: false,
                    records: [
                        ...[action.record],
                        ...state.notifications.records
                    ]
                }
            };
        
        default:
            return state;
    }
};

