import en from './services.en.json';
import ru from './services.ru.json';
import uk from './services.uk.json';

export default {
    en,
    ru,
    uk
};