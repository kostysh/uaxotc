import Web3 from 'web3';
import config from '../../config';
import { store } from '../index';
import { web3Initialized, web3AccountChanged } from '../actions';
import i18n from '../../i18n';

// System imports
import { isFirefoxMobile } from '../../utils/environment';

let initialized = false;
let selectedAccount;

/**
 * List of actions to run after each account or network change
 */
const onAccountChangeActions = () => store.dispatch(web3AccountChanged());

/** 
 * Get Ethereum netowork name by Id
 * @param {Number} id Netowork Id
 * @param {Boolean} forUrl Is domain should be returned
 *  @param {Boolean} lowercase Is network name should be in lowercase
 * @returns {String} Network name
 */
export const getNetworkNameById = (id, forUrl = false, lowercase = false) => {
    let name;

    switch (id) {
        case 1:
            name = forUrl ? 'Mainnet' : 'www';
            break;
        case 2:
            name = 'Morden';
            break;
        case 3:
            name = 'Ropsten';
            break;
        case 4:
            name = 'Rinkeby';
            break;
        case 5: 
            name = 'Goerli';
            break;
        case 42:
            name = 'Kovan';
            break;
        default:
            name = forUrl ? 'Unknown' : 'www';
    }

    return lowercase ? name.toLowerCase() : name;
};

export const updateSelectedAccount = async () => {
    const accounts = await window.web3.eth.getAccounts();

    if (accounts.length > 0) {
        
        selectedAccount = accounts[0];
        return selectedAccount;
    }

    throw new Error(i18n.t('services.errors.noaccounts'));
};

/**
 * Setup web3
 */
export const setupWeb3 = async () => {
    const metamaskErrorMessage = i18n.t('services.errors.metamask');

    if (window.ethereum && window.ethereum.isMetaMask) {
        
        // Web3 instance with MetaMask provider
        window.web3 = new Web3(window.web3.currentProvider, null, {
            transactionConfirmationBlocks: config.confirmations || 10
        });

        // Web3 instance with Websocket provider (for event listening purposes)
        window.web3ws = new Web3(new Web3.providers.WebsocketProvider(`wss://${getNetworkNameById(config.network)}.infura.io/ws/v3/${config.projectId}`));
        
        // Choosing provider for listenin system changes
        const provider = window.web3.currentProvider;
        
        // Update events flag
        let subscribedToEvents = false;
        
        // Handle case if password not entered and pop-up not shown
        await new Promise((resolve, reject) => {

            if (!isFirefoxMobile) {

                const timeout = setTimeout(() => {
                    // Handle case if password entered after error message has been shown
                    provider.on('accountsChanged', () => setTimeout(() => window.location.reload(true), 2000));
                    reject(new Error(metamaskErrorMessage));
                }, 3000);
                window.ethereum.enable()
                    .then(() => {
                        clearTimeout(timeout);
                        resolve();
                    })
                    .catch(reject);
            } else {

                updateSelectedAccount()
                    .then(resolve)
                    .catch(reject);
            }            
        });

        if (!initialized) {

            if (!subscribedToEvents) {
                provider.on('accountsChanged', onAccountChangeActions);
                provider.on('networkChanged', onAccountChangeActions);
                subscribedToEvents = true;
            }

            const networkVersion = await window.web3.eth.net.getId();
    
            if (networkVersion !== config.network) {
                throw new Error(i18n.t('services.errors.wrongnet', {
                    network: getNetworkNameById(config.network)
                }));
            }
    
            initialized = true;
        } 
        
    } else {
        throw new Error(metamaskErrorMessage);
    }
};

/**
 * Check is web3 is initialized
 */
export const isWeb3Initialized = () => initialized;

/**
 * Return currently selected address
 */
export const getSelectedAddress = () => window.ethereum.selectedAddress || selectedAccount;

/**
 * Start web3 initialization and dispatch proper action after
 */
export const setupWeb3AndDispatch = async () => {
    await setupWeb3();
    store.dispatch(web3Initialized());
};
