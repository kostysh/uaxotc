import { isWeb3Initialized, setupWeb3AndDispatch, getSelectedAddress } from './web3';
import EventEmitter from 'eventemitter3';
import PromiEvent from '../../utils/promievent';
import config from '../../config';
import { store } from '../index';
import { uaxBalanceFetch } from '../actions';
import { abi as Otc } from '../../abi/Otc.json';
import Uax from '../../abiUax/ABI.json';
import { abi as Erc20 } from '../../abi/Token.json';

// All contracts instances registry
const contracts = {};
const contractsWs = {};

// Deatils of tokens fetched during session
const tokensDetails = {};

// Address of the UAX token contract
let uaxAddress;

// Tasks intervals
let getUaxBalanceInterval;

// Subscriptions
const subscriptions = {};

/**
 * Get or initialize contact object
 * @param {String} address Address of the contract
 * @param {Object} address ABI
 * @param {Boolean} withWsProvider Is Web3 instance with Websocket provider should be used
 * @returns {Object}
 */
export const getContract = (address, abi, wsProvider = false) => {
    const targetRegistry = wsProvider ? contractsWs : contracts;
    const targetInstance = wsProvider ? window.web3ws: window.web3;

    if (targetRegistry[address]) {
        return targetRegistry[address];
    }

    targetRegistry[address] = new targetInstance.eth.Contract(abi, address);
    return targetRegistry[address];
};

export const startGetUaxBalanceTask = async () => {
    clearInterval(getUaxBalanceInterval);
    const task = () => store.dispatch(uaxBalanceFetch(getSelectedAddress()));
    task();
    getUaxBalanceInterval = setInterval(task, config.interval.balance);
};

/**
 * Fetch current UAX token address
 * @returns {Promise<{String}>}
 */
export const getUaxAddress = async () => {

    if (uaxAddress) {
        return uaxAddress;
    }

    if (!isWeb3Initialized()) {

        await setupWeb3AndDispatch();
    }

    const otc = getContract(config.contract, Otc);
    uaxAddress = await otc.methods.uaxTokenAddress().call({
        from: getSelectedAddress()
    });

    return uaxAddress;
};

/**
 * Fetch ERC20 token balance of the address
 * @param {Object} contract Token contract instance
 * @param {String} userAddress Address of the user
 * @returns {Promise<{Object}>} BN object
 */
export const getErc20Balance = async (contract, userAddress) => {    
    const balance = await contract.methods.balanceOf(userAddress).call({
        from: getSelectedAddress()
    });
    return balance;
};

/**
 * Fetch UAX token balance of the address
 * @param {String} userAddress 
 * @returns {Promise<{Object}>} BN object
 */
export const getUaxBalance = async (userAddress) => {
    const uaxAddress = await getUaxAddress();
    const contract = getContract(uaxAddress, Uax);
    const balance = await getErc20Balance(contract, userAddress);
    return balance;
};

/**
 * Get token details
 * @param {String} address Token address
 * @param {Object} abi Token ABI
 * @returns {Promise<{Object}>}
 */
export const getTokenDetails = async (address, abi) => {

    if (tokensDetails[address]) {
        return tokensDetails[address];
    }

    const contract = getContract(address, abi);

    const [
        name,
        symbol,
        decimals,
        totalSupply
    ] = await Promise.all([
        contract.methods.name().call({ from: getSelectedAddress() }),
        contract.methods.symbol().call({ from: getSelectedAddress() }),
        contract.methods.decimals().call({ from: getSelectedAddress() }),
        contract.methods.totalSupply().call({ from: getSelectedAddress() })
    ]);

    const details = {
        address,
        name,
        symbol,
        decimals: parseInt(decimals),
        totalSupply: totalSupply.toString()
    };

    tokensDetails[address] = details;

    return details;
};

/**
 * Get UAX token deatils
 * @returns {Promise<{Object}>}
 */
export const getUaxDetails = async () => {
    const uaxAddress = await getUaxAddress();
    return await getTokenDetails(uaxAddress, Uax);
};

/**
 * Fetch ERC20 token balance of the address
 * @param {String} tokenAddress Token address
 * @returns {Promise<{Object}>} BN object
 */
export const getErc20OwnerBalance = async (tokenAddress) => {
    const contract = getContract(tokenAddress, Erc20);
    const userAddress = getSelectedAddress();
    const balance = await contract.methods.balanceOf(userAddress).call({
        from: userAddress
    });
    return balance.toString();
};

/**
 * Get ERC20 token deatils
 * @param {String} tokenAddress Token address
 * @returns {Promise<{Object}>}
 */
export const getERC20TokenDetails = async (tokenAddress) => {
    return await getTokenDetails(tokenAddress, Erc20);
};

/**
 * Calculate lot fee
 * @param {Number} lotPrice Lot price in UAX
 * @returns {Promise<{Number}>} 
 */
export const calcFee = async (lotPrice) => {
    const otc = getContract(config.contract, Otc);
    const fee = await otc.methods.calcFee(lotPrice).call({
        from: getSelectedAddress()
    });
    return fee;
};

export const tokenAllowance = async (tokenAddress, ownerAddress) => {
    const contract = getContract(tokenAddress, Erc20);
    const allowance = contract.methods.allowance(ownerAddress, config.contract).call({
        from: getSelectedAddress()
    });
    return allowance;
};

export const estimateTokensMethod = async (method, tokenAddress, options) => {
    const contract = getContract(tokenAddress, Erc20);
    const gasPrice = await window.web3.eth.getGasPrice();
    const gas = await contract.methods[method].apply(contract, options).estimateGas({
        from: getSelectedAddress()
    });
    return gas * gasPrice;
};

export const estimateMarketMethod = async (method, options) => {
    const contract = getContract(config.contract, Otc);
    const gasPrice = await window.web3.eth.getGasPrice();
    const gas = await contract.methods[method].apply(contract, options).estimateGas({
        from: getSelectedAddress()
    });
    return gas * gasPrice;
};

// On done transaction helper
const onDoneTransaction = async (receipt, event, resolve) => {
    event.emit('receipt', receipt);
    const transaction = await window.web3.eth.getTransaction(receipt.transactionHash);
    event.emit('transaction', transaction);
    resolve(receipt);
};

const manageTransaction = (contract, method, options, gasPrice, eventEmitter, resolveCallback, rejectCallback) => {
    let isResolved;

    contract
        .methods[method].apply(contract, options)
        .send({
            from: getSelectedAddress(),
            gasPrice
        })
        .on('error', rejectCallback)
        .on('transactionHash', transactionHash => eventEmitter.emit('transactionHash', transactionHash))
        .on('confirmation', async (confirmationNumber, receipt) => {
                        
            if (!isResolved) {
                
                try {

                    if (Number(receipt.status) === 0) {
                        return rejectCallback(new Error('Transaction unsuccessful'));
                    }
    
                    eventEmitter.emit('confirmations', confirmationNumber);
    
                    if (confirmationNumber >= config.confirmations) {
    
                        isResolved = true;
                        await onDoneTransaction(receipt, eventEmitter, resolveCallback);
                    }
                } catch (err) {
                    rejectCallback(err);
                }
            }
        })
        .on('receipt', async (receipt) => {
                        
            if (!isResolved) {

                try {
                    isResolved = true;
                    await onDoneTransaction(receipt, eventEmitter, resolveCallback);
                } catch (err) {
                    rejectCallback(err);
                }
            }
        });   
};

const tokensApproval = (tokenAddress, tokenValue, method = null) => new PromiEvent(async (resolve, reject, event) => {

    if (!isWeb3Initialized()) {

        await setupWeb3AndDispatch();
    }

    const token = new window.web3.eth.Contract(Erc20, tokenAddress);
    const gasPrice = await window.web3.eth.getGasPrice();

    manageTransaction(
        token,
        method,
        [
            config.contract, 
            tokenValue.toString()
        ],
        gasPrice,
        event,
        resolve,
        reject
    );  
});

export const allowTokens = (tokenAddress, tokenValue) => tokensApproval(tokenAddress, tokenValue, 'approve');

export const disallowTokens = (tokenAddress, tokenValue) => tokensApproval(tokenAddress, tokenValue, 'decreaseAllowance');

export const createLot = (tokenAddress, tokenValue, lotPrice) => new PromiEvent(async (resolve, reject, event) => {

    if (!isWeb3Initialized()) {

        await setupWeb3AndDispatch();
    }

    const otc = getContract(config.contract, Otc);
    const gasPrice = await window.web3.eth.getGasPrice();

    manageTransaction(
        otc,
        'create',
        [
            tokenAddress, 
            tokenValue.toString(), 
            lotPrice.toString()
        ],
        gasPrice,
        event,
        resolve,
        reject
    );    
});

export const removeLot = lotId => new PromiEvent(async (resolve, reject, event) => {

    if (!isWeb3Initialized()) {

        await setupWeb3AndDispatch();
    }

    const otc = getContract(config.contract, Otc);
    const gasPrice = await window.web3.eth.getGasPrice();
    
    manageTransaction(
        otc,
        'remove',
        [
            lotId
        ],
        gasPrice,
        event,
        resolve,
        reject
    );
});

export const purchaseLot = lotId => new PromiEvent(async (resolve, reject, event) => {

    if (!isWeb3Initialized()) {

        await setupWeb3AndDispatch();
    }

    const otc = getContract(config.contract, Otc);
    const gasPrice = await window.web3.eth.getGasPrice();
    
    manageTransaction(
        otc,
        'purchase',
        [
            lotId
        ],
        gasPrice,
        event,
        resolve,
        reject
    );
});

export const getLotDetails = async (id, callback = () => {}) => {

    if (!isWeb3Initialized()) {

        await setupWeb3AndDispatch();
    }

    const otc = getContract(config.contract, Otc);
    const { token, value, price, fee, closed } = await otc.methods.details(id).call({
        from: getSelectedAddress()
    });

    const tokenDetails = await getERC20TokenDetails(token);

    const lotDetails = {
        id,
        token,
        tokenDetails,
        value: value.toString(),
        price: price.toString(),
        fee: fee.toString(),
        closed
    };

    callback(lotDetails);

    return lotDetails;
};

export const getLotsTask = async (targetMethod = 'activeLots()') => {
    // targetMethod can be one of 'activeLots()', 'ownedLots()'

    if (!isWeb3Initialized()) {
    
        await setupWeb3AndDispatch();
    }

    const eventEmitter = new EventEmitter();

    const start = async () => {
        
        try {
            
            const otc = getContract(config.contract, Otc);
            const activeLotsIds = await otc.methods[targetMethod]().call({
                from: getSelectedAddress()
            });
            eventEmitter.emit('ids', activeLotsIds);
            const blockNumber = await window.web3.eth.getBlockNumber();
            eventEmitter.emit('blockNumber', blockNumber);
            const lots = await Promise.all(
                activeLotsIds.reverse().map(lotId => getLotDetails(
                    lotId, 
                    lotDetails => eventEmitter.emit('lot', lotDetails)
                ))
            ); 
            eventEmitter.emit('lots', lots);
        } catch(err) {
            eventEmitter.emit('error', err);
        }
    };

    eventEmitter.start = start;

    return eventEmitter;
};

export const subscribeLotEventsTask = async (fromBlock = 0) => {

    if (!isWeb3Initialized()) {
    
        await setupWeb3AndDispatch();
    }

    const eventEmitter = new EventEmitter();

    const start = () => {

        try {

            if (subscriptions['LotCreated']) {
                subscriptions['LotCreated'].unsubscribe();
            }

            if (subscriptions['LotRemoved']) {
                subscriptions['LotRemoved'].unsubscribe();
            }

            if (subscriptions['LotPurchased']) {
                subscriptions['LotPurchased'].unsubscribe();
            }

            const otc = getContract(config.contract, Otc, true);// Web3 instance with Websocket provider will be used

            window.otc = otc;//debug

            if (fromBlock !== 0) {
                fromBlock = fromBlock+1;
            }

            subscriptions['LotCreated'] = otc.events.LotCreated({
                fromBlock
            })
                .on('data', async (event) => {

                    try {
                        const tokenDetails = await getERC20TokenDetails(event.returnValues.token);
                        const { id, price, token, value } = event.returnValues;
                        eventEmitter.emit('LotCreated', {
                            lot: {
                                id,
                                token,
                                value: value.toString(),
                                price: price.toString(),
                                tokenDetails
                            },
                            blockNumber: event.blockNumber
                        });
                    } catch(error) {
                        eventEmitter.emit('error', error);
                    }
                })
                .on('error', error => eventEmitter.emit('error', error));

            subscriptions['LotRemoved'] = otc.events.LotRemoved({
                fromBlock
            })
                .on('data', event => eventEmitter.emit('LotRemoved', {
                    id: event.returnValues.lotId,
                    blockNumber: event.blockNumber
                }))
                .on('error', error => eventEmitter.emit('error', error));

            subscriptions['LotPurchased'] = otc.events.LotPurchased({
                fromBlock
            })
                .on('data', event => eventEmitter.emit('LotPurchased', {
                    id: event.returnValues.lotId,
                    blockNumber: event.blockNumber
                }))
                .on('error', error => eventEmitter.emit('error', error));

        } catch (err) {
            eventEmitter.emit('error', err);
        }
    };

    eventEmitter.start = start;

    return eventEmitter;
};
