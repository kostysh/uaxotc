const keccak256 = window.web3.sha3 || window.web3.utils.keccak256;

let tokens = {};

const createToken = () => {
    const address = keccak256((Math.floor(Math.random() * 10) + 1).toString());

    if (tokens[address]) {
        return tokens[address];
    }

    const genSymb = num => {
        const symbols = 'ZAQWSXCDERFVBGTYHNMJUIKOLP';
        let result = '';

        while (num > 0) {
            result += symbols[Math.floor(Math.random() * 25) + 1];
            num -= 1;
        }

        return result;
    };

    tokens[address] = {
        address,
        symbol: genSymb(3),
        decimals: 18,
        totalSupply: parseInt(Math.floor(Math.random() * 100000) + 1000000)
    };

    return tokens[address];
};

export default Object.fromEntries(Array(5768).fill(0).map(_ => {
    const token = createToken();
    return [
        keccak256(Math.random().toString()),
        {
            token: token.address,
            tokenDetails: token,
            value: parseInt(Math.floor(Math.random() * 1000) + 1),
            price: parseInt(Math.floor(Math.random() * 5000) + 1)
        }
    ];
}));