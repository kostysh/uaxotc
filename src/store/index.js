import { createStore, applyMiddleware, combineReducers, compose } from 'redux';
import { connectRouter, routerMiddleware } from 'connected-react-router';
import { createBrowserHistory } from 'history';
import createSagaMiddleware from 'redux-saga';
import { persistStore, persistReducer } from 'redux-persist';
import autoMergeLevel2 from 'redux-persist/lib/stateReconciler/autoMergeLevel2';
import storage from 'redux-persist/lib/storage';
import { createPersistStorageFilter } from '../utils/persist.helpers';

import packageJson from '../../package.json';
import rootReducer from './reducers';
import * as actions from './actions';
import rootSaga from './sagas';

const appStateBacklist = createPersistStorageFilter(
    'app',
    [// properties from the reducer to be filtered 
       
    ]
);

const tradesStateBlacklist = createPersistStorageFilter(
    'trades',
    [
        'lotsFetching',
        'lotsFetchingTotal',
        'lotsFetchingCount',
        'error'
    ]
);

const persistConfig = {
    debug: process.env.NODE_ENV !== 'production',
    key: packageJson.name,
    storage,
    stateReconciler: autoMergeLevel2,
    transforms: [
        appStateBacklist,
        tradesStateBlacklist
    ],
    blacklist: ['router', 'web3', 'uax', 'mylots'] // exclude some states
};

export const history = createBrowserHistory({
    basename: '/'
});

export function configureStore(initialState, rehydrated = () => {}) {
    const sagaMiddleware = createSagaMiddleware();
    const composeEnhancers = process.env.NODE_ENV !== 'production' && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ ? 
        window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ : 
        compose;
    const combinedReducers = combineReducers({
        router: connectRouter(history),
        ...rootReducer
    });
    const persistedReducer = persistReducer(persistConfig, combinedReducers);
    
    const store = createStore(
        persistedReducer,
        initialState,
        composeEnhancers(applyMiddleware(sagaMiddleware, routerMiddleware(history)))
    );

    const persistor = persistStore(store, null, rehydrated);

    sagaMiddleware.run(rootSaga);

    // Initial setup actions
    store.dispatch(actions.web3Setup());

    return { store, persistor };
}

export const { store } = configureStore();
