# Tokens for testing OTC Dapp

## STR (Strange Coin)
Etherscan: [https://ropsten.etherscan.io/token/0xDC1323E899007bC051e73E5EaF24249385716cBB](https://ropsten.etherscan.io/token/0xDC1323E899007bC051e73E5EaF24249385716cBB)  
Contract: `0xDC1323E899007bC051e73E5EaF24249385716cBB`  
Owner: `0x2Ff7da33864Ca79FAbfa4DA12a485CA5ADF78b30`  

## DUD (Dude Coin)
Etherscan: [https://ropsten.etherscan.io/token/0x210BaC5693D3C09CBd43B440964C8A4D790c8186](https://ropsten.etherscan.io/token/0x210BaC5693D3C09CBd43B440964C8A4D790c8186)  
Contract: `0x210BaC5693D3C09CBd43B440964C8A4D790c8186`  
Owner: `0x2Ff7da33864Ca79FAbfa4DA12a485CA5ADF78b30`  

## XXL (Extra Large Coin)
Etherscan: [https://ropsten.etherscan.io/token/0xBAa4CbB7b9dd247200F267005614f976Cf975e60](https://ropsten.etherscan.io/token/0xBAa4CbB7b9dd247200F267005614f976Cf975e60)  
Contract: `0xBAa4CbB7b9dd247200F267005614f976Cf975e60`  
Owner: `0x2Ff7da33864Ca79FAbfa4DA12a485CA5ADF78b30`  

## GMBZ (Garmonbozia Coin)
Etherscan: [https://ropsten.etherscan.io/token/0x4F209f92F5cDE44FFF98C0b0CA486B14424e2834](https://ropsten.etherscan.io/token/0x4F209f92F5cDE44FFF98C0b0CA486B14424e2834)  
Contract: `0x4F209f92F5cDE44FFF98C0b0CA486B14424e2834`  
Owner: `0x2Ff7da33864Ca79FAbfa4DA12a485CA5ADF78b30`  

## COOP (Cooper Coin)
Etherscan: [https://ropsten.etherscan.io/token/0x60E95A39FeC102672af82F0Ea1ec56b24515BAE4](https://ropsten.etherscan.io/token/0x60E95A39FeC102672af82F0Ea1ec56b24515BAE4)  
Contract: `0x60E95A39FeC102672af82F0Ea1ec56b24515BAE4`  
Owner: `0x2Ff7da33864Ca79FAbfa4DA12a485CA5ADF78b30`