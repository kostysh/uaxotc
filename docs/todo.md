# TODO List

- Update token balance after the Lot has been successfully published
- Validate lot existance on each step of purchasing  
- (smart contract) Add information about the lot closing status (deleted or bought) to the lots storage

# Issues

- Error icon is displayed if the Notification centre containing error even if this error has been seen
- Language selector show selected "en" if Russian language been set by default (by browser rules)