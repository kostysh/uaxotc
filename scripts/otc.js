/**
 * PAN token governance utility. 
 * Use this script truffleframework environment
 * 
 * @author Kostiantyn Smyrnov <kostysh@gmail.com>
 * @date 2018
 */

const parseArgv = require('./helpers/argv');
const { doubleLine, print } = require('./helpers/print');
const Otc = artifacts.require('Otc');

const ERROR_PARAMETERS_NOT_FOUND = 'Command parameters not found';
const ERROR_ACTION_NOT_FOUND = 'Proper action is required as parameter';
const ERROR_UAX_ADDRESS_NOT_UPDATED = 'Error! UAX address has not been updated';

let config = {};
let otc;

try {
    config = require('../src/config');
} catch(err) {}

/**
 * Parse command parameters
 * @param {Object} params
 * @returns {String[]}
 */
const parseParams = params => {

    if (!params.params || params.params === '') {
        throw new Error(ERROR_PARAMETERS_NOT_FOUND);
    }

    return params.params.split(',').map(p => {
        const template = /^number:/g;
        return p.match(template) ? parseInt(p.replace(template, '')) : p;
    });
};

/**
 * Return total count of active lots
 */
const getActiveLotsCount = async () => {
    const count =  await otc.activeLotsCount.call();

    doubleLine();
    print(`Total active lots count: ${count}`);
    doubleLine();
};

/**
 * Get registered UAX contract address
 */
const processGetUaxAddress = async () => {
    const uaxAddress = await otc.uaxTokenAddress.call();

    doubleLine();
    print(`UAX address: ${uaxAddress}`);
};

/**
 * Update UAX contract address
 * @param {Object} params Command line parameters
 */
const processUpdateUaxAddress = async (params) => {
    const parsedParams = parseParams(params);
    const newUaxAddress = parsedParams[0];
    const initialUaxAddress = await otc.uaxTokenAddress.call();

    doubleLine();
    print(`Old UAX address: ${initialUaxAddress}`);
    print(`New UAX address: ${newUaxAddress}`);
    doubleLine();

    const result = await otc.updateUaxAddress.sendTransaction(newUaxAddress, {
        from: config.owner
    });
    const events = result.logs.filter(l => l.event === 'UaxUpdated'); 
    
    if (events.length === 0) {
        throw new Error(ERROR_UAX_ADDRESS_NOT_UPDATED);
    }

    print('UAX address updated successfully');
};

/**
 * Check is contract address in whitelisted
 * @param {Object} params 
 */
const processIsWhitelisted = async (params) => {
    const parsedParams = parseParams(params);
    const contractAddress = parsedParams[0];

    const isWhitelisted = await otc.isWhitelisted.call(contractAddress);

    doubleLine();

    if (isWhitelisted) {
        print(`Address ${contractAddress} is whitelisted`);
    } else {
        print(`Address ${contractAddress} is NOT whitelisted`);
    }    
};

/**
 * Whitelist a contract address
 * @param {Object} params 
 */
const processWhitelist = async (params) => {
    const parsedParams = parseParams(params);
    const newMarketAddress = parsedParams[0];

    await otc.addWhitelisted.sendTransaction(newMarketAddress, {
        from: config.owner
    });

    doubleLine();
    print(`Address of the new market contract (${newMarketAddress}) has been whitelisted successfully`);
};

/**
 * Import lots from the old market contract
 * @param {Object} params 
 */
const processImportLots = async (params) => {
    const parsedParams = parseParams(params);
    const oldMarketAddress = parsedParams[0];

    await otc.importLots.sendTransaction(oldMarketAddress, {
        from: config.owner
    });

    doubleLine();
    print(`All active lots from the old market (${oldMarketAddress}) has been imported successfully`);    
};

/**
 * truffleframework script
 * @param {Function} callback
 */
module.exports = async (callback) => {

    try {

        const params = parseArgv(process.argv, 6);
        
        // Initialize contracts
        otc = await Otc.at(config.contract);

        switch (params.action) {
            case 'activeLotsCount':
                await getActiveLotsCount();
                break;

            case 'getUax':
                await processGetUaxAddress();
                break;

            case 'updateUax':
                await processUpdateUaxAddress(params);
                break;

            case 'isWhitelisted':
                await processIsWhitelisted(params);
                break;

            case 'whitelist':
                await processWhitelist(params);
                break;

            case 'import':
                await processImportLots(params);
                break;    

            default:
                callback(new Error(ERROR_ACTION_NOT_FOUND));
        }

        doubleLine();
        callback();
    } catch(err) {
        callback(err);
    }    
};
