# OTC contract management scripts

> Note: Be sure you have latest compiled contracts before starting commands

```sh
npm run compile
```

## Get registered UAX token address

```sh
npx truffle exec ./scripts/otc.js --network infura_ropsten action=getUax
```

## Update UAX token address

```sh
npx truffle exec ./scripts/otc.js --network infura_ropsten action=updateUax params=0x7B759983f6c181847C9428BFEAca561c2285C127
```

## Get total count of active lots

```sh
npx truffle exec ./scripts/otc.js --network infura_ropsten action=activeLotsCount
```

## Check is contract address is whitelisted

```sh
npx truffle exec ./scripts/otc.js --network infura_ropsten action=isWhitelisted params=0x37ef0Dc076304d4764cC9D5b6C8E423a551886f0
```

## Whitelist new mmarket contract address

```sh
npx truffle exec ./scripts/otc.js --network infura_ropsten action=whitelist params=0x37ef0Dc076304d4764cC9D5b6C8E423a551886f0
```

## Import lots from the old market contract

```sh
npx truffle exec ./scripts/otc.js --network infura_ropsten action=import params=0x94C4e4B394e323974836Bf2c95Bb21284580F8Da
```