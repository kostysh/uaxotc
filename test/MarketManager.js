const assertRevert = require('./helpers/assertRevert');
const { calcFee, calcFeeLocal, createLot } = require('./helpers/market');

require('chai')
    .use(require('bn-chai')(web3.utils.BN))
    .should();

const Token = artifacts.require('Token');
const MarketManager = artifacts.require('MarketManager');
const MarketManagerTest = artifacts.require('MarketManagerTest');

contract('MarketManager', ([marketOwner, uaxOwner, tttOwner, buyer, seller, seller2]) => {
    const baseFee = 50;
    const minFee = 15;
    const feeStep = 5;
    const feeMilestones = [50000, 100000, 500000, 1000000, 2000000];
    let uax;
    let ttt;
    let market;

    beforeEach(async () => {
        uax = await Token.new('Crypto Hryvna', 'UAX', 2, '10000000000', {
            from: uaxOwner
        });

        ttt = await Token.new('Test token', 'TTT', 18, '100000000000000000000000000', {
            from: tttOwner
        });

        market = await MarketManager.new(uax.address, {
            from: marketOwner
        });

        await market.updateFee.sendTransaction(baseFee, minFee, feeStep, feeMilestones);
    });

    describe('Getters', () => {

        it('#uaxTokenAddress should return UAX token address', async () => {
            const initialUaxAddress = await market.uaxTokenAddress.call();
            (initialUaxAddress).should.equal(uax.address);
        });

        it('#calcFee should calculate fee', async () => {
            // [50000, 100000, 500000, 1000000, 2000000]

            let fee = await calcFee(market, 49000);
            (fee).should.eq.BN(calcFeeLocal(49000, baseFee, minFee, feeStep, feeMilestones));

            fee = await calcFee(market, 60000);
            (fee).should.eq.BN(calcFeeLocal(60000, baseFee, minFee, feeStep, feeMilestones));

            fee = await calcFee(market, 110000);
            (fee).should.eq.BN(calcFeeLocal(110000, baseFee, minFee, feeStep, feeMilestones));

            fee = await calcFee(market, 510000);
            (fee).should.eq.BN(calcFeeLocal(510000, baseFee, minFee, feeStep, feeMilestones));

            fee = await calcFee(market, 1010000);
            (fee).should.eq.BN(calcFeeLocal(1010000, baseFee, minFee, feeStep, feeMilestones));

            fee = await calcFee(market, 2010000);
            (fee).should.eq.BN(calcFeeLocal(2010000, baseFee, minFee, feeStep, feeMilestones));
        });

        describe('#activeLots', () => {

            describe('(): simple version', () => {

                it('should return 0 if no lots has been created before', async () => {
                    const lots = await market.activeLots.call();
                    (lots.length).should.equal(0);
                });
    
                it('should return proper array of ids', async () => {
                    await ttt.transfer.sendTransaction(seller, 150, {
                        from: tttOwner
                    });
                    await createLot(market, ttt, 50, 500, seller);
                    await createLot(market, ttt, 50, 500, seller);
                    await createLot(market, ttt, 50, 500, seller);
                    const lots = await market.activeLots.call();
                    (lots.length).should.equal(3);
                });
            });

            describe('(address): with token filter', () => {
                let ttt2;
                let ttt3;

                beforeEach(async () => {
                    ttt2 = await Token.new('Test token 2', 'TTT2', 18, '100000000000000000000000000', {
                        from: tttOwner
                    });
                    ttt3 = await Token.new('Test token 3', 'TTT3', 18, '100000000000000000000000000', {
                        from: tttOwner
                    });                    
                });

                it('should return filtered lots ids', async () => {
                    const tokensTotal = 500;
                    const step = 50;
                    let tokensRemain = tokensTotal;

                    await ttt.transfer.sendTransaction(seller, tokensTotal, {
                        from: tttOwner
                    });
                    await ttt2.transfer.sendTransaction(seller, tokensTotal, {
                        from: tttOwner
                    });
                    await ttt3.transfer.sendTransaction(seller, tokensTotal, {
                        from: tttOwner
                    });

                    while (tokensRemain > 0) {
                        await createLot(market, ttt, step, 500, seller);
                        await createLot(market, ttt2, step, 500, seller);
                        await createLot(market, ttt3, step, 500, seller);
                        tokensRemain -= step;
                    }

                    const result1 = await market.methods['activeLots(address)'].call(ttt.address);
                    (result1.length).should.equal(parseInt(tokensTotal/step));
                    const result2 = await market.methods['activeLots(address)'].call(ttt2.address);
                    (result2.length).should.equal(parseInt(tokensTotal/step));
                    const result3 = await market.methods['activeLots(address)'].call(ttt3.address);
                    (result3.length).should.equal(parseInt(tokensTotal/step));
                });
            });

            describe('(uint256,uint256): paginated version', () => {

                it('should return lots ids', async () => {
                    const tokensTotal = 500;
                    let tokensRemain = tokensTotal;
                    const step = 50;
                    const limit = 5;
                    await ttt.transfer.sendTransaction(seller, tokensTotal, {
                        from: tttOwner
                    });
    
                    while (tokensRemain > 0) {
                        await createLot(market, ttt, step, 500, seller);
                        tokensRemain -= step;
                    }
    
                    const result1 = await market.methods['activeLots(uint256,uint256)'].call(0, limit);
                    (result1.total).should.eq.BN(parseInt(tokensTotal/step));
                    (result1.lots.length).should.equal(limit);
                });
    
                it('should fail zero limit has been passed', async () => {
                    await assertRevert(market.methods['activeLots(uint256,uint256)'].call(0, 0), 'WRONG_LIMIT');
                });
            });            
        });

        describe('#ownedLots', () => {

            it('should return 0 if no records has been created before (at all)', async () => {
                const lots = await market.ownedLots.call();
                (lots.length).should.equal(0);
            });

            it('should return 0 if no records has been created before by the specific owner', async () => {
                await ttt.transfer.sendTransaction(seller, 150, {
                    from: tttOwner
                });
                await createLot(market, ttt, 50, 500, seller);
                const lots = await market.ownedLots.call({
                    from: seller2
                });
                (lots.length).should.equal(0);
            });

            it('should return proper array of ids', async () => {
                await ttt.transfer.sendTransaction(seller, 150, {
                    from: tttOwner
                });
                await createLot(market, ttt, 50, 500, seller);
                await createLot(market, ttt, 50, 500, seller);
                await createLot(market, ttt, 50, 500, seller);
                const lots = await market.ownedLots.call({
                    from: seller
                });
                (lots.length).should.equal(3);
            });
        });

        describe('#details', () => {

            it('should fail if passed unknown lot Id', async () => {
                await assertRevert(market.details.call(web3.utils.asciiToHex('unknown')), 'UNKNOWN_LOT');
            });

            it('should return details of existed lot', async () => {
                await ttt.transfer.sendTransaction(seller, 100, {
                    from: tttOwner
                });
                const event = await createLot(market, ttt, 100, 400, seller);
                const lot = await market.details.call(event.args.id);
                (lot.token).should.equal(event.args.token);
                (lot.value).should.eq.BN(100);
                (lot.price).should.eq.BN(400);
                (lot.fee).should.eq.BN(calcFeeLocal(lot.price, baseFee, minFee, feeStep, feeMilestones));
                (lot.closed).should.equal(false);
            });
        });
    });

    describe('Menagement functions', () => {

        describe('#updateUaxAddress', () => {
            let newUax;

            beforeEach(async () => {
                newUax = await Token.new('Crypto Hryvna', 'UAX', 2, new web3.utils.BN('10000000000'), {
                    from: uaxOwner
                });
            });

            it('should fail if called by not an owner', async () => {
                await assertRevert(market.updateUaxAddress.sendTransaction(newUax.address, {
                    from: buyer
                }));
            });

            it('should update UAX address', async () => {
                const initialUaxAddress = await market.uaxTokenAddress.call();
                const result = await market.updateUaxAddress.sendTransaction(newUax.address, {
                    from: marketOwner
                });
                const events = result.logs.filter(l => l.event === 'UaxUpdated');
                (events.length).should.equal(1);
                (events[0].args.owner).should.equal(marketOwner);
                (events[0].args.oldAddress).should.equal(initialUaxAddress);
                (events[0].args.newAddress).should.equal(newUax.address);
            });
        });
    });

    describe('Creation of lots', () => {

        describe('#create', () => {

            it('should fail if provided zero tokenAddress', async () => {
                const testMarket = await MarketManagerTest.new(uax.address, {
                    from: marketOwner
                });
                await assertRevert(testMarket.testCreateWithWrongAddress.sendTransaction({
                    from: seller
                }), 'WRONG_TOKEN_ADDRESS');
            });

            it('should fail if passed wrong tokenValue', async () => {
                await assertRevert(market.create.sendTransaction(ttt.address, 0, 400, {
                    from: seller
                }), 'WRONG_TOKEN_VALUE');
            });

            it('should fail if passed wrong lotPrice', async () => {
                await assertRevert(market.create.sendTransaction(ttt.address, 100, 0, {
                    from: seller
                }), 'LOT_PRICE_TO0_LOW');
                await assertRevert(market.create.sendTransaction(ttt.address, 100, 100, {
                    from: seller
                }), 'LOT_PRICE_TO0_LOW');
            });

            it('should fail if passed amount of tokens more then balance', async () => {
                await ttt.transfer.sendTransaction(seller, 100, {
                    from: tttOwner
                });
                await assertRevert(market.create.sendTransaction(ttt.address, 101, 400, {
                    from: seller
                }), 'INSUFFICIENT_TOKENS');
            });

            it('should fail if tokens not been allowed or allowed less then required', async () => {
                await ttt.transfer.sendTransaction(seller, 100, {
                    from: tttOwner
                });
                await assertRevert(market.create.sendTransaction(ttt.address, 100, 400, {
                    from: seller
                }), 'TOKENS_NOT_ALLOWED');
                await ttt.approve.sendTransaction(market.address, 90, {
                    from: seller
                });
                await assertRevert(market.create.sendTransaction(ttt.address, 100, 400, {
                    from: seller
                }), 'TOKENS_NOT_ALLOWED');
            });

            it('should create new lot', async () => {
                await ttt.transfer.sendTransaction(seller, 100, {
                    from: tttOwner
                });
                const tokenValue = 100;
                const lotPrice = 400;
                const event = await createLot(market, ttt, tokenValue, lotPrice, seller);
                (event.args.id).should.be.a('string');
                (event.args.token).should.equal(ttt.address);
                (event.args.value).should.eq.BN(tokenValue);
                (event.args.price).should.eq.BN(lotPrice);
                const tttBalance = await ttt.balanceOf(market.address);
                (tttBalance).should.eq.BN(tokenValue);
            });
        });
    });

    describe('Removing of lots', () => {

        it('should fail if passed unknown lot id', async () => {
            await assertRevert(market.remove.sendTransaction(web3.utils.asciiToHex('unknown')), 'UNKNOWN_LOT');
        });

        it('should fail if called by not an owner', async () => {
            await ttt.transfer.sendTransaction(seller, 100, {
                from: tttOwner
            });
            const event = await createLot(market, ttt, 100, 400, seller);
            await assertRevert(market.remove.sendTransaction(event.args.id, {
                from: marketOwner
            }), 'NOT_AN_OWNER');
        });

        it('should close the lot', async () => {
            await ttt.transfer.sendTransaction(seller, 100, {
                from: tttOwner
            });
            const initialSellerTokenBalance = await ttt.balanceOf(seller);
            const createEvent = await createLot(market, ttt, 100, 400, seller);
            const result = await market.remove.sendTransaction(createEvent.args.id, {
                from: seller
            });
            const removeEvent = result.logs.filter(l => l.event === 'LotRemoved');
            (removeEvent.length).should.equal(1);
            (removeEvent[0].args.removedBy).should.equal(seller);
            (removeEvent[0].args.lotId).should.equal(createEvent.args.id);
            await assertRevert(market.remove.sendTransaction(createEvent.args.id, {
                from: seller
            }), 'LOT_CLOSED');
            const sellerTokenBalance = await ttt.balanceOf(seller);
            (sellerTokenBalance).should.eq.BN(initialSellerTokenBalance);
        });
    });

    describe('Purchasing of lots', () => {

        it('should fail if passed unknown lot id', async () => {
            await assertRevert(market.purchase.sendTransaction(web3.utils.asciiToHex('unknown')), 'UNKNOWN_LOT');
        });

        it('should fail if lot already closed', async () => {
            await ttt.transfer.sendTransaction(seller, 100, {
                from: tttOwner
            });
            const createEvent = await createLot(market, ttt, 100, 400, seller);
            await market.remove.sendTransaction(createEvent.args.id, {
                from: seller
            });
            await assertRevert(market.purchase.sendTransaction(createEvent.args.id), 'LOT_CLOSED');
        });

        it('should fail if buyer has insufficient UAX', async () => {
            await ttt.transfer.sendTransaction(seller, 100, {
                from: tttOwner
            });
            const createEvent = await createLot(market, ttt, 100, 400, seller);
            await assertRevert(market.purchase.sendTransaction(createEvent.args.id, {
                from: buyer
            }), 'INSUFFICIENT_UAX');
        });

        it('should fail if buyer not allowed to lock UAX', async () => {
            await uax.transfer.sendTransaction(buyer, 400, {
                from: uaxOwner
            });
            await ttt.transfer.sendTransaction(seller, 100, {
                from: tttOwner
            });
            const createEvent = await createLot(market, ttt, 100, 400, seller);
            await assertRevert(market.purchase.sendTransaction(createEvent.args.id, {
                from: buyer
            }), 'UAX_NOT_ALLOWED');
        });
        
        it('should purchase the lot', async () => {
            const initialUAXBuyerBalance = 1500;
            const initialTTTSellerBalance = 200;
            const tokenValue = 100;
            const lotPrice = 1000;
            await uax.transfer.sendTransaction(buyer, initialUAXBuyerBalance, {
                from: uaxOwner
            });
            await ttt.transfer.sendTransaction(seller, initialTTTSellerBalance, {
                from: tttOwner
            });
            await uax.approve.sendTransaction(market.address, lotPrice, {
                from: buyer
            });
            const createEvent = await createLot(market, ttt, tokenValue, lotPrice, seller);
            const result = await market.purchase.sendTransaction(createEvent.args.id, {
                from: buyer
            });
            const events = result.logs.filter(l => l.event === 'LotPurchased');
            (events.length).should.equal(1);
            (events[0].args.buyer).should.equal(buyer);
            (events[0].args.lotId).should.equal(createEvent.args.id);

            // Check UAX balances
            const marketUAXBalance = await uax.balanceOf(market.address);
            const buyerUAXBalance = await uax.balanceOf(buyer);
            const sellerUAXBalance = await uax.balanceOf(seller);
            const commission = await calcFee(market, lotPrice);
            (marketUAXBalance).should.eq.BN(commission);
            (buyerUAXBalance).should.eq.BN(initialUAXBuyerBalance - lotPrice);
            (sellerUAXBalance).should.eq.BN(lotPrice - commission);

            // Check token balance
            const buyerTTTBalance = await ttt.balanceOf(buyer);
            const sellerTTTBalance = await ttt.balanceOf(seller);
            (buyerTTTBalance).should.eq.BN(tokenValue);
            (sellerTTTBalance).should.eq.BN(initialTTTSellerBalance - tokenValue);
        });
    });    
});
