const assertRevert = require('./helpers/assertRevert');

require('chai')
    .use(require('bn-chai')(web3.utils.BN))
    .should();

const Token = artifacts.require('Token');

contract('Token', ([owner1, owner2, owner3]) => {
    const decimals = 2;
    let uax;
    
    before('Setup', async () => {
        uax = await Token.new('Crypto Hryvna', 'UAX', decimals, new web3.utils.BN('10000000000'));
    });

    describe('Token parameters', () => {

        it('#name should return token name', async () => {
            const name = await uax.name.call({from: owner1});
            (name).should.equal('Crypto Hryvna');
        });

        it('#symbol should return token symbol', async () => {
            const symbol = await uax.symbol.call({from: owner1});
            (symbol).should.equal('UAX');
        });

        it('#decimals should return token decimals', async () => {
            const decimals = await uax.decimals.call({from: owner1});
            (decimals).should.eq.BN(decimals);
        });

        it('#totalSupply should return 100 bil', async () => {
            const totalSupply = await uax.totalSupply.call({from: owner1});
            (totalSupply.toString()).should.equal('10000000000');
        });
    });
});
