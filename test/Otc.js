const assertRevert = require('./helpers/assertRevert');
const { createLot } = require('./helpers/market');

require('chai')
    .use(require('bn-chai')(web3.utils.BN))
    .should();

const Token = artifacts.require('Token');
const Otc = artifacts.require('Otc');

contract('Otc', ([marketOwner, uaxOwner, tttOwner, buyer, seller]) => {
    const baseFee = 50;
    const minFee = 15;
    const feeStep = 5;
    const feeMilestones = [50000, 100000, 500000, 1000000, 2000000];
    let uax;
    let ttt;
    let market;

    beforeEach(async () => {
        uax = await Token.new('Crypto Hryvna', 'UAX', 2, new web3.utils.BN('10000000000'), {
            from: uaxOwner
        });

        ttt = await Token.new('Test token', 'TTT', 18, new web3.utils.BN('100000000000000000000000000'), {
            from: tttOwner
        });

        market = await Otc.new(uax.address, {
            from: marketOwner
        });

        await market.updateFee.sendTransaction(baseFee, minFee, feeStep, feeMilestones);
    });

    describe('Pausable', () => {
        let pMarket;

        beforeEach(async () => {
            pMarket = await Otc.new(uax.address, {
                from: marketOwner
            });
        });

        describe('#pause', () => {

            it('should fail if called by wrong owner', async () => {
                await assertRevert(pMarket.pause.sendTransaction({
                    from: buyer
                }));
            });

            it('should make contract paused', async () => {
                const result = await pMarket.pause.sendTransaction({
                    from: marketOwner
                });
                const events = result.logs.filter(l => l.event === 'Paused');
                (events[0].args.account).should.equal(marketOwner);
            });

            it('should prevent calling of #create', async () => {
                const tokenValue = 200;
                const lotPrice = 200;
                await pMarket.pause.sendTransaction({
                    from: marketOwner
                });
                await ttt.transfer.sendTransaction(seller, tokenValue, {
                    from: tttOwner
                });
                await ttt.approve.sendTransaction(pMarket.address, tokenValue, {
                    from: seller
                });
                await assertRevert(pMarket.create.sendTransaction(ttt.address, tokenValue, lotPrice, {
                    from: seller
                }));
            });

            it('should prevent calling of #purchase', async () => {
                const tokenValue = 200;
                const lotPrice = 200;
                await uax.transfer.sendTransaction(buyer, lotPrice, {
                    from: uaxOwner
                });
                await uax.approve.sendTransaction(pMarket.address, lotPrice, {
                    from: buyer
                });
                await ttt.transfer.sendTransaction(seller, tokenValue, {
                    from: tttOwner
                });
                await ttt.approve.sendTransaction(pMarket.address, tokenValue, {
                    from: seller
                });
                const result = await pMarket.create.sendTransaction(ttt.address, tokenValue, lotPrice, {
                    from: seller
                });
                const events = result.logs.filter(l => l.event === 'LotCreated');
                const lotId = events[0].args.id;
                await pMarket.pause.sendTransaction({
                    from: marketOwner
                });
                await assertRevert(pMarket.purchase.sendTransaction(lotId, {
                    from: buyer
                }));
            });
        });

        describe('#unpause', () => {

            it('should fail if called by wrong owner', async () => {
                await pMarket.pause.sendTransaction({
                    from: marketOwner
                });
                await assertRevert(pMarket.unpause.sendTransaction({
                    from: buyer
                }));
            });

            it('should make contract not paused', async () => {
                await pMarket.pause.sendTransaction({
                    from: marketOwner
                });
                const result = await pMarket.unpause.sendTransaction({
                    from: marketOwner
                });
                const events = result.logs.filter(l => l.event === 'Unpaused');
                (events[0].args.account).should.equal(marketOwner);
            });

            it('should release preventing of calling #create', async () => {
                const tokenValue = 200;
                const lotPrice = 200;
                await pMarket.pause.sendTransaction({
                    from: marketOwner
                });
                await ttt.transfer.sendTransaction(seller, tokenValue, {
                    from: tttOwner
                });
                await ttt.approve.sendTransaction(pMarket.address, tokenValue, {
                    from: seller
                });
                await assertRevert(pMarket.create.sendTransaction(ttt.address, tokenValue, lotPrice, {
                    from: seller
                }));
                await pMarket.unpause.sendTransaction({
                    from: marketOwner
                });
                const result = await pMarket.create.sendTransaction(ttt.address, tokenValue, lotPrice, {
                    from: seller
                });
                const events = result.logs.filter(l => l.event === 'LotCreated');
                (events[0].args.price).should.eq.BN(lotPrice);
            });

            it('should release preventing of calling #purchase', async () => {
                const tokenValue = 200;
                const lotPrice = 200;
                await uax.transfer.sendTransaction(buyer, lotPrice, {
                    from: uaxOwner
                });
                await uax.approve.sendTransaction(pMarket.address, lotPrice, {
                    from: buyer
                });
                await ttt.transfer.sendTransaction(seller, tokenValue, {
                    from: tttOwner
                });
                await ttt.approve.sendTransaction(pMarket.address, tokenValue, {
                    from: seller
                });
                const result = await pMarket.create.sendTransaction(ttt.address, tokenValue, lotPrice, {
                    from: seller
                });
                const events = result.logs.filter(l => l.event === 'LotCreated');
                const lotId = events[0].args.id;
                await pMarket.pause.sendTransaction({
                    from: marketOwner
                });
                await assertRevert(pMarket.purchase.sendTransaction(lotId, {
                    from: buyer
                }));
                await pMarket.unpause.sendTransaction({
                    from: marketOwner
                });
                const purchaseResult = await pMarket.purchase.sendTransaction(lotId, {
                    from: buyer
                });
                const purchaseEvents = purchaseResult.logs.filter(l => l.event === 'LotPurchased');
                (purchaseEvents[0].args.lotId).should.equal(lotId);
            });
        });
    });

    describe('Migration from an old to the new contract', () => {
        const tokenValue = 200;
        const lotPrice = 200;
        let marketOld;
        let marketNew;
        
        beforeEach(async () => {
            marketOld = await Otc.new(uax.address, {
                from: marketOwner
            });
            await ttt.transfer.sendTransaction(seller, tokenValue, {
                from: tttOwner
            });
            await ttt.approve.sendTransaction(marketOld.address, tokenValue, {
                from: seller
            });
            await marketOld.create.sendTransaction(ttt.address, tokenValue, lotPrice, {
                from: seller
            });
            marketNew = await Otc.new(uax.address, {
                from: marketOwner
            });
        });

        describe('#importLots', () => {

            it('should fail if called by not an owner', async () => {
                await marketOld.pause.sendTransaction({
                    from: marketOwner
                });
                await marketOld.addWhitelisted.sendTransaction(marketNew.address, {
                    from: marketOwner
                });
                await assertRevert(marketNew.importLots.sendTransaction(marketOld.address, {
                    from: buyer
                }));
            });

            it('should fail if source contract not paused', async () => {
                await marketOld.addWhitelisted.sendTransaction(marketNew.address, {
                    from: marketOwner
                });
                await assertRevert(marketNew.importLots.sendTransaction(marketOld.address, {
                    from: marketOwner
                }));
            });

            it('should fail if new contract not whitelisted', async () => {
                await marketOld.pause.sendTransaction({
                    from: marketOwner
                });
                await assertRevert(marketNew.importLots.sendTransaction(marketOld.address, {
                    from: marketOwner
                }));
            });

            it('should import active lots from the old contract', async () => {
                await marketOld.pause.sendTransaction({
                    from: marketOwner
                });
                await marketOld.addWhitelisted.sendTransaction(marketNew.address, {
                    from: marketOwner
                });
                await marketOld.activeLots.call();
                await marketNew.importLots.sendTransaction(marketOld.address, {
                    from: marketOwner
                });
                const importedActiveLots = await marketNew.activeLots.call();
                (importedActiveLots.length).should.equal(1);
                const oldLot = await marketOld.details.call(importedActiveLots[0]);
                (oldLot.closed).should.equal(true);
                const lot = await marketNew.details.call(importedActiveLots[0]);
                (lot.price).should.eq.BN(tokenValue);
                const balance = await ttt.balanceOf.call(marketNew.address);
                (balance).should.eq.BN(tokenValue);
            });
        });
    });

    describe('Withdrawal', () => {

        it('should fail if called from not an owner', async () => {
            await assertRevert(market.withdraw.sendTransaction(1000, {
                from: tttOwner
            }));
        });

        it('should fail if isufficient UAX balance', async () => {
            await assertRevert(market.withdraw.sendTransaction(1000, {
                from: marketOwner
            }), 'INSUFFICIENT_BALANCE');
        });

        it('should withdraw UAX balance to owner', async () => {
            const initialUAXBuyerBalance = 1500;
            const initialTTTSellerBalance = 200;
            const tokenValue = 100;
            const lotPrice = 1000;
            await uax.transfer.sendTransaction(buyer, initialUAXBuyerBalance, {
                from: uaxOwner
            });
            await ttt.transfer.sendTransaction(seller, initialTTTSellerBalance, {
                from: tttOwner
            });
            await uax.approve.sendTransaction(market.address, lotPrice, {
                from: buyer
            });
            const createEvent = await createLot(market, ttt, tokenValue, lotPrice, seller);
            await market.purchase.sendTransaction(createEvent.args.id, {
                from: buyer
            });            
            const marketUAXBalance = await uax.balanceOf.call(market.address);
            await market.withdraw.sendTransaction(marketUAXBalance, {
                from: marketOwner
            });
            const marketOwnerUAXBalance = await uax.balanceOf(marketOwner);
            (marketOwnerUAXBalance).should.eq.BN(marketUAXBalance);
        });
    });

    describe('#activeLotsCount', () => {

        it('Should return total count of active lots', async () => {
            const tokenValue = 50;
            const lotPrice = 500;
            await ttt.transfer.sendTransaction(seller, tokenValue * 3, {
                from: tttOwner
            });
            await createLot(market, ttt, tokenValue, lotPrice, seller);
            await createLot(market, ttt, tokenValue, lotPrice, seller);
            const createEvent = await createLot(market, ttt, tokenValue, lotPrice, seller);
            await uax.transfer.sendTransaction(buyer, lotPrice, {
                from: uaxOwner
            });
            await uax.approve.sendTransaction(market.address, lotPrice, {
                from: buyer
            });
            await market.purchase.sendTransaction(createEvent.args.id, {
                from: buyer
            });
            const activeCount = await market.activeLotsCount.call();
            (activeCount).should.eq.BN(2);
        });
    });
});

