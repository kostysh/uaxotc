module.exports.calcFee = async (contract, price) => {
    return await contract.calcFee.call(price);
};

module.exports.calcFeeLocal = (price, baseFee, minFee, feeStep, feeMilestones) => {
    let fee = baseFee;
    let startEdge = 1;
    let rightEdge;

    for (let i = 0; i < feeMilestones.length; i++) {
        rightEdge = feeMilestones[i];

        if (price > startEdge && price <= rightEdge) {
            break;
        }

        fee = fee - feeStep >= minFee ? fee - feeStep : minFee;
        startEdge = rightEdge;
    }

    return parseInt(price * fee / 10000);
};

module.exports.createLot = async (marketContract, tokenContract, tokenValue, lotPrice, sellerAddress) => {
    await tokenContract.approve.sendTransaction(marketContract.address, tokenValue, {
        from: sellerAddress
    });
    const result = await marketContract.create.sendTransaction(tokenContract.address, tokenValue, lotPrice, {
        from: sellerAddress
    });
    const events = result.logs.filter(l => l.event === 'LotCreated');

    if (events.length === 0) {
        return Promise.reject(new Error('LotCreated event not fired'));
    }

    return events[0];
};
