pragma solidity 0.5.8;

import "openzeppelin-solidity/contracts/ownership/Ownable.sol";
import "openzeppelin-solidity/contracts/token/ERC20/IERC20.sol";
import "openzeppelin-solidity/contracts/token/ERC20/SafeERC20.sol";
import "./managers/MarketManager.sol";


contract Otc is MarketManager {

    using SafeERC20 for IERC20;
    using Address for address;

    /**
     * @dev Otc constructor
     * @param uaxAddress UAX token address
     */
    constructor(address uaxAddress) 
        public 
        MarketManager(uaxAddress) {}

    /**
     * @dev Fallback function
     */
    function() external payable {

        if (msg.value > 0) {

            msg.sender.transfer(msg.value);
        }
    }

    /**
     * @dev Withdaw contract balance
     * @param value Amount to withdraw
     */
    function withdraw(uint256 value) external onlyOwner {
        IERC20 uaxToken = market.getUaxToken();
        require(uaxToken.balanceOf(address(this)) >= value, "INSUFFICIENT_BALANCE");
        uaxToken.safeTransfer(msg.sender, value);        
    }
}
