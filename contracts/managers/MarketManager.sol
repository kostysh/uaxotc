pragma solidity 0.5.8;

import "openzeppelin-solidity/contracts/ownership/Ownable.sol";
import "openzeppelin-solidity/contracts/lifecycle/Pausable.sol";
import "openzeppelin-solidity/contracts/access/roles/WhitelistedRole.sol";
import "../libraries/MarketLib.sol";
import "./IMarketManager.sol";


/**
 * @title MarketManager
 * @dev Core tokens market manager contract
 */
contract MarketManager is IMarketManager, Ownable, Pausable, WhitelistedRole {

    using MarketLib for MarketLib.MarketStorage;

    /**
     * @dev Internal market storage
     */
    MarketLib.MarketStorage internal market;

    /**
     * @dev This event will be emitted when UAX link is updated
     * @param owner Who made update
     * @param newAddress New UAX address
     * @param oldAddress Old UAX address
     */
    event UaxUpdated(
        address owner,
        address newAddress,
        address oldAddress
    );

    /**
     * @dev This event will be emitted when new Lot is created
     * @param id Unique lot id
     * @param token Token address
     * @param value Token value
     * @param price Lot price
     */
    event LotCreated(
        bytes32 id,
        address token, 
        uint256 value, 
        uint256 price
    );

    /**
     * @dev This event will be emitted when lot is removed
     * @param removedBy Buyer address
     * @param lotId Lot Id
     */
    event LotRemoved(
        address removedBy,
        bytes32 lotId
    );

    /**
     * @dev This event will be emitted when lot is purchased
     * @param buyer Buyer address
     * @param lotId Lot Id
     */
    event LotPurchased(
        address buyer,
        bytes32 lotId
    );

    /**
     * @dev MarketManager constructor
     * @param uaxTokenAddress Address of the UAX token contract
     */
    constructor(address uaxTokenAddress) public {
        market.setUaxToken(uaxTokenAddress);
    }

    /**
     * @dev Update the link to UAX token
     * @param uaxTokenAddress Address of the UAX token contract
     */
    function updateUaxAddress(address uaxTokenAddress) external onlyOwner {
        address oldOne = address(market.getUaxToken());
        market.setUaxToken(uaxTokenAddress);
        emit UaxUpdated(msg.sender, uaxTokenAddress, oldOne);
    }

    /**
     * @dev Update the service fee parameters
     * @param base Base service fee
     * @param min Minimum value of the service fee
     * @param step Reduction step
     * @param milestones Array of fee changing milestones
     */
    function updateFee(
        uint256 base, 
        uint256 min, 
        uint256 step, 
        uint256[] calldata milestones
    ) external {
        market.setFee(base, min, step, milestones);
    }

    /**
     * @dev Lot migration
     * @param lotId Lot Id
     */
    function export(bytes32 lotId) external whenPaused onlyWhitelisted returns (
        address owner,
        address token,
        uint256 value,
        uint256 price
    ) {
        MarketLib.Lot memory lot = market.getLot(lotId);

        owner = lot.owner;
        token = lot.token;
        value = lot.value;
        price = lot.price;

        // Remove the Lot
        // related to the lot tokens will be transfered to the sender (new contract, importer)
        market.remove(lotId, true); 
        emit LotRemoved(msg.sender, lotId);
    }

    /**
     * @dev Import active lots from old market contract
     * @param source Old market contract instance
     */
    function importLots(IMarketManager source) external onlyOwner {
        bytes32[] memory lots = source.activeLots();

        for (uint256 i = 0; i < lots.length; i++) {
            (
                address owner,
                address token,
                uint256 value,
                uint256 price
            ) = source.export(lots[i]);
            
            market.addLot(owner, lots[i], token, value, price);
            emit LotCreated(lots[i], token, value, price);
        }
    }

    /**
     * @dev Creation of the new lot
     * @param tokenAddress Address of the token to sell
     * @param tokenValue Value of the token to sell
     * @param lotPrice Price of the lot
     */
    function create(
        address tokenAddress,
        uint256 tokenValue,
        uint256 lotPrice
    ) external whenNotPaused {
        MarketLib.Lot memory lot = market.create(tokenAddress, tokenValue, lotPrice); 
        emit LotCreated(lot.id, lot.token, lot.value, lot.price);
    }

    /**
     * @dev Removing of a Lot
     * @param lotId Lot Id
     */
    function remove(bytes32 lotId) external {
        market.remove(lotId, false); 
        emit LotRemoved(msg.sender, lotId);
    }

    /**
     * @dev Purchase of a Lot
     * @param lotId Lot Id
     */
    function purchase(bytes32 lotId) external whenNotPaused {
        market.purchase(lotId);
        emit LotPurchased(msg.sender, lotId);
    }

    /**
     * @dev Return total count of active lots
     * @return uint256
     */
    function activeLotsCount() external view returns (uint256) {
        return market.activeLotsCount(address(0));
    }

    /**
     * @dev Get list of active lots
     * @return bytes32[] Array of active lots ids
     */
    function activeLots() external view returns (bytes32[] memory) {
        return market.activeLotsIds(address(0));
    }

    /**
     * @dev Get list of active lots
     * @param token Token address
     * @return bytes32[] Array of active lots ids
     */
    function activeLots(address token) external view returns (bytes32[] memory) {
        return market.activeLotsIds(token);
    }

    /**
     * @dev Get list of active lots with pagination params
     * @param start Lots start position
     * @param limit Count of lots to return
     * @return bytes32[] Array of active lots ids
     * @return uint256
     */
    function activeLots(uint256 start, uint256 limit) external view returns (
        bytes32[] memory lots, 
        uint256 total
    ) {
        (lots, total) = market.activeLotsIdsPaginated(start, limit);
    }

    /**
     * @dev Get list of lots for sender
     * @return bytes32[] Array of active lots ids
     */
    function ownedLots() external view returns (bytes32[] memory) {
        return market.ownedLotsIds(msg.sender);
    }

    /**
     * @dev Returns a lot details
     * @param lotId Lot Id
     * @return Lot details
     */
    function details(bytes32 lotId) external view returns (
        address token,
        uint256 value,
        uint256 price,
        uint256 fee,
        bool closed
    ) {
        MarketLib.Lot memory lot = market.getLot(lotId);         
        token = lot.token;
        value = lot.value;
        price = lot.price;
        fee = MarketLib.calcFee(lot.price, lot.fee);
        closed = lot.closed;
    }

    /**
     * @dev Calculate fee for price
     * @param price Price
     * @return uint256 Fee value
     */
    function calcFee(uint256 price) external view returns (uint256) {
        return MarketLib.calcFee(price, market.fee);
    }

    /**
     * @dev Returns a UAX token address
     * @return address
     */
    function uaxTokenAddress() external view returns (address) {
        return address(market.getUaxToken());
    }
}