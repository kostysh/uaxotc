pragma solidity 0.5.8;


contract IMarketManager {
    
    function updateUaxAddress(address uaxTokenAddress) external;

    function updateFee(
        uint256 base, 
        uint256 min, 
        uint256 step, 
        uint256[] calldata milestones
    ) external;

    function export(bytes32 lotId) external returns (
        address owner,
        address token,
        uint256 value,
        uint256 price
    );

    function create(
        address tokenAddress,
        uint256 tokenValue,
        uint256 lotPrice
    ) external;

    function remove(bytes32 lotId) external;

    function purchase(bytes32 lotId) external;

    function activeLotsCount() external view returns (uint256);

    function activeLots() external view returns (bytes32[] memory);
    function activeLots(address token) external view returns (bytes32[] memory);
    function activeLots(uint256 start, uint256 limit) external view returns (
        bytes32[] memory lots, 
        uint256 total
    );

    function ownedLots() external view returns (bytes32[] memory);

    function details(bytes32 lotId) external view returns (
        address token,
        uint256 value,
        uint256 price,
        uint256 fee,
        bool closed
    );

    function calcFee(uint256 price) external view returns (uint256);

    function uaxTokenAddress() external view returns (address);
}