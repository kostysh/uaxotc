pragma solidity 0.5.8;

import "openzeppelin-solidity/contracts/math/SafeMath.sol";
import "openzeppelin-solidity/contracts/token/ERC20/IERC20.sol";
import "openzeppelin-solidity/contracts/token/ERC20/SafeERC20.sol";


/**
 * @title MarketLib
 * @dev This library hold a tokens market management logic
 * @author Kostiantyn Smyrnov <kostysh@gmail.com>
 */
library MarketLib {

    using SafeMath for uint256;
    using SafeERC20 for IERC20;

    /// @dev Service fee structure
    struct Fee {
        uint256 base; 
        uint256 min; 
        uint256 step; 
        uint256[] milestones;
    }

    /// @dev Lot storage structure
    struct Lot {
        address owner;
        bytes32 id;
        address token;
        uint256 value;
        uint256 price;
        Fee fee;// Lot fee (can be different to current)
        bool closed;
    }

    /// @dev Market storage structure
    struct MarketStorage {
        Fee fee;// Current fee
        IERC20 uaxToken;
        mapping (bytes32 => Lot) lots;// lotId => Lot 
        bytes32[] ids;
        mapping (bytes32 => bool) hashes;// unique hashes list
        bytes32 lastHash;// last generated hash
    }

    /**
     * @dev Set UAX token
     * @param store Market storage
     * @param uaxTokenAddress Link to UAX token
     */
    function setUaxToken(
        MarketStorage storage store,
        address uaxTokenAddress
    ) internal {
        require(uaxTokenAddress != address(0), "INAVALID_UAX_TOKEN");
        store.uaxToken = IERC20(uaxTokenAddress);
    }

    /**
     * @dev Set service fee parameters
     * @param store Market storage
     * @param base Base service fee
     * @param min Minimum value of the service fee
     * @param step Reduction step
     * @param milestones Array of fee changing milestones
     */
    function setFee(
        MarketStorage storage store,
        uint256 base, 
        uint256 min, 
        uint256 step, 
        uint256[] memory milestones
    ) internal {
        uint256 prevEdge = milestones[0];

        for (uint256 i = 1; i < milestones.length; i++) {

            if (milestones[i] < prevEdge) {
                revert("INVALID_FEE_MILSTONES");
            }

            prevEdge = milestones[i];
        }

        store.fee = Fee(base, min, step, milestones);
    }

    /**
     * @dev Getting list of active lots Ids
     * @param store Market storage
     * @param token Token address
     * @return bytes32[] Array of active lots ids 
     */
    function activeLotsIds(MarketStorage storage store, address token) internal view returns (bytes32[] memory) {
        bytes32[] memory ids = new bytes32[](activeLotsCount(store, token));
        uint256 index = 0;

        for (uint256 i = 0; i < store.ids.length; i++) {

            if (isActive(store.lots[store.ids[i]])) {

                if (token == address(0) || 
                    (token != address(0) && store.lots[store.ids[i]].token == token)) {
                    
                    ids[index] = store.ids[i];
                    index += 1;
                }
            }
        }

        return ids;
    }

    /**
     * @dev Get list of active lots with pagination params
     * @param store Market storage
     * @param start Lots start position
     * @param limit Count of lots to return
     * @return bytes32[] Array of active lots ids
     * @return uint256 Total count of ids
     */
    function activeLotsIdsPaginated(
        MarketStorage storage store, 
        uint256 start, 
        uint256 limit
    ) internal view returns (
        bytes32[] memory ids, 
        uint256 total
    ) {
        require(limit > 0, "WRONG_LIMIT");

        total = activeLotsCount(store, address(0));
        ids = new bytes32[](limit < total ? limit : total);
        uint256 index = 0;
        uint256 indexTotal = 0;

        for (uint256 i = 0; i < store.ids.length; i++) {

            if (isActive(store.lots[store.ids[i]])) {

                if (indexTotal >= start && indexTotal < start.add(limit)) {
                    ids[index] = store.ids[i];
                    index += 1;
                }

                indexTotal += 1;
            }
        }
    }

    /**
     * @dev Getting list of lots Ids of the specific owner
     * @param store Market storage
     * @param owner Lot owner (creator)
     * @return bytes32[] Array of active lots ids 
     */
    function ownedLotsIds(MarketStorage storage store, address owner) internal view returns (bytes32[] memory) {
        bytes32[] memory ids = new bytes32[](ownedLotsCount(store, owner));
        uint256 index = 0;

        for (uint256 i = 0; i < store.ids.length; i++) {

            if (store.lots[store.ids[i]].owner == owner) {
                ids[index] = store.ids[i];
                index += 1;
            }
        }

        return ids;
    }

    /**
     * @dev Add new lot to the storage
     * @param store Market storage
     * @param owner Lot owner
     * @param lotId Unique lot id
     * @param tokenAddress Address of token contract
     * @param tokenValue Token value
     * @param lotPrice Price of the lot
     * @return Lot instance
     */
    function addLot(
        MarketStorage storage store,
        address owner,
        bytes32 lotId,
        address tokenAddress,
        uint256 tokenValue,
        uint256 lotPrice
    ) internal returns (Lot storage) {
        // Create new Lot id or use given
        lotId = getOrCreateUniqueHash(store, lotId);

        store.lots[lotId] = Lot({
            owner: owner,
            id: lotId,
            token: tokenAddress,
            value: tokenValue,
            price: lotPrice,
            fee: store.fee,
            closed: false
        });
        store.ids.push(lotId);

        return store.lots[lotId];
    }

    /**
     * @dev Creation of the new lot
     * @param store Market storage
     * @param tokenAddress Address of token contract
     * @param tokenValue Token value
     * @param lotPrice Price of the lot
     * @return Lot instance
     */
    function create(
        MarketStorage storage store,
        address tokenAddress,
        uint256 tokenValue,
        uint256 lotPrice
    ) internal returns (Lot storage) {
        require(tokenAddress != address(0), "WRONG_TOKEN_ADDRESS");
        require(tokenValue > 0, "WRONG_TOKEN_VALUE");

        if (store.fee.base > 0 && calcFee(lotPrice, store.fee) == 0) {
            revert("LOT_PRICE_TO0_LOW");
        }

        // Try to lock tockens from sender
        IERC20 tokenToSell = IERC20(tokenAddress);
        require(tokenToSell.balanceOf(msg.sender) >= tokenValue, "INSUFFICIENT_TOKENS");
        require(tokenToSell.allowance(msg.sender, address(this)) >= tokenValue, "TOKENS_NOT_ALLOWED");
        tokenToSell.safeTransferFrom(msg.sender, address(this), tokenValue);

        return addLot(store, msg.sender, bytes32(0), tokenAddress, tokenValue, lotPrice);
    }

    /**
     * @dev Remove a lot
     * @param store Market storage
     * @param lotId Lot Id
     * @param migration Is tokens should be refunded
     */
    function remove(
        MarketStorage storage store, 
        bytes32 lotId,
        bool migration
    ) internal {
        Lot storage lot = getLot(store, lotId);
        
        if (!migration) {
            require(lot.owner == msg.sender, "NOT_AN_OWNER");
        }

        require(!lot.closed, "LOT_CLOSED");

        lot.closed = true;

        IERC20 tokenToSell = IERC20(lot.token);
        tokenToSell.safeTransfer(msg.sender, lot.value);       
    }

    /**
     * @dev PurchaseReturns a lot
     * @param store Market storage
     * @param lotId Lot Id
     */
    function purchase(
        MarketStorage storage store,
        bytes32 lotId
    ) internal {
        Lot storage lot = getLot(store, lotId);
        require(!lot.closed, "LOT_CLOSED");
        require(address(store.uaxToken) != address(0), "UAX_TOKEN_NOT_SET");
        require(store.uaxToken.balanceOf(msg.sender) >= lot.price, "INSUFFICIENT_UAX");
        require(store.uaxToken.allowance(msg.sender, address(this)) >= lot.price, "UAX_NOT_ALLOWED");

        lot.closed = true;
        
        uint256 commission = calcFee(lot.price, lot.fee);
        
        if (commission > 0) {
            store.uaxToken.safeTransferFrom(msg.sender, address(this), commission);
        }

        store.uaxToken.safeTransferFrom(msg.sender, lot.owner, lot.price.sub(commission));

        IERC20 tokenToSell = IERC20(lot.token);
        tokenToSell.safeTransfer(msg.sender, lot.value);
    }

    /**
     * @dev Returns a lot by its Id
     * @param store Market storage
     * @param lotId Lot Id
     * @return Lot
     */
    function getLot(MarketStorage storage store, bytes32 lotId) internal view returns (Lot storage) {
        require(store.lots[lotId].id == lotId, "UNKNOWN_LOT");
        return store.lots[lotId];
    }

    /**
     * @dev Returns UAX token address
     * @param store Market storage
     * @return IERC20
     */
    function getUaxToken(MarketStorage storage store) internal view returns (IERC20) {
        return store.uaxToken;
    }

    /**
     * @dev Returns calculated interest
     * @param value Source value
     * @param fee Fee parameters struct
     * @return uint256
     */
    function calcFee(
        uint256 value,
        Fee memory fee
    ) internal pure returns (uint256) {
        
        if (fee.base == 0) {
            return 0;// Sometimes fee can be equal to zero
        }

        uint256 feeValue = fee.base;
        uint256 startEdge = 1;
        uint256 rightEdge;

        for (uint256 i=0; i < fee.milestones.length; i++) {
            rightEdge = fee.milestones[i];

            if (value > startEdge && value <= rightEdge) {
                break;
            }

            feeValue = feeValue - fee.step >= fee.min ? feeValue.sub(fee.step) : fee.min;
            startEdge = rightEdge;
        }

        return value.mul(feeValue).div(10000);
    }

    /**
     * @dev Returns count of active lots
     * @param store Market storage
     * @param token Token address
     * @return uint256
     */
    function activeLotsCount(MarketStorage storage store, address token) internal view returns (uint256) {
        uint256 length = 0;

        for (uint256 i = 0; i < store.ids.length; i++) {

            if (isActive(store.lots[store.ids[i]])) {

                if (token == address(0) || 
                    (token != address(0) && store.lots[store.ids[i]].token == token)) {
                    
                    length += 1;
                }               
            }
        }

        return length;
    }

    /**
     * @dev Return state of the Lot
     * @return bool
     */
    function isActive(Lot storage lot) private view returns (bool) {
        return lot.token != address(0) && lot.closed != true;
    }

    /**
     * @dev Returns count of lots of the specific owner
     * @param store Market storage
     * @param owner Lot owner
     * @return uint256
     */
    function ownedLotsCount(MarketStorage storage store, address owner) private view returns (uint256) {
        uint256 length = 0;

        for (uint256 i = 0; i < store.ids.length; i++) {

            if (store.lots[store.ids[i]].owner == owner) {
                length += 1;
            }
        }

        return length;
    }

    /**
     * @dev Returns unique hash that can be used as ID
     * @param store Unique hashes storage
     * @return bytes32 Unique hash
     */
    function getOrCreateUniqueHash(
        MarketStorage storage store,
        bytes32 uHash
    ) private returns (bytes32) {
        uint256 blockNumber = block.number - 1;

        // If hash not provided
        if (uHash == bytes32(0)) {

            uHash = keccak256(
                abi.encodePacked(
                    store.lastHash,
                    msg.sender,
                    gasleft(),
                    block.gaslimit,
                    block.difficulty,
                    msg.sig,
                    blockhash(blockNumber)
                )
            );
        }        

        require(store.hashes[uHash] != true, "NOT_UNIQUE_HASH");
        store.hashes[uHash] = true;
        store.lastHash = uHash;

        return uHash;
    }
}
