pragma solidity 0.5.8;

import "openzeppelin-solidity/contracts/token/ERC20/IERC20.sol";
import "../managers/MarketManager.sol";


/**
 * @title MarketManagerTest
 * @dev This contract represents testing logic for MarketManager
 * @author Kostiantyn Smyrnov <kostysh@gmail.com>
 */
contract MarketManagerTest is MarketManager {

    /**
     * @dev MarketManagerTest constructor
     * @param uaxAddress Address of the UAX token contract
     */
    constructor(
        address uaxAddress
    ) public MarketManager(uaxAddress) {}

    /**
     * @dev Trying to call #create function with zero tokenAddress parameter
     */
    function testCreateWithWrongAddress() public {
        market.create(address(0), 100, 100);
    }
}
