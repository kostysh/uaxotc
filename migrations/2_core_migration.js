const Otc = artifacts.require('Otc');

const uaxAddress = '0xdfcab0b79acf7b189f4c50b512fe059f8b28e081';// current UAX contract in the Ropsten network

module.exports = async (deployer, network, accounts) => {
    await deployer.deploy(Otc, uaxAddress);
    const otc = await Otc.deployed();
    await otc.updateFee.sendTransaction(50, 15, 5, [50000, 100000, 500000, 1000000, 2000000]);
    
};

// const Token = artifacts.require('Token');

// module.exports = async (deployer, network, accounts) => {
//     // await deployer.deploy(Token, 'Dude Coin', 'DUD', 18, new web3.utils.BN('10000000000000000000000000'));
//     // await deployer.deploy(Token, 'Extra Large Coin', 'XXL', 18, new web3.utils.BN('10000000000000000000000000'));
//     // await deployer.deploy(Token, 'Garmonbozia Coin', 'GMBZ', 18, new web3.utils.BN('10000000000000000000000000'));
//     // await deployer.deploy(Token, 'Cooper Coin', 'COOP', 18, new web3.utils.BN('10000000000000000000000000'));
// };
