const HDWalletProvider = require('truffle-hdwallet-provider');
const NonceTrackerSubprovider = require('web3-provider-engine/subproviders/nonce-tracker');

let pkey;
let apiKey;

try {
    const keys = require('./localkeys.json');
    pkey = keys.pkey;
    apiKey = keys.infuraApiKey;
} catch(e) {}

module.exports = {
    networks: {
        coverage: {
            host: 'localhost',
            port: 8555,
            network_id: '*',
            gas: 0xfffffffffffff,
            gasPrice: 0x01
        },
        ganache: {
            host: '127.0.0.1',
            port: 8545,
            network_id: '*',
            websockets: true
        },
        infura_ropsten: {
            provider: _ => {
                const provider = new HDWalletProvider(pkey, `https://ropsten.infura.io/v3/${apiKey}`);
                const nonceTracker = new NonceTrackerSubprovider();
                provider.engine.addProvider(nonceTracker);
                return provider;                
            },
            network_id: 3,
            gas: 5500000,
            confirmations: 2,
            timeoutBlocks: 200,
            skipDryRun: true
        },
    },

    mocha: {
        enableTimeouts: false
        // timeout: 100000
    },

    compilers: {
        solc: {
            version: '0.5.8',
            settings: {
                optimizer: {
                    enabled: true,
                    runs: 200
                },
                evmVersion: 'constantinople'
            }
        }
    }
};
